## Set timestep simulation
include("parameters/synapses.jl");
include("parameters/neurons.jl");
include("parameters/static_params.jl");

function print_parameters(method::Function, args...)
    parameters = method(args...)
    println("==================================================")
    println("Params: "*string(typeof(parameters)))
    println("--------------------------------------------------")
    for name in fieldnames(typeof(parameters))
        println(string(name)*": "*string(getfield(parameters,name)))
    end
    println("==================================================")
end

function print_parameters()
    print_parameters(get_dendrite_params,"distal")
    print_parameters(get_dendrite_params, "proximal")
    print_parameters(get_lif_inh_params)
    print_parameters(get_AdEx_params)
    print_parameters(get_synapses_params_exc,dt,"dendrite")
    print_parameters(get_tripod_circuit)
end

function write_parameters(fp, method::Function, args...)
    parameters = method(args...)
    write(fp,"\n\n")
    write(fp,"Params: "*string(typeof(parameters))*"\n")
    write(fp, "--------------------------------------------------\n")
    for name in fieldnames(typeof(parameters))
        write(fp,string(name)*": "*string(getfield(parameters,name))*"\n")
    end
end

function write_parameters(filepath::String)
    fp = open(filepath*"parameters.txt","w")
        write(fp,"==================================================\n")
        write_parameters(fp,get_dendrite_params,"distal")
        write_parameters(fp,get_dendrite_params, "proximal")
        write_parameters(fp,get_lif_inh_params)
        write_parameters(fp,get_AdEx_params)
        write_parameters(fp,get_synapses_params_exc,dt,"dendrite")
        write_parameters(fp,get_tripod_circuit)
    write(fp,"==================================================\n")
    close(fp)
end
