"""
Inject current in the compartment

current (pA)
compartment (Soma, Dendrite)
"""
function inject_current(current::Real, compartment::Union{Dendrite,Soma})
	if isa(compartment, Soma)
		compartment.v += current*compartment.model.C⁻*dt
	elseif isa(compartment, Dendrite)
		compartment.v += current*compartment.pm.C⁻*dt
	end
end

function inject_current(compartment::Union{Dendrite,Soma}, current::Real)
	if isa(compartment, Soma)
		compartment.v += current*compartment.model.C⁻*dt
	elseif isa(compartment, Dendrite)
		compartment.v += current*compartment.pm.C⁻*dt
	end
end

"""
Set the external input rate for a neural compartment.

efficacy multiplies the synaptic conductance.
compartment is the target compartment.
rate is the incoming rate in KHz.
dt is the time resolution of the simulation in ms.
"""

set_rate!(compartment, rate; eff=1.) = set_rate!(eff, compartment, rate)

function set_rate!( efficacy::Real, compartment::Union{Dendrite,Soma}, rate::Float64, dt::Real=dt)
    ### rate in Hz, transform to milliHertz
    λ = rate*dt
	@assert(!isnan(λ))
	if λ != 0.
	    spikes = rand(Poisson(abs(λ)))
	    if λ < 0
			for _ in 1:spikes
			    inh_spike!(compartment, eff=efficacy)
			end
		else
			for _ in 1:spikes
			    exc_spike!(compartment, eff=efficacy)
		    end
		end
	end
end


function get_EPSP(v::Array{Float64,2}; spiketime=-1, rest=0, inh=false, compartment=1)
	spiketime = spiketime < 0 ? EXCSPIKETIME : spiketime
	if inh == false
	    return maximum(v[compartment,spiketime:end]) - rest
	else
	    return minimum(v[compartment,spiketime:end]) - rest
	end
end

function _PoissonInput(Hz_rate::Real, interval::Int64, dt::Float64)
    λ = 1000/Hz_rate
	spikes = falses(round(Int,interval/dt))
	t = 1
	while t < interval/dt
		Δ = rand(Exponential(λ/dt))
		t += Δ
		if t < interval/dt
			spikes[round(Int,t)] = true
		end
	end
	return spikes
end

function PoissonInput(Hz_rate::Real, interval::Int64, dt::Float64; neurons::Int64=1)
	spikes = falses(neurons, round(Int,interval/dt))
	for n in 1:neurons
		spikes[n,:] .= _PoissonInput(Hz_rate::Real, interval::Int64, dt::Float64)
	end
	return spikes
end

logrange(x1, x2, n) = [10^y for y in range(log10(x1), log10(x2), length=n)]

function null_input(;kwargs...)
end

function active_neuron(;
                  step::Int64,
                  dt::Float64,
				  tripod=nothing,
                  soma=nothing,
				  somaspike=false)
		set_rate(50., tripod.s, 0.1)
end

function test_synapse(;
                  step::Int64,
                  dt::Float64,
				  tripod=nothing,
                  soma=nothing,
				  somaspike=false)

    if step == 1
        if tripod != nothing
            if somaspike
                exc_spike(tripod.s, 1.)
				inh_spike(tripod.s, 1.)
            else
                exc_spike(tripod.d[1], 1.)
				inh_spike(tripod.d[1], 1.)
            end
        end
        if soma != nothing
            exc_spike(soma,1.)
			inh_spike(soma, 1.)
        end
    end
end

function get_efficacies(rate_range, effective_range)
    efficacy = Array{Float64,2}(undef, length(rate_range),length(effective_range))
    for n in eachindex(effective_range)
        for i in eachindex(rate_range)
            efficacy[i,n] = effective_range[n]/rate_range[i]
        end
    end
    return efficacy
end

function set_synapses(neurons, receptor::String, value::Float64)
    if isa(neurons,Array{Tripod,1})
        for neuron in neurons
            for d in neuron.d
                set_gsyn(getfield(d.syn,Symbol(receptor)),value)
            end
        end
    elseif isa(neurons,Tripod)
        for d in neurons.d
            set_gsyn(getfield(d.syn,Symbol(receptor)),value)
        end
    end
end

nmda_curr(v) =-(v-syn.NMDA_rev)*(1+ 1/syn.NMDA_b *exp(syn.NMDA_k*v))^-1
nmda_curr_corr(v) =-(v-syn.NMDA_rev)*(1+(1/syn.NMDA_b)*exp(syn.NMDA_k*v))^-1 
