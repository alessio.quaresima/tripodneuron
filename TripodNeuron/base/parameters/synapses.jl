function duarte_exc_synapse(dt::Float64, compartment::String)
    E_exc    =  0.00       #(mV) Excitatory reversal potential

    gsyn_ampa  = 0.73   #(nS) from Renato heterogeneity paper
    τr_ampa = 0.26        #(ms) ampa conductance decay time
    τd_ampa = 2.0         #(ms) ampa conductance rise time

    gsyn_nmda  = 0.1595   #(nS)
    # gsyn_nmda  = 0.1269   #(nS)
    τd_nmda = 100.0       #(ms) nmda conductance rise time
    τr_nmda = 0.99        #(ms) nmda conductance decay time

    E_gabaA   = -75       #(mV) GABA_A reversal potential
    gsyn_gabaA = 0.1259   #(nS)
    τr_gabaA= 0.24        #(ms) GABA_A decay time
    τd_gabaA= 6.0         #(ms) GABA_A decay time

    gsyn_gabaB = .006     #(nS)
    E_gabaB   = -90      #(mV) GABA_B reversal potential
    τr_gabaB= 30       #(ms) GABA_B conductance decay time
    τd_gabaB= 400        #(ms) GABA_B conductance decay time

    nmda_b   = 3.36       #(no unit) parameters for voltage dependence of nmda channels
    nmda_k   = -0.062     #(1/V) source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
    nmda_v   = 0.0        #(1/V)    source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
	nar = 1.
	## To be implemented in equations:
	τD  = 500 			  # Synaptic vescicles depression from Wang 1999
	pv  = 0.35

	AMPA  = Receptor(E_exc, τr_ampa, τd_ampa, gsyn_ampa)
	NMDA  = ReceptorVoltage(E_exc, τr_nmda, τd_nmda, gsyn_nmda, nmda_b, nmda_k, nmda_v)
	GABAa = Receptor(E_gabaA, τr_gabaA, τd_gabaA, gsyn_gabaA)
	GABAb = Receptor(E_gabaB, τr_gabaB, τd_gabaB, gsyn_gabaB)

    return Synapse(AMPA, NMDA, GABAa, GABAb, false)
end

function eyal_exc_synapse(dt::Float64, compartment::String)
    E_exc    =  0.00       #(mV) Excitatory reversal potential

    gsyn_ampa  = 0.73     #(nS) from Renato heterogeneity paper
    τr_ampa = 0.26        #(ms) ampa conductance decay time
    τd_ampa = 2.0         #(ms) ampa conductance rise time

    gsyn_nmda  = 1.31     #(nS)
    τr_nmda = 8.          #(ms) nmda conductance rise time
    τd_nmda = 34.99       #(ms) nmda conductance decay time

    E_gabaA   = -75       #(mV) GABA_A reversal potential
    gsyn_gabaA = 0.1259    #(nS)
    τr_gabaA= 5.8         #(ms) GABA_A decay time
    τd_gabaA= 28.0        #(ms) GABA_A decay time

    gsyn_gabaB = 0.006     #(nS)
    E_gabaB   = -90      #(mV) GABA_B reversal potential
    τr_gabaB = 30.       #(ms) GABA_B conductance decay time
    τd_gabaB= 400.        #(ms) GABA_B conductance decay time

	if compartment == "soma"
		## no GabaB no NMDA
	    gsyn_nmda  = 0.   #(nS)
	    gsyn_gabaB = 0.0  #(nS)

		## fit from Miller
	    gsyn_gabaA = .265   #(nS)
	    τr_gabaA= 0.1     #(ms) GABA_A decay time
	    τd_gabaA= 18.0    #(ms) GABA_A decay time
	end

    nmda_b   = 3.36       #(no unit) parameters for voltage dependence of nmda channels
    nmda_k   = -0.062     #(1/V) source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
    nmda_v   = 0.0        #(1/V)    source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
	nar = 1.
	## To be implemented in equations:
	τD  = 500 			  # Synaptic vescicles depression from Wang 1999
	pv  = 0.35

	AMPA  = Receptor(E_exc, τr_ampa, τd_ampa, gsyn_ampa)
	NMDA  = ReceptorVoltage(E_exc, τr_nmda, τd_nmda, gsyn_nmda, nmda_b, nmda_k, nmda_v)
	GABAa = Receptor(E_gabaA, τr_gabaA, τd_gabaA, gsyn_gabaA)
	GABAb = Receptor(E_gabaB, τr_gabaB, τd_gabaB, gsyn_gabaB)

    return Synapse(AMPA, NMDA, GABAa, GABAb, false)

end

function get_synapses_pv(dt::Float64)
    E_exc    =  0.00       #(mV) Excitatory reversal potential
	E_gabaB   = -90      #(mV) GABA_B reversal potential
    E_gabaA   = -75       #(mV) GABA_A reversal potential

	gsyn_ampa   = 1.040196
	τr_ampa   = 0.087500
	τd_ampa   = 0.700000

	gsyn_nmda   = 0.002836
	τr_nmda   = 0.990099
	τd_nmda   = 100.000000

	gsyn_gabaA   = 0.844049
	τr_gabaA   = 0.096154
	τd_gabaA   = 2.500000

	gsyn_gabaB   = 0.009419
	τr_gabaB   = 12.725924
	τd_gabaB   = 118.866124


    nmda_b   = 3.36       #(no unit) parameters for voltage dependence of nmda channels
    nmda_k   = -0.062     #(1/V) source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
    nmda_v   = 0.0        #(1/V)    source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
	nar = 1.
	## To be implemented in equations:
	τD  = 500 			  # Synaptic vescicles depression from Wang 1999
	pv  = 0.35

	AMPA  = Receptor(E_exc  , τd_ampa,  gsyn_ampa*norm_synapse( τr_ampa,τd_ampa))
	NMDA  = ReceptorVoltage(E_exc, τd_nmda, gsyn_nmda*norm_synapse( τr_ampa,τd_ampa), nmda_b, nmda_k, nmda_v)
	GABAa = Receptor(E_gabaA, τd_gabaA, gsyn_gabaA*norm_synapse(τr_gabaB,τd_gabaB))
	GABAb = Receptor(E_gabaB, τd_gabaB, gsyn_gabaB*norm_synapse(τr_gabaA,τd_gabaA))

    return Synapse(AMPA, NMDA, GABAa, GABAb, true)

end

function get_synapses_sst(dt::Float64)
    E_exc    =  0.00       #(mV) Excitatory reversal potential
    E_gabaA   = -75       #(mV) GABA_A reversal potential
    E_gabaB   = -90      #(mV) GABA_B reversal potential

	gsyn_ampa   = 0.557470
	τr_ampa   = 0.180000
	τd_ampa   = 1.800000

	gsyn_nmda   = 0.011345
	τr_nmda   = 0.990099
	τd_nmda   = 100.000000

	gsyn_gabaA   = 0.590834
	τr_gabaA   = 0.192308
	τd_gabaA   = 5.000000

	gsyn_gabaB   = 0.016290
	τr_gabaB   = 21.198947
	τd_gabaB   = 193.990036


    nmda_b   = 3.36       #(no unit) parameters for voltage dependence of nmda channels
    nmda_k   = -0.062     #(1/V) source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
    nmda_v   = 0.0        #(1/V)    source: http://dx.doi.org/10.1016/j.neucom.2011.04.018)
	nar = 1.
	## To be implemented in equations:
	τD  = 500 			  # Synaptic vescicles depression from Wang 1999
	pv  = 0.35

	AMPA  = Receptor(E_exc, τr_ampa, τd_ampa, gsyn_ampa)
	NMDA  = ReceptorVoltage(E_exc, τr_nmda, τd_nmda, gsyn_nmda, nmda_b, nmda_k, nmda_v)
	GABAa = Receptor(E_gabaA, τr_gabaA, τd_gabaA, gsyn_gabaA)
	GABAb = Receptor(E_gabaB, τr_gabaB, τd_gabaB, gsyn_gabaB)


    return Synapse(AMPA, NMDA, GABAa, GABAb, false)

end
