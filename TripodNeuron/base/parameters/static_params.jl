const dt = 0.1
const do_plot = true

const AdEx = get_AdEx_params()
const LIF_pv  = get_lif_params("PV")
const LIF_sst  = get_lif_params("SST")

get_synapses_exc(dt,comp)= duarte_exc_synapse(dt,comp)
get_synapses_exc(dt,comp)= eyal_exc_synapse(dt,comp)

Esyn_dend = get_synapses_exc(dt, "dend")
Esyn_soma = get_synapses_exc(dt, "soma")
Isyn_sst  = get_synapses_sst(dt)
Isyn_pv   = get_synapses_pv(dt)

const Mg_mM     = 1.

HUMAN = Physiology(200Ω*cm,38907Ω*cm^2, 0.5μF/cm^2)
MOUSE = Physiology(200Ω*cm,1700Ω*cm^2,1μF/cm^2)

adapt = Adaptation(2/dt) ## post_spike refractory period


const H_distal_distal    = "H. 1->s, 400,4; 2->s, 400, 4"
const H_proximal_proximal= "H. 1->s, 150,4; 2->s, 150, 4"
const H_medial_proximal  = "H. 1->s, 300,4; 2->s, 150, 4"
const H_distal_proximal  = "H. 1->s, 400,4; 2->s, 150, 4"
const H_ball_stick       = "H. 1->s, 400,4"
const H_proximal_thin    = "H. 1->s, 150,2.5; 2->s, 150, 4"
const H_medial           = "H. 1->s, 314,4; 2->s, 314, 4"
const H_distal           = "H. 1->s, 400,4; 2->s, 400, 4"
const H_proximal         = "H. 1->s, 150,4; 2->s, 150, 4"
const H_ss               = "H. 1->1, 100,4; 2->2,100,4"

const M_distal_distal    = "M. 1->s, 400,4; 2->s, 400, 4"
const M_proximal_proximal= "M. 1->s, 150,4; 2->s, 150, 4"
const M_distal_proximal  = "M. 1->s, 400,4; 2->s, 150, 4"
const M_ball_stick       = "M. 1->s, 400,4"
const M_proximal_thin    = "M. 1->s, 150,2; 2->s, 150, 4"
const M_medial           = "M. 1->s, 314,4; 2->s, 314, 4"

const default_model  = M_distal_proximal

const models = [H_distal, H_medial, H_proximal, H_distal_proximal, H_medial_proximal, H_ss]
const labels = ["distal-distal", "sh.distal-sh.distal", "proximal-proximal", "distal-proximal", "sh.distal-proximal", "soma only"]
# const models = Dict("hdd"=>distal_distal, "hpp"=>proximal_proximal, "hdp"=>distal_proximal,
#                     "dd"=>distal_distal, "pp"=>proximal_proximal, "dp"=>distal_proximal)

## With the set parameter this is the maximal dendritic length
const distal_th = 314
