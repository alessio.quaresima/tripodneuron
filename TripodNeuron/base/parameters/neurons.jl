#===========================
Get neuronal parameters
===========================#
function get_lif_params(type::String)
	if type=="PV"
	    a     = .0        #(nS) 'sub-threshold' adaptation conductance
	    b     = 10.       #(pA) 'sra' current increment
	    τw    = 144        #(s) adaptation time constant (~Ca-activated K current inactivation)
		E_rest  = -64.33
	    u_reset = -57.47   #(mV)
	    u_th  = -38.97   #(mV)
	    C     = 104.52     #(pF)
	    gL    = 9.75       #(nS)
	    t_ref  = 0.52       #(ms)
	    τm    = C/gL       #(ms) membrane RC time
	    return LIF_PV(τm, gL, E_rest, u_reset, u_th, t_ref, a,b,τw)
	elseif type =="SST"
	    a     = 4.0        #(nS) 'sub-threshold' adaptation conductance
	    b     = 80.5       #(pA) 'sra' current increment
	    τw    = 144        #(s) adaptation time constant (~Ca-activated K current inactivation)
		E_rest  = -61
	    u_reset = -47.11   #(mV)
		u_th    = -34.4
	    C       = 102.87     #(pF)
	    gL      = 4.61       #(nS)
	    t_ref    = 1.34       #(ms)
	    τm      = C/gL       #(ms) membrane RC time
	    return LIF_SST(τm, gL, E_rest, u_reset, u_th, t_ref, a,b,τw)
	end
     #(s) adaptation time constant (~Ca-activated K current inactivation)
end

function get_AdEx_params(;initburst=false, noadapt=false)
    ## membrane params
    C     = 281        #(pF)
    gL    = 40         #(nS) leak conductance #BretteGerstner2005 says 30 nS
    τm    = C/gL       #(ms) membrane RC time

	# thresholds
    u_reset = -70.6      #(mV)
	E_rest  = -76.4
	u_th    = -44
	t_ref   = 2.05       #ms Not used!

    ## adaptation
    τw    = 144        #(ms) adaptation time constant (~Ca-activated K current inactivation)
    a     = 4.0        #(nS) 'sub-threshold' adaptation conductance
    b     = 80.5       #(pA) 'sra' current increment
	if noadapt
		a=0
		b=0
	end

    # I0    = 0.65e-9  #(A) applied current in test example
    initburst =false
    if initburst
        τm= 5e-3
        gL  = 2e-9     # leak conductance
        Cm  = τm*gL
        a   = 0.5e-9   # 'sub-threshold' adaptation conductance
        b   = 7e-12    # 'sra' current increment
        τw= 0.200    # adaptation time constant (~Ca-activated K current inactivation)
        I0  = 0.065e-9 # applied current
    end
	# syn_soma = get_synapses_params_exc(dt,"soma")
	# syn_dend = get_synapses_params_exc(dt,"dend")
    return AdExParams(τm, gL, E_rest, u_reset, u_th, t_ref, τw, a, b)
end
