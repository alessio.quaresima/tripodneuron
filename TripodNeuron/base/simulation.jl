
## Define timestep for simulation

"""
Basic tool to test stimulation protocols
"""
function run_simulation(protocol; synapses=false, sim_time=5000, tripod=nothing, fast=false, kwargs...)
        if tripod != nothing
            t = Tripod(tripod)
        else
            t = Tripod(default_model)
        end

        if fast
            voltage = simulate_fast(t, sim_time, protocol; kwargs...)
            spikes = get_spikes(voltage[1,:])
            rate   = get_spike_rate(voltage)
            return voltage, spikes
        else
            voltage, currents, synapses = simulate_tripod(t, sim_time, protocol; record_synapses=synapses, kwargs...)
            inh_voltage = nothing
        end
        spikes = get_spikes(voltage[1,:])
        rate   = get_spike_rate(voltage)
    return voltage,currents,spikes, synapses, inh_voltage
end

function simulate_model(model::String, sim_time::Int, protocol::Function; kwargs...)
	""" Simulate voltage dynamics with Tripod or TripodSoma model"""
		t = Tripod(model)
    if model == H_ss
	 	_ = simulate_tripod_soma(t,1000, protocol; kwargs...)
	 	return simulate_tripod_soma(t,sim_time, protocol; kwargs...)
	else
        _ = simulate_fast(t, 1000, protocol; kwargs...)
        return simulate_fast(t, sim_time, protocol; kwargs...)
	end
end

function simulate_neuron(neuron, protocol; synapses=false, sim_time=5000, kwargs...)
	""" Simulate tripod or inhibitory neurons """
        if isa(neuron, Tripod)
            voltage, currents, synapses = simulate_tripod(neuron, sim_time, protocol; record_synapses=synapses, kwargs...)
            spikes = get_spikes(voltage)
            rate   = get_spike_rate(voltage)
            return voltage,currents,spikes, rate, synapses
        elseif isa(neuron,Soma)
            voltage, synapses = simulate_lif(neuron, sim_time, protocol; record_synapses=synapses, kwargs...)
            spikes = get_spikes(voltage)
            rate   = get_spike_rate(voltage)
            return voltage, spikes, rate, synapses
        else
            @assert(1==0)
        end
end


"""
Run basic simulation of a single cell.

Parameters
----------
t -> an instance of Tripod neuron.

stimulation_protocol -> function to be run during the simulation, the function has 3 variables: 'tripod','time_step','resolution'

record_synapse -> store the values of synaptic conductance through out the simulation, it slows down the simulation

kwargs -> parameters of the stimulation protocol

Return
------
voltage 4xNsteps array. Soma, d1, d2, d3
current 5xNsteps array. Soma, d1, d2, d3, w_adapt_soma
synapses

"""
function simulate_tripod(t::Tripod,simTime::Int64,
                            stimulation_protocol::Union{Function, Array{Int64}};
                            record_synapses=false, record_currents=true, kwargs...)
    total_steps = round(Int,simTime/dt)
    n_dend = length(t.d)
    ## store currents --> to be removed lateron
    ## Recordings
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
    current = Array{Float64,2}(undef,n_dend+2,total_steps)
    if record_synapses
        synapses = Array{Float64,3}(undef,4,total_steps,n_dend+1)
    else
        synapses  = nothing
    end
    spiked = (t.s.v > AdEx.u_th)
	last_spike = -100.

    currents = Array{Float64,1}(undef,n_dend+1)
    # println("simulation with n_dend:", n_dend)
    @fastmath @inbounds for tt in 1:total_steps
        stimulation_protocol(;tripod=t,step=tt,dt=dt, kwargs...)
		if last_spike + adapt.post_spike > tt
			t.s.v = AdEx.Er
		end
        if update_tripod!(t,currents,spiked)
			spiked = true
			last_spike = tt
		else
			spiked = false
		end
        voltage[1,tt] = t.s.v
        for n in 1:n_dend
            voltage[1+n,tt] = t.d[n].v
        end

        ### soma incoming current
        # This is a bit cumbersome, what I do here is to
        # get the current from the simulation and dispose in this order
        # current = [s_in, d1_out, d2_out, d3_in-out, w_adapt]
        current[1,tt] = currents[end]
        current[2:end-1,tt] = - currents[1:end-1]
        current[end,tt] = t.s.w
        if record_synapses
            store_synapses(synapses,t,tt)
        end
    end
    ## Current and Voltage plot
    return voltage, current, synapses
end

function simulate_fast(t::Tripod,simTime::Int64,
                            stimulation_protocol::Union{Function, Array{Int64}}; kwargs...)
    total_steps = round(Int,simTime/dt)
    n_dend = length(t.d)
    ## Recordings
    currents = Array{Float64,1}(undef,n_dend+1)
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
    spiked=false
	last_spike = -100.
    spiked = (t.s.v > AdEx.u_th)

    @fastmath @inbounds for tt in 1:total_steps
        stimulation_protocol(;tripod=t,step=tt,dt=dt, kwargs...)
		if last_spike + adapt.post_spike > tt
			t.s.v = AdEx.Er
		end
        if update_tripod!(t,currents,spiked)
			spiked = true
			last_spike = tt
		else
			spiked = false
		end
        voltage[1,tt] = t.s.v
        for n in 1:n_dend
            voltage[1+n,tt] = t.d[n].v
        end
    end
    ## Current and Voltage plot
    return voltage
end

function simulate_nospike(t::Tripod,simTime::Int64,
                            stimulation_protocol::Union{Function, Array{Int64}}; kwargs...)
    total_steps = round(Int,simTime/dt)
    n_dend = length(t.d)
    ## Recordings
    currents = Array{Float64,1}(undef,n_dend+1)
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
	last_spike = -100.

    @fastmath @inbounds for tt in 1:total_steps
        stimulation_protocol(;tripod=t,step=tt,dt=dt, kwargs...)
        update_nospike!(t,currents)
        voltage[1,tt] = t.s.v
        for n in 1:n_dend
            voltage[1+n,tt] = t.d[n].v
        end
    end
    ## Current and Voltage plot
    return voltage
end

function simulate_tripod_soma(t::Tripod,simTime::Int64,
                            stimulation_protocol::Union{Function, Array{Int64}}; record_currents=false, kwargs...)
    total_steps = round(Int,simTime/dt)
    n_dend = length(t.d)
    ## Recordings
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
    currents = Array{Float64,1}(undef,total_steps)
    spiked=false
	last_spike = -100.
    spiked = (t.s.v > AdEx.u_th)

    @fastmath @inbounds for tt in 1:total_steps
        stimulation_protocol(;tripod=t,step=tt,dt=dt, kwargs...)
		if last_spike + adapt.post_spike > tt
			t.s.v = AdEx.Er
		end
		# update dendrites
        soma_v = t.s.v
    	for (n,d) in enumerate(t.d)
            d.v = soma_v
            update_synapses_double!(d, Esyn_dend)
            t.s.v -= dt*AdEx.C⁻ *(syn_current(d, Esyn_dend))
    	end
        if update_AdEx_soma!(t.s, spiked)
			spiked = true
			last_spike = tt
		else
			spiked = false
		end
        voltage[:,tt] .= t.s.v
		currents[tt] = t.s.w
    end
	if record_currents
	    return voltage, currents
	else
	    return voltage
	end
end


function simulate_lif(lif::Soma,simTime::Int64,
                            stimulation_protocol::Union{Function, Array{Int64}};
                            record_synapses=false, kwargs...)
    total_steps = round(Int,simTime/dt)
    ## Recordings
    voltage = Array{Float64,1}(undef,total_steps)
    if record_synapses
        synapses = Array{Float64,3}(undef,4,total_steps,1)
    else
        synapses  = nothing
    end
    spiked = false

    @fastmath @inbounds for n in 1:total_steps
        spiked = update_lif!(lif,spiked)
        stimulation_protocol(;soma=lif, step=n,dt=dt, kwargs...)
        voltage[n] = lif.v
        ### soma incoming current
        # This is a bit cumbersome, what I do here is to
        # get the current from the simulation and dispose in this order
        # current = [s_in, d1_out, d2_out, d3_in-out, w_adapt]
        if record_synapses
            store_synapses(synapses,lif,n)
        end
    end
    ## Current and Voltage plot
    return voltage, synapses
end

# function coupled_tripod(t::Tripod,simTime::Int64,
#                             stimulation_protocol::Function;
#                             coupling =false, record_synapses=false, kwargs...)
#     total_steps = round(Int,simTime/dt)
#     n_dend = length(t.d)
#     ## store currents --> to be removed lateron
#     currents = Array{Float64,1}(undef,n_dend+1)
#     ## Recordings
#     voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
#     current = Array{Float64,2}(undef,n_dend+2,total_steps)
#     inh_voltage = Array{Float64,1}(undef,total_steps)
#     if record_synapse
#         synapses = Array{Float64,3}(undef,4,total_steps,n_dend+1)
#     else
#         synapses  = nothing
#     end
#     spiked = false
#     spike_inh = true
#     if coupling =="soma"
#         lif = PV()
#     else
#         lif = SST()
#     end
#     # println("Simulate ", total_steps, " with feedback coupling: ", coupling)
#     # println("simulation with n_dend:", n_dend)
#     @fastmath @inbounds for n in 1:total_steps
#         set_rate(1., lif, 1.0, dt)
#         spiked = update_tripod(t,currents,spiked)
#         spike_inh = update_lif(lif, spike_inh)
#         if spiked
#             exc_spike(lif, 500.)
#         end
#         if spike_inh
#             if coupling == "apical"
#                 inh_spike(t.d[2], 20.)
#                 inh_spike(t.d[1], 20.)
#             elseif coupling == "soma"
#                 inh_spike(t.s, 40.)
#             elseif coupling == "proximal"
#                 inh_spike(t.d[3], 40.)
#             end
#         end
#         stimulation_protocol(;tripod=t,step=n,dt=dt, kwargs...)
#         voltage[:,n] = append!([t.s.v],[d.v for d in t.d])
#         inh_voltage[n] = lif.v
#         ### This is a bit cumbersome, what I do here is to
#         ### get the current from the simulation and dispose in this
#         ### order:
#         ### current = [s_in, d1_out, d2_out, d3_in-out, w_adapt]
#         current[1,n] = currents[end]
#         current[2:end-1,n] = - currents[1:end-1]
#         current[end,n] = t.s.w
#         if record_synapse
#             store_synapses(synapses,t,n)
#         end
#     end
#     ## Current and Voltage plot
#     return voltage, current, synapses, inh_voltage
# end


function store_synapses(synapses::Union{Array{Float64,3},Array{Float64,2}}, t::Union{Soma, Tripod},n::Int64)
    if isa(t,Tripod)
        compartments = [t.s,t.d...]
    else
        compartments = [t]
    end
    for (nc,d) in enumerate(compartments)
        synapses[:,n,nc] = [ d.syn.AMPA.gsyn*d.g_AMPA,
                             d.syn.NMDA.gsyn*d.g_NMDA,
                             d.syn.GABAa.gsyn*d.g_GABAa,
                             d.syn.GABAb.gsyn*d.g_GABAb]
     end
     return synapses
 end
