#=====================================
This file presents all the structs used in the code.
The structs are organized in hierarchies.

Neuron:
	Tripod <- [Soma, [Dendrite...], TripodCircuit ]
	PassiveMembraneParameters

Parameters:
	Synapse <- [AMPA, NMDA, GABAa, GABAb]
	AdEx
	LIF
=====================================#
include("structs/synapses.jl")
include("structs/modelParams.jl")
include("structs/neurons.jl")
include("structs/dendrites.jl")
include("structs/units.jl")
