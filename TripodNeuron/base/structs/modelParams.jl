#=====================================
# Neurons
=====================================#
abstract type NeuronParams end

struct AdExParams <: NeuronParams
    #Membrane parameters
    C::Float64                  # (pF) membrane timescale
    gl::Float64                 # (nS) gl is the leaking conductance,opposite of Rm
    Rm::Float64                 # (GΩ) total membrane resistance
    τm::Float64                 # (s) C / gl
    Er::Float64                 # (mV) resting potential

    # AdEx model
    u_r::Float64            # (mV) Reset potential of membrane
    θ::Float64              # (mv) Rheobase threshold
    ΔT::Float64             # (mV) Threshold sharpness
    u_th::Float64           # (mV) Spiking Threshold
	t_ref::Float64          # (ms) absolute refractory time

    # Adaptation parameters
    τw::Float64             #ms adaptation current relaxing time
    a::Float64              #nS
    b::Float64              #pA adaptation increase due to spike

    # Inverse value for simulation speedup
    C⁻::Float64             # (pF) inverse membrane timescale
    τw⁻::Float64            #ms inverse adaptation current relaxing time
    τm⁻::Float64            #ms inverse adaptation current relaxing time
    ΔT⁻::Float64             # (mV) inverse Threshold sharpness

    function AdExParams(τm, gl, E_r, u_r, u_th, t_ref, τw, a, b)
        """
        Parameters for AdEx neuron:
            default parameters get from text
            τm membrane timescale(ms)
            gl membrane leakage
            τw adaptation timescale(ms)
            a subtreshold adaptation (nS)
            b spike-triggered adaptation (pA)
            u_r voltage reset (mV)
        """
        ### Biological values
        # Rm = 0.5e9
        # gl = 1/Rm  #nS
        Rm = 1/gl    #GΩ
        C  = τm * gl #pF
        θ = - 50.4 #mV
        ΔT = 2
        ## These parameters are inverse, to speedup the computation
        C⁻ = 1/C
        τw⁻ = 1/τw
        ΔT⁻ = 1/ΔT
        return new(C, gl, Rm, τm, E_r,
		 			u_r, θ, ΔT, u_th, t_ref,
					 τw, a, b, C⁻,τw⁻,1/τm,ΔT⁻)
    end
end

struct LIF_SST <:NeuronParams
    #Membrane parameters
    C::Float64                  # (pF) membrane timescale
    gl::Float64                 # (nS) gl is the leaking conductance,opposite of Rm
    Rm::Float64                 # (GΩ) total membrane resistance
    τm::Float64                 # (s) C / gl
    Er::Float64                # (mV) resting potential

    # LIF model
    u_r::Float64            # (mV) Reset potential of membrane
    u_th::Float64           # (mV) Spiking Threshold
    t_ref::Float64          # (ms) absolute refractory time

    # Adaptation parameters
    τw::Float64             #ms adaptation current relaxing time
    a::Float64              #nS
    b::Float64              #pA adaptation increase due to spike

    # Inverse value for simulation speedup
    C⁻::Float64             # (pF) inverse membrane timescale
    τw⁻::Float64            #ms inverse adaptation current relaxing time
    τm⁻::Float64            #ms inverse adaptation current relaxing time
    function LIF_SST(τm, gl, E_r, u_r, u_th, t_ref, a,b,τw)
        """
        Parameters for LIF neuron:
        """
        ## These parameters are inverse, to speedup the computation
        C  = τm * gl #pF
        Rm = 1/gl    #GΩ
        C⁻ = 1/C
        τw⁻ = 1/τw
        return new(C, gl, Rm, τm, E_r,
		 		   u_r, u_th, t_ref,
				   τw, a, b, C⁻,τw⁻,1/τm)
    end
end

struct LIF_PV <:NeuronParams
    #Membrane parameters
    C::Float64                  # (pF) membrane timescale
    gl::Float64                 # (nS) gl is the leaking conductance,opposite of Rm
    Rm::Float64                 # (GΩ) total membrane resistance
    τm::Float64                 # (s) C / gl
    Er::Float64                # (mV) resting potential

    # LIF model
    u_r::Float64            # (mV) Reset potential of membrane
    u_th::Float64           # (mV) Spiking Threshold
    t_ref::Float64          # (ms) absolute refractory time

    # Adaptation parameters
    τw::Float64             #ms adaptation current relaxing time
    a::Float64              #nS
    b::Float64              #pA adaptation increase due to spike

    # Inverse value for simulation speedup
    C⁻::Float64             # (pF) inverse membrane timescale
    τw⁻::Float64            #ms inverse adaptation current relaxing time
    τm⁻::Float64            #ms inverse adaptation current relaxing time

    function LIF_PV(τm, gl, E_r, u_r, u_th, t_ref, a,b,τw)
        """
        Parameters for LIF neuron:
        """
        ## These parameters are inverse, to speedup the computation
        C  = τm * gl #pF
        Rm = 1/gl    #GΩ
        C⁻ = 1/C
        τw⁻ = 1/τw
        return new(C, gl, Rm, τm, E_r,
		 		   u_r, u_th, t_ref,
				   τw, a, b, C⁻,τw⁻,1/τm)
    end
end

struct PassiveMembraneParameters
    type::String
    Rm::Float64                 # (GΩ) total membrane resistance
    τm⁻::Float64                # (s) 1/RC
    E_r::Float64                # (mV) resting potential
    C⁻::Float64                 #(ms) membrane timescale
	g_ax::Float64				# (nS) axial conductance
	s::String                   # Dend specie (M or H)
	d::Float64					# μm dendrite diameter
	l::Float64					# μm distance from next compartment
    function  PassiveMembraneParameters(
                                type::String,
								s,
								d,
								l)
					gL, g_ax, Cm, = dend_parameters(s=s,d=d, l=l);
				    τm⁻    = gL/Cm    #(1/s) inverse of membrane τ = RC time
				    Rm  = 1/gL
				    E_r    = -76.6   #(mV) leak reversal potential
            return new(type,Rm, τm⁻, E_r,1/Cm, g_ax,s, d, l)
        end
end
