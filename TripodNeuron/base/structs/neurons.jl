mutable struct Soma
    ### circuit element
    v::Float64         #(mV) membrane potential of axosomatic compartment
    w::Float64         #(pA) adaptive current

    ### Synapse
    g_GABAa::Float64       #(nS) decay_var of dendrite compartment
    g_GABAb::Float64       #(nS) decay_var of dendrite compartment
    g_AMPA::Float64        #(nS) decay_var of dendrite compartment
    h_GABAa::Float64       #(nS) rise_variable of soma compartment
    h_GABAb::Float64       #(nS) rise_variable of soma compartment
    h_AMPA::Float64        #(nS) rise_variable of soma compartment
    h_NMDA::Float64        #(nS) rise_variable of dend compartment
    g_NMDA::Float64        #(nS) decay_var of dendrite compartment

	id::Int64
	syn::Synapse
	model::NeuronParams
    function Soma(syn, model)
	    id=1
        w = 0. #AdEx.b
        v = model.Er
        return new(v,w,zeros(8)..., id,syn, model)
    end
    function Soma(id, syn, model)
        w = 0. #AdEx.b
        v = model.Er
        return new(v,w,zeros(8)..., id,syn, model)
    end
end

mutable struct Dendrite
    id::Int16        # Dendrite id number
    v::Float64       # (mV) membrane potential of dendritic compartment
    ### Synapse
    g_GABAa::Float64       #(nS) decay_var of dendrite compartment
    g_GABAb::Float64       #(nS) decay_var of dendrite compartment
    g_NMDA::Float64        #(nS) decay_var of dendrite compartment
    g_AMPA::Float64        #(nS) decay_var of dendrite compartment
    h_GABAa::Float64       #(nS) rise_variable of dend compartment
    h_GABAb::Float64       #(nS) rise_variable of dend compartment
    h_NMDA::Float64        #(nS) rise_variable of dend compartment
    h_AMPA::Float64        #(nS) rise_variable of dend compartment
	syn::Synapse
    pm::PassiveMembraneParameters
    function Dendrite(n, pm::PassiveMembraneParameters)
        return new(n, pm.E_r,zeros(8)..., Esyn_dend, pm)
    end
    function Dendrite(n, s, d, l)
		type= l > 150 ? "distal" : "proximal"
		pm = PassiveMembraneParameters(type,s,d,l)
        return new(n, pm.E_r,zeros(8)..., Esyn_dend, pm)
    end
end

struct Adaptation
	post_spike::Float64
end


Compartment = Union{Soma, Dendrite}

struct TripodCircuit
    """
    This struct contains the circuital parameters of the tripod model
    The circuit is defined by 6 conductance values.
    The conductances between two compartments are not symmetric

    When the neuron has 2 compartments instead than 3 the number of
    parameters is the same.
    """
	links::Array{Int64,2}
	conductance::Array{Float64,2}
end

##
function model_parser(model::String)
	if occursin("->",model)
		function get_link(link, n_soma)
			o,t = split(link,"->")
			o = parse(Int,o)
			t = try
			    parse(Int,t)
			  catch
			    n_soma
			end
			return [o,t]
		end
		species, model = split(model,".")
		compartments = split(model,";")
		n_soma = length(compartments)+1
		links = Vector()
		params = Vector()
		for comp in compartments
			link, l, d = split(strip(comp),",")
			push!(links, get_link(link,n_soma))
			push!(params,Dict("s"=>species, "l"=>parse(Float64,l), "d"=>parse(Float64,d)))
		end
		links = hcat(links...)
		return links, params
	else
		return [[] []], []
	end
end

struct TripodModel
	links::Array{Int,2}
	params::Array{Any,1}
	function TripodModel(model::String)
		links, params = model_parser(model)
		new(links, params)
	end
end

##
##
##



struct Tripod
    """
    """
    s::Soma
    d::Array{Dendrite,1}
    c::TripodCircuit
	model::TripodModel
    function Tripod(;id=1, model::TripodModel=default_model)
        ### TODO AdEx and Esyn are constant but may change
        dendrites = get_dendrites(model)
        soma = Soma(id, Esyn_soma, AdEx)
        circuit = get_tripod_circuit(model, dendrites)
        return new(soma, dendrites, circuit, model)
    end
    function Tripod(stringparams::String)
		model = TripodModel(stringparams)
		Tripod(id=1, model=model)
	end
    function Tripod(model::TripodModel)
		model = TripodModel(stringparams)
		Tripod(id=1, model=model)
	end
	# end
    function Tripod(id::Int64)
		Tripod(id=id,model=TripodModel(default_model))
	end
    function Tripod()
		Tripod(id=1,model=TripodModel(default_model))
	end
    function Tripod(model::TripodModel)
		Tripod(id=1,model=model)
	end
end

function SST(;)
	return Soma(Isyn_sst, LIF_sst)
end
function SST(id::Int64)
	return Soma(id, Isyn_sst, LIF_sst)
end

function PV(;)
	return Soma(Isyn_pv, LIF_pv)
end

function PV(id::Int64)
	return Soma(id, Isyn_pv, LIF_pv)
end

function ADEX(id::Int64)
	return Soma(id, Esyn_soma, AdEx)
end

function ADEX(;)
	return Soma( Esyn_soma, AdEx)
end
