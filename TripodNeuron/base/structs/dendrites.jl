function get_dendrites(model::TripodModel)
    d = Array{Dendrite,1}(undef,0)
	for (n,params) in enumerate(model.params)
		push!(d,Dendrite(n,params["s"], params["d"],params["l"]))
	end
	return d
end


function get_tripod_circuit(model::TripodModel,dendrites::Array{Dendrite})
	n_dend = length(dendrites)
	conductance = zeros(n_dend+1, n_dend+1)
	if n_dend >0
		for (origin, target) in eachcol(model.links)
			conductance[origin,target] = dendrites[origin].pm.g_ax
			conductance[target,origin] = dendrites[origin].pm.g_ax *2
		end
	end
    return TripodCircuit(model.links, conductance)
end

function dend_parameters(;d,l,s)
    d = d*μm
    l = l*μm
	if s =="M"
		Ri,Rd,Cd = MOUSE.Ri,MOUSE.Rd,MOUSE.Cd
	elseif s =="H"
		Ri,Rd,Cd = HUMAN.Ri,HUMAN.Rd,HUMAN.Cd
	end
	if s=="M" && l.val>400
	    return G_mem(Rd=Rd,d=d,l=l).val, G_axial(Ri=Ri,d=d,l=l).val, C_mem(Cd=Cd,d=d, l=l).val
	else
	    return G_mem(Rd=Rd,d=d,l=l).val, G_axial(Ri=Ri,d=d,l=l).val, C_mem(Cd=Cd,d=d, l=l).val
	end
end

function dend_parameters(dendrite::Dendrite)
    d = dendrite.pm.d
    l = dendrite.pm.l
	s = dendrite.pm.s
	dend_parameters(d=d,l=l,s=s)
end


function get_circuit_properties(comp::Union{Dendrite,Soma})
	if isa(comp,Soma)
		return AdEx.gl, AdEx.C
	end
	if isa(comp,Dendrite)
		return dend_parameters(comp)
	end
end


function get_circuit_properties(model)
	tripod = Tripod(model)
	x =""
	soma = get_circuit_properties(tripod.s)
	x *= @sprintf "Soma: G leak: %.2f nS; C %.2f pF \n" soma[1] soma[2]
	for (n,d) in enumerate(tripod.d)
		dend = get_circuit_properties(d)
		len    = d.pm.l
		diam    = d.pm.d
		x*= @sprintf "Dendrite %d, diameter: %d μm, length %d μm; G leak: %.2f nS; G axial %.2f nS, C %.2f pF \n" n diam len dend[1] dend[2] dend[3]
	end
	return x
end

function get_memory_time(p)
	return p[3]/(p[1]+p[2])
end
