using Pkg

Pkg.add("Distributions")
Pkg.add("PyCall")
# Pkg.add("PyPlot")
Pkg.add("Plots")
Pkg.add("StatsPlots")
Pkg.add("HDF5")
Pkg.add("RollingFunctions")
