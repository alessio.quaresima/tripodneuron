Comments on meeting
###################

1. Shunting inhibition can be biologically considered as short circuit. I could check this looking for the current outflowing from the compartment
I should change the terminology and swap _shunting_ with _divisive_.

2. Call _somatic_ -> _soma_

<!-- 4. Put legend in colorbar and set it to fixed values -->

5. Add absolute refractory times? Nor Clopath nor GersTNer use them

6. Check parameters for synaptic efficacy.
    I ve done so and fitted the synaptic timescales
    BE VERY CAREFUL ABOUT THE  switch to alpha function. Take E/I PSP from Renato paper from the tripl exponential. And double check all the synaptic and receptor parameters.

7. Study the dynamic ranges of spike rates with the intensity of inhibition.

8. Check the time course of NMDA spikes and [Larkum] work.  

9. Consider Ca spikes in case of the proximal compartment.

10. READ the paper.
