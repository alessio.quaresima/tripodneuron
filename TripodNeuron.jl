using Random
using Distributions
using Logging
using Printf


module TripodNeuron
    import ..Logging, ..Printf, ..Distributions
    using BenchmarkTools
    using Logging, Printf, Distributions
    # include("TripodNeuron/base/structs/dendrites.jl")
    include("TripodNeuron/base.jl")
end


import .TripodNeuron
TN = TripodNeuron

#

function TripodTest()
    TN.Tripod()

    # mini tests
    v = TripodNeuron.run_simulation(TripodNeuron.null_input)
    @assert(abs(mean(v[1][1,:])- TN.AdEx.Er) <1)

    v = TripodNeuron.simulate_neuron(TN.SST(), TripodNeuron.null_input)
    @assert(abs(mean(v[1][:])- TN.LIF_sst.Er) <1)

    v = TripodNeuron.simulate_neuron(TN.PV(), TripodNeuron.null_input)
    @assert(abs(mean(v[1][:])- TN.LIF_pv.Er) <1)

    function constant_firing()
        tripod = TN.Tripod()
        spiked = false
        currents = [0., 0., 0.]
        v = Vector()
        for i in 1:1000
            TN.exc_spike!(tripod.s)
            spiked = TN.update_tripod!(tripod, currents, spiked)
            push!(v,tripod.s.v)
        end
        tripod = TN.SST()
        spiked = false
        v = Vector()
        for i in 1:1000
            TN.exc_spike!(tripod)
            spiked = TN.update_lif_sst!(tripod, spiked)
            push!(v,tripod.v)
        end
    end
    constant_firing()
end
##
TripodTest()
