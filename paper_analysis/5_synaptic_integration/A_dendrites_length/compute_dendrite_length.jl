"""
Compute the spiking rate of the tripod in all dendritic configurations
Synaptic input only on dendrites, and innhibition with same input rate than the excitation
"""
##
include("../../base.jl")
using HDF5

function stimulus(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  eff::Real,
                  ν::Real)
    """ Poisson input train"""
      TN.set_rate!(tripod.d[1], ν)
      TN.set_rate!(tripod.d[2], ν)
      TN.set_rate!(tripod.d[1], -ν,eff=eff)
      TN.set_rate!(tripod.d[2], -ν,eff=eff)
end


function test_length(; d1::Int,d2::Int, ν::Float64, eff::Float64)
    model ="H. 1->s, $d1,4; 2->s, $d2,4"
    tripod = TN.Tripod(TN.TripodModel(model))
    _= TN.simulate_fast(tripod, 1000, stimulus, eff=eff, ν=ν)
    voltage= TN.simulate_fast(tripod, 10000, stimulus, eff=eff, ν=ν)
    r = TN.get_spike_rate(voltage)
    c = TN.get_cv(voltage)
    v = mean(voltage, dims=2)
    return [r c v...]
end



function map_dspace(;ds, rates, opt_eff::Bool)
    samples= 20
    eff = 1.
    data = zeros(5, length(ds), length(ds), length(rates)) ## rate/cv/voltage/soma/d1/d2
    Threads.@threads for i in eachindex(rates)
        ν = rates[i]
        println("ν: ", round(ν, digits=2), " eff: ", round(eff, digits=2))
        effs[i] = eff
        for (n,d1) in enumerate(ds)
            for (m,d2) in enumerate(ds)
                out = [0. 0. 0. 0. 0.]
                for _ in 1:samples
                    out .+= test_length(d1=d1, d2=d2, ν=ν, eff=eff)
                end
            data[:,n,m,i] = out ./samples
        end
        end
    end
    return data
end

# out = test_length(d1=200, d2=220, ν=1., eff=1.)
println("Dendrite length test")
println("#####################")
ds = 100:10:500
rates = 10 .^range(-0.5,1.5,length=40)

data = map_dspace(ds=ds, rates=rates)
file =joinpath(@__DIR__,"data/dendritic_length.h5")
isfile(file) && (rm(file))
h5open(file,"w") do fid
    fid["rate"] = data[1,:,:,:]
    fid["cv"]   = data[2,:,:,:]
    fid["soma"] = data[3,:,:,:]
    fid["d1"]   = data[4,:,:,:]
    fid["d2"]   = data[5,:,:,:]
    fid["length_range"] = collect(ds)
    fid["rate_range"] = rates
end
