"""
This file reproduces Fig 6a, Fig6b, SI Fig 2,3,4.
It is necessary to run compute_dendritic_length.jl before it
"""
using RollingFunctions
using EllipsisNotation
include("../../../TripodNeuron/base/colors.jl")

## Read data
fid = read(h5open(joinpath(@__DIR__,"data/dendritic_length.h5"),"r") )

rate = fid["rate"]
cv = fid["cv"]
mydata = zeros(2,size(rate)...)
rate[isnan.(rate)] .=.0
cv[isnan.(cv)] .= 0.
mydata[1,..] = rate
mydata[2,..] = sqrt.(cv)

soma = fid["soma"]
d1 = fid["d1"]
d2 = fid["d2"]
membrane = zeros(3,size(soma)...)
membrane[1,..]=soma
membrane[2,..]=d1
membrane[3,..]=d2

ls = fid["length_range"]
rs = fid["rate_range"]
effs = fid["eff_range"]
## run analysis


function heatmaps(data, ls, rs)
    ps_rate = []
    ps_cv = []
    rate_extrema = (0,maximum(data[1,..]))
    cv_extrema = (0,maximum(data[2,..]))
    for x in 1:length(rs)
        p1 = heatmap(ls, ls, data[1,:,:,x], colorbar=false, xaxis=false, yaxis=false, clims=rate_extrema)
        annotate!(p1,[(400,400,text(string(round(rs[x], digits=1)),:white,8))])
        p2 = heatmap(ls, ls, data[2,:,:,x], colorbar=false, xaxis=false, yaxis=false, clims=cv_extrema)
        annotate!(p2,[(400,400,text(string(round(rs[x], digits=1)),:white, 8))])
        push!(ps_rate, p1)
        push!(ps_cv, p2)
    end
    p1 = plot(ps_rate..., layout=(4,10))
    p2 = plot(ps_cv..., layout=(4,10))
    max_rate = collect(0:0.1:maximum(data[1,..][:]))
    a = reshape(repeat(max_rate,2),(length(max_rate),2))'
    bar2 = heatmap(a, xticks=([1,length(max_rate)],[max_rate[1], max_rate[end]]), yaxis=false, colorbar=false, c=:amp)
    max_cv = collect(-1:0.1:maximum(data[2,..][:]))
    a = reshape(repeat(max_cv,2),(length(max_cv),2))'
    bar1 = heatmap(a, xticks=([1.,10.,length(max_cv)],[max_cv[1], 0., max_cv[end]]), yaxis=false, colorbar=false, c=:amp)
    bars = plot(bar2, bar1, layout=(2,1))
    return p1, p2, bars
end


function maxrates(data, ls, rs)
    diagonal = zeros(2,size(data)[end-1:end]...)
    for x in 1:size(data)[2]
        diagonal[:,x,:] .= data[:,x,x,:]
    end
    _rs = length(rs)
    max_rate = runmean.([map(x-> maximum(x), eachcol(data[1,:,:,r])) for r in 1:length(rs)],5)
    diag_rate = runmean.([diagonal[1,:,r] for r in 1:length(rs)],5)
    mean_rate = runmean.([map(x-> mean(x), eachcol(data[1,:,:,r])) for r in 1:length(rs)],5)
    mean_var = runstd.([map(x-> mean(x), eachcol(data[1,:,:,r])) for r in 1:length(rs)],5)

    pmax = plot()
    pmean = plot()
    pdiag = plot()

    CList = range(BLU, stop=RED,length=_rs)
    for r in 1:_rs
        pmax   =  plot!(pmax,  ls, max_rate[r], c=CList[r])
        pdiag  =  plot!(pdiag, ls, diag_rate[r], c=CList[r])
        pmean  =  plot!(pmean, ls, mean_rate[r], ribbon=mean_var[r], c=CList[r])
    end
    plot!(pmean, legend=false, ylabel="Spike rate (Hz)", xlabel="Dendritic length "*L"(\mu"*"m)", frame=:axes)
    plot!(pmax, legend=false, ylabel="Spike rate (Hz)", xlabel="Dendritic length "*L"(\mu"*"m)", frame=:axes)
    plot!(pdiag, legend=false, ylabel="Spike rate (Hz)", xlabel="Dendritic length "*L"(\mu"*"m)", frame=:axes)
    color_bar = reshape(repeat(1:_rs,2),(_rs,2))'
    colormap(L,c1=BLU, c2=RED) = range(c1, stop=c2,length=L)

    scatter!(pmax, ls[argmax.(max_rate)], maximum.(max_rate), c=CList, markerstrokecolor=CList, markersize = 5)
    heatmap!(
        color_bar,
        inset = (1, bbox(0.05, 0.85, 0.30, 0.05, :bottom, :right)),
        ticks = nothing,
        c = ColorGradient(colormap(_rs)),
        subplot = 2,
        title="Input rate(kHz)",
        titlefontsize=13,
        xticks=([1,_rs],round.(rs[[1,_rs]], digits=1)),
        colorbar = false,
        bg_inside = nothing,
        xscale=:log,
        xlims =(0.8,31.6),
        frame = :grid

    )
    scatter!(pdiag, ls[argmax.(diag_rate)], maximum.(diag_rate), c=CList, markerstrokecolor=CList, markersize = 5)


    heatmap!(
        color_bar,
        inset = (1, bbox(0.05, 0.85, 0.30, 0.05, :bottom, :right)),
        ticks = nothing,
        c = ColorGradient(colormap(_rs)),
        subplot = 2,
        title="Input rate(kHz)",
        titlefontsize=13,
        xticks=([1,_rs],round.(rs[[1,_rs]], digits=1)),
        colorbar = false,
        bg_inside = nothing,
        xscale=:log,
        xlims =(0.8,31.6),
        frame = :grid

    )
    scatter!(pmean, ls[argmax.(mean_rate)], maximum.(mean_rate), c=CList, markerstrokecolor=CList, markersize = 5)
    return pmax, pdiag, pmean
end




function best_coupling(data, ls, rs)

    _rs = length(rs)
    max_rate = [map(x-> argmax(x), eachcol(data[1,:,:,r])) for r in 1:length(rs)]

    CList = range(BLU, stop=RED,length=_rs)
    p = plot()
    for r in 1:_rs
        # p  =  plot!(ls,runmean(ls[max_rate[r]],3), c=CList[r], markerstrokecolor=CList[r])
        p  =  scatter!(ls,ls[max_rate[r]]./ls, c=CList[r], markerstrokecolor=CList[r])
        p  =  plot!(ls,ls[max_rate[r]]./ls, c=CList[r], markerstrokecolor=CList[r])
    end

    plot!(p, legend=false, ylabel="Best dendrititc length ratio", xlabel="Dendritic length "*L"(\mu"*"m)")

    color_bar = reshape(repeat(1:_rs,2),(_rs,2))'
    colormap(L,c1=BLU, c2=RED) = range(c1, stop=c2,length=L)
    heatmap!(
        color_bar,
        inset = (1, bbox(0.05, 0.85, 0.30, 0.05, :bottom, :right)),
        ticks = nothing,
        c = ColorGradient(colormap(_rs)),
        subplot = 2,
        title="Input rate(kHz)",
        titlefontsize=13,
        xticks=([1,_rs],round.(rs[[1,_rs]], digits=1)),
        colorbar = false,
        bg_inside = nothing,
        xscale=:log,
        xlims =(0.8,31.6),
        frame = :grid

    )
    return p
end

function fixed_dendrite(data, ls, rs; fixed_length::Int)
    models = data[:,fixed_length,:,:]
    _rs = length(rs)
    diag_rate = runmean.([models[1,:,r] for r in 1:length(rs)],5)
    CList = range(BLU, stop=RED,length=_rs)
    pdiag = plot()
    for r in 1:_rs
        pdiag  =  plot!(pdiag, ls, diag_rate[r], c=CList[r])
    end
    plot!(pdiag, legend=false, ylabel="Spike rate (Hz)", xlabel="Dendritic length "*L"(\mu"*"m)")
    scatter!(pdiag, ls[argmax.(diag_rate)], maximum.(diag_rate), c=CList, markerstrokecolor=CList, markersize = 5)

    color_bar = reshape(repeat(1:_rs,2),(_rs,2))'

    colormap(L,c1=BLU, c2=RED) = range(c1, stop=c2,length=L)
    heatmap!(
        color_bar,
        inset = (1, bbox(0.05, 0.85, 0.30, 0.05, :bottom, :right)),
        ticks = nothing,
        c = ColorGradient(colormap(_rs)),
        subplot = 2,
        title="Input rate(KHz)",
        titlefontsize=13,
        xticks=([1,_rs],round.(rs[[1,_rs]], digits=1)),
        colorbar = false,
        bg_inside = nothing
    )
    return pdiag
end


##

p1, p2, bars= heatmaps(mydata, ls, rs)
pmax, pdiag, pmean= maxrates(mydata, ls, rs)
plot!(pmax, legendfontsize=12, bglegend=:transparent, fglegend=:transparent,grid=false, guidefontsize = 18, tickfontsize=13)
plot!(pdiag, legendfontsize=12, bglegend=:transparent, fglegend=:transparent,grid=false, guidefontsize = 18, tickfontsize=13)
pbest = best_coupling(mydata, ls, rs)
# plot!(xscale=:log)
f(x) = 250/x
plot!(100:10:500,f.(100:10:500), c=:black,ls = :dash)
f(x) = 100/x
plot!(100:10:500,f.(100:10:500), c=:black, ls=:dash)
plot!(pbest, legendfontsize=12, bglegend=:transparent, fglegend=:transparent,grid=false, guidefontsize = 18, tickfontsize=13)

Plots.savefig(p1,   joinpath(@__DIR__,"rate_heatmap.pdf"))
Plots.savefig(p2,   joinpath(@__DIR__,"cv_heatmap.pdf"))
Plots.savefig(pmean,joinpath(@__DIR__,"mean_lines.pdf"))
Plots.savefig(pmax, joinpath(@__DIR__,"max_lines.pdf"))
Plots.savefig(pdiag, joinpath(@__DIR__,"diag_lines.pdf"))
Plots.savefig(pbest, joinpath(@__DIR__,"best_coupling.pdf"))


proximal = 6
long_proximal = 11
short_distal = 21
distal= 31
pprox = fixed_dendrite(mydata, ls, rs, fixed_length=long_proximal)
pprox = fixed_dendrite(mydata, ls, rs, fixed_length=proximal)
pshort = fixed_dendrite(mydata, ls, rs, fixed_length=short_distal)
pdistal = fixed_dendrite(mydata, ls, rs, fixed_length=distal)


rs[20]

p1 = heatmap(ls, ls, mydata[1,:,:,15], colorbar=true, c=:amp)
plot!(p1,ls,150*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,ls,300*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,ls,400*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,150*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
plot!(p1,300*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
plot!(p1,400*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
plot!(legend=false, tickfontsize=13, ylabel="Dendritic length "*L"(\mu m)",  xlabel="Dendritic length "*L"(\mu m)", guidefontsize=18, clabel="Spike Rate (Hz)")
savefig(p1,joinpath(@__DIR__,"1khzheatmap.pdf"))

##
p1 = heatmap(ls, ls, mydata[2,:,:,15], colorbar=true, clims=(0,1), c=:amp);
plot!(p1,ls,150*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,ls,300*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,ls,400*ones(length(ls)), ls=:dash, c=:black, lw=3);
plot!(p1,150*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
plot!(p1,300*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
plot!(p1,400*ones(length(ls)),ls, ls=:dash, c=:black, lw=3);
annotate!(p1,[(160,405,text("distal",:black,15, :bottom,:left ))])
annotate!(p1,[(160,305,text("short distal",:black,15, :bottom,:left ))])
annotate!(p1,[(160,155,text("proximal",:black,15, :bottom,:left ))])
plot!(legend=false, tickfontsize=13, ylabel="Dendritic length "*L"(\mu m)",  xlabel="Dendritic length "*L"(\mu m)", guidefontsize=18, clabel="Spike Rate (Hz)")
savefig(p1,joinpath(@__DIR__,"1khzheatmap_cv.pdf"))
