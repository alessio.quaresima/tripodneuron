"""
Compute the spiking rate of the tripod in all dendritic configurations
Synaptic input only on dendrites, and innhibition with same input rate than the excitation
"""
##
include("../../base.jl")
using HDF5
prox_medial ="H. 1->s, 150,4; 2->s, 300,4"
prox_distal ="H. 1->s, 150,4; 2->s, 400,4"
models = [prox_distal, prox_medial, TN.H_distal, TN.H_medial, TN.H_proximal, TN.H_ss]

function stimulus(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  ν1::Real,
                  ν2::Real)
    """ Poisson input train"""
      TN.set_rate!(tripod.d[1], ν1)
      TN.set_rate!(tripod.d[2], ν1)
      TN.set_rate!(tripod.d[1], -ν2)
      TN.set_rate!(tripod.d[2], -ν2)
end


function test_length(; ν1,ν2, model::String)
    voltage = TN.simulate_model(model, 15000, stimulus, ν1=ν1, ν2=ν2)
    r = TN.get_spike_rate(voltage)
    c = TN.get_cv(voltage)
    v = mean(voltage, dims=2)
    vs = std(voltage, dims=2)
    return [r c v... vs...]
end



function map_νspace(;νs, models)
    samples= 20
    data = zeros(8,  length(νs), length(νs), length(models)) ## rate/cv/voltage/soma/d1/d2
        Threads.@threads for n in eachindex(νs)
                            ν1 = νs[n]
                for (m,ν2) in enumerate(νs)
                    @show ν1, ν2
                    for (h, model) in enumerate(models)
                        out = [0. 0. 0. 0. 0. 0. 0. 0.]
                        fails = 0
                        for _ in 1:samples
                            try
                                out .+= test_length(ν1=ν1, ν2=ν2, model=model)
                            catch
                                fails +=1
                            end
                        end
                        if fails < samples
                            data[:,n,m, h] = out ./(samples - fails)
                        else
                            @show ν1, ν2, "fails"
                        end
                    end
                end
            end
    return data
end


## out = test_length(d1=200, d2=220, ν=1., eff=1.)
println("Dendrite length test")
println("#####################")
rates = 10 .^range(-1,2,length=40)
data = map_νspace(νs=rates,models=models)
file = joinpath(@__DIR__,"data","skewd_inh.h5")
isfile(file) && (rm(file))
h5open(file,"w") do fid
    fid["rate"] = data[1,:,:,:]
    fid["cv"]   = data[2,:,:,:]
    fid["soma"] = data[3,:,:,:]
    fid["d1"]   = data[4,:,:,:]
    fid["d2"]   = data[5,:,:,:]
    fid["std_soma"] = data[7,:,:,:]
    fid["std_d1"]   = data[7,:,:,:]
    fid["std_d2"]   = data[8,:,:,:]
    fid["rate_range"] = rates
end

#
