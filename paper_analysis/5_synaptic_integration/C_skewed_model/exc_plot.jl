"""
This file reproduce Fig. 6d
"""

using HDF5
using StatsBase
using Plots
using RollingFunctions
using EllipsisNotation
include("../../../TripodNeuron/base/colors.jl")
## Increase excitation and inhibition separatedly
fid = read(h5open(joinpath(@__DIR__,"data","skewd_exc.h5"),"r") )

rs = fid["rate_range"]
rate = fid["rate"]
cv = fid["cv"]
mydata = zeros(2,size(rate)...)
rate[isnan.(rate)] .=.0
cv[isnan.(cv)] .= -1.
mydata[1,..] = rate
mydata[2,..] = cv

cv[cv .<0.] .= 0.
sqrt.(cv)

soma = fid["soma"]
d1 = fid["d1"]
d2 = fid["d2"]
membrane = zeros(3,size(soma)...)
membrane[1,..]=soma
membrane[2,..]=d1
membrane[3,..]=d2

colormap(L,c1=BLU, c2=RED) = range(c1, stop=c2,length=L)
color_bar = reshape(repeat(1:length(rs),2),(length(rs),2))'
clist = colormap(length(rs), BLU,RED)
heatmap(rs, rs, rate[..,2], xscale=:log, yscale=:log, c=:amp)
heatmap(rs, rs, cv[..,1], xscale=:log, yscale=:log, c=:amp)


## plots
p1 = plot(rs,rate[..,1]', legend=false, c=clist, lw=2, xscale=:log, xlabel="Distal input (Hz)", ylabel="Spike rate (Hz)", guidefontsize=18, frame=:axes, tickfontsize=13)
plot!(p1,[rs[30], rs[30]], [0,100], lw=2, ls=:dash, c=:black )
p2 = plot(rs,rate[..,1:30,1], legend=false, c=clist, lw=2, xscale=:log, xlabel="Proximal input (Hz)", guidefontsize=18, frame=:axes, tickfontsize=13)
p2 = plot!(rs,rate[..,30:end,1], legend=false, c=clist[30:end], lw=2, ls=:dash, xscale=:log, xlabel="Proximal input (Hz)", guidefontsize=18, frame=:axes, tickfontsize=13)
heatmap!(
        color_bar,
        inset = (1, bbox(0.1, 0.15, 0.30, 0.05, :bottom, :right)),
        # ticks = nothing,
        yaxis=false,
        c = ColorGradient(colormap(length(rs))),
        subplot = 2,
        title="Other\n dendrite\n input (Hz)",
        titlefontsize=13,
        xticks=([1,length(rs)],["10e.5", "10e1.5"]),
        colorbar = false,
        bg_inside = nothing,
        xscale=:log,tickfontsize=10

    )
p = plot(p1, p2)

savefig(p, joinpath(@__DIR__, "skewd.pdf"))
heatmap(rs, rs, sqrt.(cv[..,1]), xscale=:log, yscale=:log, clims=(0,1))
p2 = plot!(xscale=:log, ylabel="Proximal input (Hz)", xlabel="Distal input (Hz)", guidefontsize=18, frame=:axes, tickfontsize=13)
annotate!([(0.4,10, Plots.text("Regular firing",:white,:left, 15)),(0.4,0.4,Plots.text("No firing",:white,:left,15))])
savefig(p2, joinpath(@__DIR__, "skewd_cv.pdf"))
p2
