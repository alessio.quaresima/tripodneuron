"""
This file reproduce Fig. 6d
"""

using HDF5
using StatsBase
using Plots
using RollingFunctions
using LaTeXStrings
using EllipsisNotation
include("../../../TripodNeuron/base/colors.jl")
pyplot()
## Read data

## Increase excitation and inhibition separatedly
fid = read(h5open(joinpath(@__DIR__,"data","skewd_inh.h5"),"r") )

myrate = fid["rate"]
cv = fid["cv"]
mydata = zeros(2,size(myrate)...)
myrate[isnan.(myrate)] .=.0
cv[isnan.(cv)] .= -1.
mydata[1,..] = myrate
mydata[2,..] = cv

soma = fid["soma"]
d1 = fid["d1"]
d2 = fid["d2"]
membrane = zeros(3,size(soma)...)
membrane[1,..]=soma
membrane[2,..]=d1
membrane[3,..]=d2

fid
rs = fid["rate_range"]
rs

file = joinpath(@__DIR__,"inhibitory_balance55.h5")

fid = h5open(file,"r")
νs = read(fid["freqs"])
labels = read(fid["labels"])
μmem = read(fid["mem"])
kie = read(fid["kie"])

# # colors= [:black TN.RED TN.BLU TN.GREEN TN.PURPLE]
# scatter(νs,kie["passive"], labels=labels, ylims=(-2,4), xscale=:log10, lc=:match, markersize=8, c=colors)
# scatter!(νs,kie["active"], labels="", ylims=(-2,4), xscale=:log10, markershape=:diamond, lc=:match, markersize=8, c=colors)
#
kie["active"]
dd = kie["passive"][:,4]
mm = kie["passive"][:,3]
pp = kie["passive"][:,2]
ss = kie["passive"][:,1]
dp = kie["passive"][:,5]
mp = kie["passive"][:,5]
kie_passive=hcat(dp,mp,dd,mm,pp,ss)


##
plots = []
titles=["distal - proximal", "sh.distal - proximal", "distal - distal","sh.dist - sh.dist", "proximal -proximal", "soma-only"]
# models = [prox_distal, prox_medial, TN.H_distal, TN.H_medial, TN.H_proximal, TN.H_ss]
for x in [3,4,5,1,2,6]
    p = heatmap(rs,rs, myrate[..,x]', xscale=:log, yscale=:log, frame=:axes, xrotation=-45,  colorbar=false,  title=titles[x],titlefontsize=13 )
    # plot!([0.1,100],[0.1,100], c=:black, lw=1, label="")
    # plot!([0.1,100],[0.1,100], ls=:dash,lc=:black, lw=2, label="")
    plot!(yaxis=false, xaxis=false)
    push!(plots, p)
    if x==3
        scatter!([0.2],[50],markershape=:cross, color=:white, legendfontcolor=:white, label="", markersize=10, legend=:left)
        annotate!([(0.3,50, text("-55 mV", :white, :left,12))])
    end
    if x <6
        annotate!([(95,0.1, text("max:\n "*string(round(Int,maximum(myrate[..,x])))*"Hz", :black, :right, :bottom,12))])
        scatter!(νs, νs .*kie_passive[:,x], ylims=(0.1,100),  markersize=7, label="", markershape=:cross, c=:white, xlims=(0.1,100))
    elseif x==6
        annotate!([(95,0.1, text("max:\n "*string(round(Int,maximum(myrate[..,x])))*"Hz", :white, :right, :bottom,12))])
        scatter!(νs ./4, νs./ 1.25 .*kie_passive[:,x], ylims=(0.1,100),  markersize=7, label="", markershape=:cross, c=:white, xlims=(0.1,100))
    end

end
# heatmap(rs, rs, cv[..,1], xscale=:log, yscale=:log)
plot!(plots[4], xlabel="                                                 Inhibiton (Hz)")
plot!(plots[4], ylabel="                     Excitation (Hz)")
plot!(plots[4], xaxis=true, yaxis=true)
p = plot(plots..., guidefontsize=18, tickfontsize=13, titlefontsize=13,frame=:grid)
savefig(p,joinpath(@__DIR__,"balance_rate.pdf"))
##

plots = []

titles=["prox-distal", "prox-sh.distal", "distal-distal","sh.dist-sh.dist", "prox-prox", "soma-only"]
# models = [prox_distal, prox_medial, TN.H_distal, TN.H_medial, TN.H_proximal, TN.H_ss]
for x in [3,4,5,6,1,2]
    p = heatmap(rs,rs, cv[..,x]', xscale=:log, yscale=:log, frame=:axes, xrotation=-45,  colorbar=false, c=:amp, title=titles[x],titlefontsize=12 )
    plot!([0.1,100],[0.1,100], c=:black, lw=1, label="")
    # plot!([0.1,100],[0.1,100], ls=:dash,lc=:black, lw=2, label="")
    push!(plots, p)
    annotate!([(100,0.1, text("max: "*string(round(maximum(cv[..,x]), digits=1)), :white, :right, :bottom,10))])
end
p = plot(plots..., guidefontsize=18, tickfontsize=10)
savefig(p,joinpath(@__DIR__,"balance_cv.pdf"))

# plot(myrate[..,1]', legend=false)
# plot(rate[..,2]', legend=false)

plots = []
# models = [prox_distal, prox_medial, TN.H_distal, TN.H_medial, TN.H_proximal, TN.H_ss]
for x in [1,2,6,3,4,5]
    p = heatmap(rs,rs, soma[..,x]', xscale=:log, yscale=:log, frame=:axes, xrotation=-45,  colorbar=false, c=:amp, title=titles[x],titlefontsize=12 )
    plot!([0.1,100],[0.1,100], c=:black, lw=1, label="")
    # plot!([0.1,100],[0.1,100], ls=:dash,lc=:black, lw=2, label="")
    push!(plots, p)
    annotate!([(100,0.1, text("max: "*string(round(maximum(soma[..,x]), digits=1))*"mV", :white, :right, :bottom,10))])
end
p =plot(plots..., guidefontsize=18, tickfontsize=10)
savefig(p,joinpath(@__DIR__,"balance_soma.pdf"))
