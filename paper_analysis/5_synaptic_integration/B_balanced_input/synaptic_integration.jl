"""
Reproduce Fig 7a, run inhibitory_balance.jl before
"""
include("../../base.jl")
using StatsBase
using HDF5
using RollingFunctions
using Plots
# %% codecell
SIMTIME = 50000
INITTIME= 1000


dd() = TN.Tripod(TN.H_distal_distal);
dp() = TN.Tripod(TN.H_distal_proximal);
pp() = TN.Tripod(TN.H_proximal_proximal);
mm() = TN.Tripod(TN.H_medial)
mp() = TN.Tripod(TN.H_medial_proximal)
ss() = TN.Tripod(TN.H_ss);

function stimulation(;tripod::Union{Nothing,TN.Tripod}=nothing,
                        step,
                        dt,
                        d1=0., d2=0., s=0., Kie=0., kwargs...)
    ## baseline
    ## this stimulation set the soma to the desired voltage
            TN.inject_current(tripod.s, Ke_soma)
    # somatic input
            TN.set_rate!(tripod.s, s)
            TN.set_rate!(tripod.s, -Kie* s)
            TN.set_rate!(tripod.d[1], d1)
            TN.set_rate!(tripod.d[2], d2)
            TN.set_rate!(tripod.d[1], -Kie*d1)
            TN.set_rate!(tripod.d[2], -Kie*d2)

end;

### Now compute neuron statistics
function free_membrane(;νs, KIE, models)
    """Computes the voltage in the free membrane condition """
    n_mod = length(models)
    vs = zeros(size(νs)[1], n_mod, 2)
    indexs = length(νs)
    Threads.@threads for n in 1:indexs
        ν=νs[n]
        for (m, tm) in enumerate(models)
            (tm == ss)  && ( d1 = 0.; d2 = 0.; s= ν)
            (tm != ss) && (d1 = ν;d2 = ν;s= 0.)
            tm = tm()
            # dry run to remove transient
            _ = TN.simulate_nospike(tm, INITTIME, stimulation, d1=d1, d2=d2, s=s, Kie = KIE[n,m])
            voltage = TN.simulate_nospike(tm, SIMTIME, stimulation, d1=ν, d2=ν, s=s, Kie = KIE[n,m])
            vs[n, m, 1] = mean(voltage[1,:])
            vs[n, m, 2] = std(voltage[1,:])
            @show d1, s, vs[n,m,2]
        end
    end
    return vs
end

function correlation_lenght(voltage)
    avg = zeros(3)
    for comp in 1:3
        avg[comp] = findfirst(x->isless(x,exp(-1)),
                    autocor(voltage[comp,:], 0:10:4000, demean=true)) |> x-> isnothing(x) ? -1 : x
    end
    return avg
end

function non_free_membrane(;νs, KIE, models)
    """ Store information about the tripod state"""
    n_mod = length(models)
    gtot = zeros(size(νs)[1], n_mod, 2, 3)    #in_rate, model, mean/var compartment
    spikes = zeros(size(νs)[1], n_mod, 2)    #in_rate, model, out_rate/cv
    corr_time = zeros(size(νs)[1], n_mod, 3) #in_rate, model, compartment
    membrane = zeros(size(νs)[1], n_mod, 2, 3)    #in_rate, model, mean/var,  compartment
    indexs = length(νs)
    Threads.@threads for n in 1:indexs
        ν=νs[n]
        for (m, model) in enumerate(models)
            ## pick model values
            (model == ss)  && ( d1 = 0.; d2 = 0.; s= ν)
            (model != ss)  && (d1 = ν;d2 = ν;s= 0.)
            tm = model()

            ## compute values
            _, _, _= TN.simulate_tripod(tm, INITTIME, stimulation,
                d1=d1, d2=d2, s=s, Kie=KIE[n,m])
            voltage, currents, synapses= TN.simulate_tripod(tm, SIMTIME, stimulation,
                d1=d1, d2=d2, s=s, Kie=KIE[n,m], record_synapses=true)

            ## compute the spike rate to verigy the simulation did not diverge
            rate   = TN.get_spike_rate(voltage[1,:])
            @show d1, s, rate
            ###### Store it to compute the mean
            ## Total conductance. Synapses: Receptors, time, compartment0
            gtot[n, m, 1, :] .= sum(mean(synapses, dims=2), dims=1)[1,1,:]
            gtot[n, m, 2, :] .= std(mean(synapses, dims=2), dims=1)[1,1,:]

            ## spikes
            spikes[n, m ,1 ] = TN.get_spike_rate(voltage[1,:])
            spikes[n, m ,2 ] = TN.get_cv(voltage[1,:])

            ## membrane
            corr_time[n, m, :]  .= correlation_lenght(voltage)
            membrane[n, m, 1, :]  .= mean(voltage,dims=2)[:,1]
            membrane[n, m, 2, :]  .= std(voltage,dims=2)[:,1]
            end
        end
    return gtot, spikes, corr_time, membrane
end


##

labels = ["ss" "pp" "mm" "dd" "dp" "mp"]
models = [ss, pp, mm, dd, dp, mp]

file = joinpath(@__DIR__,"inhibitory_balance.h5")
fid = h5open(file,"r")
νs = read(fid["freqs"])
labels = read(fid["labels"])
μmem = read(fid["mem"])
kie_active = read(fid["kie_active"])
kie_passive = read(fid["kie_passive"])
Ke_soma=kie["soma"]
##
#Passive soma
vs = free_membrane( νs = νs, KIE=kie_passive, models=models);
gtot, spikes, corr_time, membrane= non_free_membrane( νs=νs, KIE=kie_active, models=models);

## Active soma
vs = free_membrane( νs = νs, KIE=kie_active, models=models);
gtot, spikes, corr_time,    membrane= non_free_membrane( νs=νs, KIE=kie_active, models=models);

##
labels= [  "distal - distal" "sh.distal - sh.distal" "proximal-proximal"  "distal - proximal" "sh.distal-proximal" "soma only"]
colors = [TN.color_list[1:5]... :black]

_spikes= view(spikes, :,[4,3,2,5,6,1],:)
_vs= view(vs, :,[4,3,2,5,6,1],:)
z = map(x->runmean(_spikes[:,x,1],3), 1:6)
p1 = plot(νs,map(x->runmean(_vs[:,x,1],3), 1:6), xscale=:log, labels=labels, lw=2, c=colors, label=labels, xlims=(0.07,95e3), xticks=[0.1,1,10,100], xlabel="Input rate (kHz)")
p2 = plot(z,map(x->runmean(_vs[:,x,2],3), 1:6), labels=labels, lw=2, xlabel="Spike rate (Hz)", alpha=reverse(1/31:1/31:1), c= colors, legend=false, ylabel="         Soma Membrane Std")
# spikes[end,4,1]=0.
p=plot(p1,p2,layout=(2,1), frame=:axes,bglegend=:white, guidefontsize=18, tickfontsize=13)
savefig(p, joinpath(@__DIR__, "figures","Fig7a.pdf"))
