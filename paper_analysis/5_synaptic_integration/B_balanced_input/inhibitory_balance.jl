"""
Compute inhibitory balance. Very long computation because it's particle swarm optimization
"""
include("../../base.jl")
using StatsBase
using HDF5
using EllipsisNotation
using Optim


# %% codecell
SIMTIME = 10000
INITTIME= 1000


dd() = TN.Tripod(TN.H_distal_distal);
dp() = TN.Tripod(TN.H_distal_proximal);
pp() = TN.Tripod(TN.H_proximal_proximal);
mm() = TN.Tripod(TN.H_medial)
ss() = TN.Tripod(TN.H_ss);

function stimulation(;tripod::Union{Nothing,TN.Tripod}=nothing,
                        step,
                        dt,
                        d1=0., d2=0., s=0., Kie=0., kwargs...)
    ## baseline
    ## this stimulation set the soma to the desired voltage
            TN.inject_current(tripod.s, Ke_soma)
    # somatic input
            TN.set_rate!(tripod.s, s)
            TN.set_rate!(tripod.s, -Kie* s)
            TN.set_rate!(tripod.d[1], d1)
            TN.set_rate!(tripod.d[2], d2)
            TN.set_rate!(tripod.d[1], -Kie*d1)
            TN.set_rate!(tripod.d[2], -Kie*d2)

end;

# %% markdown
# Set the input on the soma as such the membrane is stable around -55 mv#
# compute the input inhibitory rate necessary for it
# %% codecell

function _compute_voltage(;model::Function,μ_mem::Real=-55,ν::Real, Kie::Float64, v::Bool=false)
    (model == ss)  && ( d1 = 0.; d2 = 0.; s= ν)
    (model != ss) && (d1 = ν;d2 = ν;s= 0.)
    tm  = model()
    _ = TN.simulate_nospike(tm, INITTIME, stimulation, d1=d1, d2=d2, s=s, Kie=Kie )
    voltage = TN.simulate_nospike(tm, SIMTIME, stimulation, d1=d1, d2=d2, s=s, Kie=Kie )
    μ_computed = mean(voltage[1,:])
    (v) && (return voltage)
    target = abs(μ_mem - μ_computed)

    ## correct to 0.1%
    return target
end;
#
# f(x) = _compute_voltage(;model=ss,μ_mem=-55, ν=10., Kie=x[1] )
# k = optimize(f,[1.],ParticleSwarm(n_particles=10))
#

function _compute_voltage_noinput(x, μ_mem)
    global Ke_soma = x
    tripod  = ss()
    _ = TN.simulate_nospike(tripod, INITTIME, stimulation)
    voltage = TN.simulate_nospike(tripod, SIMTIME, stimulation)
    μ_computed = mean(voltage[1,:])
    target = abs(μ_mem - μ_computed)
    return target
end

function compute_min_rate(;μ_mem::Real)
    f(x) = _compute_voltage_noinput(x[1], μ_mem)
    Ke = optimize(f,[1000.], ParticleSwarm()).minimizer
    return Ke
end

function balance_over_input_rate(; model::Function, νs, μ_mem::Real)
    inputs = length(νs)
    kies =zeros(inputs)
    name = String(Symbol(model))
    Threads.@threads for i in 1:inputs
        f(x) =  _compute_voltage(;model=model,ν=νs[i], μ_mem=μ_mem, Kie=x[1] )
        result = optimize(f,[1.],ParticleSwarm())

        @show name, νs[i], result.minimizer, result.minimum
        kies[i] = result.minimizer[1]
    end
    return kies
end

function inhibitory_balance(;models, νs, μ_mem)
    KIE = []
    for m in models
        push!(KIE, balance_over_input_rate(model=m, νs=νs, μ_mem=μ_mem))
    end
    return hcat(KIE...)
end

models = [dd, mm, pp, dp, mp, ss]
νs = 10 .^(-1.:0.1:log10(100))
μmem = -55

println("Equilibrium values for passive soma")
Ke_soma=0.
println("Ke_soma: ", Ke_soma)
kie_passive=inhibitory_balance(models=models, νs=νs, μ_mem=μmem )
println("Equilibrium values for active soma")
ke = compute_min_rate(;μ_mem = μmem)
println("Ke_soma: ", Ke_soma)
kie_active=inhibitory_balance(models=models, νs=νs, μ_mem=μmem)

file = joinpath(@__DIR__,"inhibitory_balance"*string(abs(μmem))*".h5")
isfile(file) && (rm(file))
h5open(file,"w") do fid
    fid["freqs"] = νs
    fid["labels"] = labels
    fid["mem"] =μmem
    kie = create_group(fid,"kie")
    kie["passive"] = kie_passive
    kie["active"] = kie_active
    kie["soma"]   = Ke_soma
end;
