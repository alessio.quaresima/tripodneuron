using Gadfly, Statistics,Cairo
include("analysis.jl")

data = import_data()

## Rate depends on the basal_eff only if the conductance is low
p1 = Gadfly.plot(filter(row-> row.st_type=="AND" && row.conductance =="high", data),x=:vrange, y=:rate, xgroup=:coupling, ygroup=:basalrate, color=:basal_eff,  Geom.SubplotGrid(free_y_axis=true, Geom.line))
p1 = Gadfly.plot(filter(row-> row.st_type=="OR" && row.conductance =="high", data),x=:vrange, y=:rate, xgroup=:coupling, ygroup=:basalrate, color=:basal_eff,  Geom.SubplotGrid(free_y_axis=true, Geom.line))
# p1 = Gadfly.plot(data,x=:basal_eff, y=:rate, xgroup=:coupling, ygroup=:basalrate, color=:vrange,  Geom.SubplotGrid(free_y_axis=true, Geom.line))
                              # layer(filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="high"  && row.st_type=="OR", data), line_style=:dashed, Geom.LineGeometry),
                              # layer(filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="low"  && row.st_type=="OR", data), line_style=:dashed, Geom.LineGeometry)))
                        # ,
                          # Guide.xlabel("Xlabel"), Guide.ylabel("Ylabel"))
# p1 = Gadfly.plot(data,x=:basal_eff, y=:rate, xgroup=:coupling, ygroup=:basalrate, color=:vrange,  Geom.SubplotGrid(free_y_axis=true, Geom.line))
                              # layer(filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="high"  && row.st_type=="OR", data), line_style=:dashed, Geom.LineGeometry),
                              # layer(filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="low"  && row.st_type=="OR", data), line_style=:dashed, Geom.LineGeometry)))
                        # ,
                          # Guide.xlabel("Xlabel"), Guide.ylabel("Ylabel"))

                          # filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="high", data)

high_cond = filter(row->mod(row.vrange,2)==0 && row.basalrate=="low" && row.conductance=="high", data)
low_cond1 = filter(row->mod(row.vrange,2)==0 &&row.conductance=="low" && row.basalrate=="high",  data)
low_cond2 = filter(row->mod(row.vrange,2)==0 &&row.conductance=="high" && row.basalrate=="high",  data)
p1 = Gadfly.plot(data, xgroup=:coupling, ygroup=:st_type,  x=:basal_eff, y=:rate,color=:vrange,Geom.SubplotGrid(layer(low_cond1, Geom.LineGeometry)))
p2 = Gadfly.plot(data, xgroup=:coupling, ygroup=:st_type,  x=:basal_eff, y=:rate,color=:vrange,Geom.SubplotGrid(layer(low_cond2, Geom.LineGeometry)))
vstack(p1,p2)

p1 = Gadfly.plot(data, xgroup=:coupling,  x=:vrange, y=:rate,color=:st_type, line_style=:dot,
                        Geom.LineGeometry,Geom.Coord.Cartesian(xmax=10))
                        # layer(filter( row->row.basal_eff == 15, low_cond), Geom.LineGeometry)))
p2 = Gadfly.plot(data, xgroup=:coupling, ygroup=:conductance,  x=:vrange, y=:rate,color=:st_type,Geom.SubplotGrid(layer(high_cond, Geom.LineGeometry)) )
vstack(p1,p2)

# p1 = Gadfly.plot(filter(row->mod(row.vrange,2)==0 && row.basalrate=="high"  && row.st_type=="OR", data), x=:basal_eff,

y=:rate, color=:vrange, xgroup=:coupling, ygroup=:conductance, Geom.SubplotGrid(free_y_axis=true,Geom.LineGeometry), Guide.xlabel("Basal input efficacy"), Guide.ylabel("Spike Rate (Hz)"))


p = title(vstack(p1,p2), "Spike rate depends on conductance")

draw(PDF("test.pdf"),p)

low = filter(row->mod(row.basal_eff,2)==0 && row.basalrate=="low" && row.conductance=="high", data)
my_data = filter(row -> !isnan(row.soma_exc_voltage), data)
low = by(data, [:coupling, :vrange, :st_type, :conductance], :cv=>mean)
Gadfly.plot(low, x=:vrange, y=:cv_mean, color=:st_type, ygroup=:coupling, xgroup=:conductance, Geom.SubplotGrid(Geom.line))

high = filter(row->mod(row.basal_eff,2)==0 && row.basalrate=="low" && row.conductance=="high", data)
my_data = filter(row -> !isnan(row.soma_exc_voltage), data)
# low = by(data, [:coupling, :vrange, :st_type, :conductance], :=>mean)
Gadfly.plot(, x=:vrange, y=:, color=:st_type, ygroup=:coupling, xgroup=:conductance, Geom.SubplotGrid(Geom.line))

controlled_spike =filter(row->mod(row.basal_eff,5)==0 && row.coupling!="null" && row.conductance=="high", data)
Gadfly.plot(controlled_spike, x=:rate, y=:soma_volt, color=:coupling, ygroup=:conductance, xgroup=:st_type, Geom.SubplotGrid(Geom.line,free_y_axis=true,free_x_axis=true))

no_spike =by(filter(row->mod(row.basal_eff,1)==0 && row.rate==0 && row.conductance=="high", data), [:vrange, :basal_eff,:st_type],:soma_volt=>mean)
Gadfly.plot(no_spike, x=:vrange, y=:soma_volt_mean, color=:basal_eff, xgroup=:st_type, Geom.SubplotGrid(Geom.line,free_y_axis=true,free_x_axis=true))

first_spike =filter(row->row.basal_eff==10 && row.vrange<3 && row.basalrate=="high" , data)
Gadfly.plot(first_spike, x=:vrange, y=:rate, color=:st_type, ygroup=:coupling, xgroup=:conductance, Geom.SubplotGrid(Geom.line,free_y_axis=true))

first_spike =filter(row->row.basal_eff==10 && row.vrange<3 && row.basalrate=="low" , data)
Gadfly.plot(first_spike, x=:vrange, y=:rate, color=:st_type, ygroup=:coupling, xgroup=:conductance, Geom.SubplotGrid(Geom.line,free_y_axis=true))

## This plot show us that the high conductance state is necessary to have the AND OR effect.
difference_AND = filter(row->row.st_type=="AND",data)
difference_OR = filter(row->row.st_type=="OR",data)
difference_AND[!,:sr_diff] = (difference_AND[:rate] - difference_OR[:rate])
difference_AND[:sr_diff] = difference_AND[:sr_diff]/(1 .+difference_OR[:rate])

Gadfly.plot(difference_AND, x=:vrange, y=:sr_diff, color=:basal_eff, ygroup=:coupling, xgroup=:conductance, Geom.SubplotGrid(Geom.line,free_y_axis=true))


apical = filter(row->row.coupling=="apical" &&row.basalrate=="high",data)
Gadfly.plot(apical, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:cv,color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(apical, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:rate, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(apical, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:d1_volt, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(apical, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:soma_exc_voltage, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))

soma = filter(row->row.coupling=="soma" &&row.basalrate=="high",data)
Gadfly.plot(soma, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:cv,color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(soma, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:rate, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(soma, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:d1_volt, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
Gadfly.plot(soma, xgroup=:conductance, ygroup=:st_type,  x=:vrange, y=:soma_exc_voltage, color=:basal_eff,Geom.SubplotGrid(Geom.LineGeometry, free_y_axis=true))
no_spike =filter(row-> row.rate==0,  data)
