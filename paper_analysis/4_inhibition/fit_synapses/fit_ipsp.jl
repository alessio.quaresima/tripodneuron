"""
This code fits the parameters of Soma and Dendrite GABAa to the IPSP
reported in Miles 1996.

It recreates a stimuli consistent with the paper methods and use it to fit the IPSP.
"""
###
include("../../../TripodNeuron.jl")
using Plots
using HDF5
using LsqFit
using Bootstrap
pyplot()


MODEL= TN.TripodModel(TN.H_distal)
tripod = TN.Tripod(MODEL)

function base(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
      TN.inject_current(500, tripod.s)
      TN.inject_current(200, tripod.d[1])
      TN.inject_current(200, tripod.d[2])
end
function protocol_dend(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
    TN.inject_current(500, tripod.s)
    TN.inject_current(200, tripod.d[1])
    TN.inject_current(200, tripod.d[2])
    if step ==SPIKETIME
      TN.inh_spike!(tripod.d[1], eff=10.)
    end
end
function protocol(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)

                  TN.inject_current(500, tripod.s)
    if step ==100
        TN.inh_spike!(tripod.d[1],eff= 10.)
        TN.inh_spike!(tripod.s,eff= 10.)
    end
    # TN.set_rate(140., -0.5, tripod.d[2],dt)
end

function protocol_soma(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
    TN.inject_current(500, tripod.s)
    TN.inject_current(200, tripod.d[1])
    TN.inject_current(200, tripod.d[2])

  if step ==SPIKETIME
      TN.inh_spike!(tripod.s,eff=10.)
  end
end
## Run Tripod and check graphically the fit
    # Run the simulation ove an interval of 127 ms, to reproduce miles timeframe
file = joinpath(@__DIR__,"miles_data.h5")
fid = h5open(file,"r")
miles_range = read(fid["x"])
soma_data = read(fid["soma"])
dendrite_data = read(fid["dend"])
close(fid)


xrange= 0.1:0.1:127
SPIKETIME = 260 # 27ms

voltage, currents,spikes, synapses, _ = TN.simulate_neuron(tripod, protocol_dend; record_synapses=true, sim_time=1000)
voltage, currents,spikes, synapses, _ = TN.simulate_neuron(tripod, protocol_dend; record_synapses=true, sim_time=127)
s = plot(xrange,.+ voltage[1,:] .- voltage[1,1], color=TN.RED, label="")
plot!(miles_range, dendrite_data, color=TN.RED, linestyle=:dash, label="")
voltage, currents,spikes, synapses, _ = TN.simulate_neuron(tripod, protocol_soma; record_synapses=true, sim_time=127)
s = plot!(xrange,.+ voltage[1,:] .- voltage[1,1], color=TN.BLU, label="")
plot!(miles_range, soma_data, color=TN.BLU,linestyle=:dash, label="")

plot!(s, [],[],color=:black, linestyle=:dash, label = "Miles et al. 1996");
plot!(s, [],[],color=TN.RED, linestyle=:solid, label = "Dendritic spike")
plot!(s, [],[],color=TN.BLU, label = "Perisomatic spike");
s = plot!(s,[50,70], [.25, .25], color=:black, label=false);
s = plot!(s,[130,130], [0.25, -0.75], color=:black, label=false)
annotate!([(133,-0.25, Plots.text("1 mV",:left, 18)),(60,.40,Plots.text("20 ms",19))])
plot!(xlims=(0,155), ylims=(-1.5, 0.9))
# annotate!([(1700,-40, Plots.text("Dendrite")),(1700,-50,Plots.text("Soma"))]);
# plot!(s,xlims=(1500,2600), ylims=(-55,-48))
plot!(s,xaxis=false, yaxis=false, legend=:topright, legendfontsize=13);
plot(s, linewidth=3.)

## Fit parameters

tripod_range= 0.1:0.1:127

sample_points_miles = map(x->argmin(abs.(miles_range .- x)), 27:120)
sample_points_tripod = map(x->argmin(abs.(tripod_range .- x)), 27:120)

function dend_func(xx,p)
    tripod = TN.Tripod(TN.H_distal_distal)
    tripod.d[1].syn.GABAa.τr⁻ = 1/p[1]
    tripod.d[1].syn.GABAa.τd⁻ = 1/p[2]
    tripod.d[1].syn.GABAa.gsyn = p[3]*TN.norm_synapse(p[1],p[2])
    @show p
    _, _,_, _, _ = TN.simulate_neuron(tripod, base; record_synapses=false, sim_time=1000)
    voltage, currents,spikes, synapses, no = TN.simulate_neuron(tripod, protocol_dend; record_synapses=false, sim_time=127)
    return voltage[1,xx] .- voltage[1,1]
end

function soma_func(xx,p)
    tripod = TN.Tripod(TN.H_distal_distal)
    tripod.s.syn.GABAa.τr⁻ = 1/p[1]
    tripod.s.syn.GABAa.τd⁻ = 1/p[2]
    tripod.s.syn.GABAa.gsyn = p[3]*TN.norm_synapse(p[1],p[2])
    @show p
    _, _,_, _, _ = TN.simulate_neuron(tripod, base; record_synapses=false, sim_time=1000)
    voltage, currents,spikes, synapses, no = TN.simulate_neuron(tripod, protocol_soma; record_synapses=false, sim_time=127)
    return voltage[1,xx] .- voltage[1,1]
end

# soma[sample_points_miles]
sgaba = TN.Esyn_soma.GABAa
dgaba = TN.Esyn_dend.GABAa
base_soma = [1/sgaba.τr⁻, 1/sgaba.τd⁻, sgaba.gsyn/TN.norm_synapse(sgaba)]
base_dend = [1/dgaba.τr⁻, 1/dgaba.τd⁻, dgaba.gsyn/TN.norm_synapse(dgaba)]
lower = [0.1, 0.1, 0.1]
upper = [40., 40., 40.]
fit_soma(soma_data) = LsqFit.curve_fit(soma_func,sample_points_tripod, soma_data[sample_points_miles], base_soma, lower=lower, upper=upper).param
fit_dend(dend_data) = LsqFit.curve_fit(dend_func, sample_points_tripod, dend_data[sample_points_miles], base_dend, lower=lower, upper=upper).param

##
fit_sol_dend = fit_dend(dendrite_data)
fit_sol_soma = fit_soma(soma_data)
soma_bootstrap = bootstrap(fit_soma, soma_data, BasicSampling(50))
dend_bootstrap = bootstrap(fit_dend, dendrite_data, BasicSampling(50))

plot(dend_func(sample_points_tripod,fit_sol_dend), c = TN.RED)
scatter!(dendrite[sample_points_miles],c= TN.RED)
plot!(soma_func(sample_points_tripod,fit_sol_soma), c =TN.BLU)
scatter!(soma[sample_points_miles], c =TN.BLU)

serialize(joinpath(@__DIR__,"fit","soma.so"),soma_bootstrap)
serialize(joinpath(@__DIR__,"fit","dend.so"),dend_bootstrap)
