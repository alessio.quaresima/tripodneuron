include("../base.jl")
using Plots
using Printf


"""
This file measures effect of interaction between excitationa nd inhibition in time, attempting to reproduce the results in Koch.
This file reproduces Fig 5a

The effect largely depends on the base-line current. For consistency. we inject a steady current of 100 nA in the soma.

"""
nmda_curr(v) =-(v-syn.NMDA_rev)*(1+ 1/syn.NMDA_b *exp(syn.NMDA_k*v))^-1 ##NMDA
nmda_curr_corr(v) =-(v-syn.NMDA_rev)*(1+(1/syn.NMDA_b)*exp(syn.NMDA_k*v))^-1

models= TN.H_distal_distal, TN.H_proximal_proximal, TN.H_medial, TN.H_ss
MODEL =  "H. 1->3,400,4; 2->3, 400,4; 3->s, 150,4"

EXCSPIKETIME = 500
INPUT_SPIKE = 10.
v,_,_,_,_ = TN.run_simulation(base_rate; tripod=TN.H_ss);
REST = mean(v[1,EXCSPIKETIME:end])

## Maintain it with an external current
function base_rate(;tripod::TN.Tripod,step::Int,dt::Float64)
        TN.inject_current(100., tripod.s)
end

function excitation_only(;tripod,step,dt)
        if step<300
                for d in tripod.d
                        d.v=REST
                end
                tripod.s.v=REST
        end
        base_rate(tripod=tripod,step=step,dt=dt)
        if step == EXCSPIKETIME
                TN.exc_spike!(tripod.d[1],eff=INPUT_SPIKE)
        end
end

function inh_onpath(;tripod, step, dt, Δ,compartment,gi_factor)
        if step<300
                for d in tripod.d
                        d.v=REST
                end
                tripod.s.v=REST
        end
        # if tt< EXCSPIKETIME
        base_rate(tripod=tripod,step=step,dt=dt)
        if compartment >0
                if step == EXCSPIKETIME + round(Int,Δ/dt)
                        TN.inh_spike!(tripod.d[compartment],eff=gi_factor*INPUT_SPIKE)
                end
        elseif compartment == 0
                if step == EXCSPIKETIME + round(Int,Δ/dt)
                        TN.inh_spike!(tripod.s,eff=gi_factor*INPUT_SPIKE)
                end
		end
        if step == EXCSPIKETIME
                TN.exc_spike!(tripod.d[1],eff=INPUT_SPIKE)
        end
end

## Computer the EPSP
function get_EPSP(v::Array{Float64,2}, spiketime=-1)
	spiketime = spiketime < 0 ? EXCSPIKETIME : spiketime
    return maximum(v[1,spiketime-1:end]) - REST
end

function single_run(compartment, delay, gi_factor; model::String, doplot=true, exc=0.)
		if doplot
	        p= plot(30 .+ v[1+mycomp,:], color = TN.RED)
	        p= plot!(p,+ v[1,:], color=TN.RED)
	        p= plot!(p,[1, 2000], [REST+get_EPSP(v), REST+get_EPSP(v)],linestyle=:dash)
		end
        # exc_int = get_EPSP_integral(v)
        # length(v[:,400:end])
		v = TN.simulate_model(model, 300, inh_onpath, Δ=delay, compartment=compartment,gi_factor=gi_factor)
        inh =get_EPSP(v)
        fano =(exc/inh)
		if doplot
	        p= plot!(p,30 .+ v[mycomp+1,:],color= TN.BLU)
	        p= plot!(p,+ v[1,:], color= TN.BLU)
	        p= plot!(p,[1, 2000], [REST+get_EPSP(v), REST+get_EPSP(v)],linestyle=:dash)
	        p= plot!(p,[EXCSPIKETIME-200, EXCSPIKETIME-200], [REST-5, REST+5],linestyle=:dash, color=:black)
	        p= plot!(p,[EXCSPIKETIME+1000, EXCSPIKETIME+1000], [REST-5, REST+5],linestyle=:dash, color=:black)
	        return plot!(xlim=(0,2000), title ="F: "*string(fano))
		else
	        return fano, false
		end
end

function F_factor(gi_factor,delay; compartments=0:2, model::String)
        m_=plot()
        EPSPS = []
		ls = [:solid,:solid,:dash,:solid]
        for (n,compartment) in enumerate(compartments)
                epsps = Array{Float64,1}(undef,delay.len)
                epsps_int = Array{Float64,1}(undef,delay.len)
				v = TN.simulate_model(model, 200, excitation_only)
			    exc = get_EPSP(v)
                for (n,Δ) in enumerate(delay)
						@printf "\r%d%%" (n/delay.len +compartment)/3*100
						fano, integral= single_run(compartment,Δ,gi_factor;model=model, doplot=false, exc=exc)
                        epsps[n] = fano
                        epsps_int[n] = integral
                end
                labels=["on-soma","on-excitation","on-distal","on-the-path"]
				labels = ["PV soma","SST on-exc.","SST off-path","SST on-path"]
				colors= [TN.BLU, TN.RED, TN.RED, :green]
                plot!(m_,delay,epsps[:], label=labels[n], color=colors[n], linestyle=ls[n], linewidth=2.)
                push!(EPSPS,epsps)
        end
        a=plot!(m_,legendtitle="Inhibition", ylabel="F factor",xlabel="delay over excitation (ms)")
        plot!(a,title="maximum EPSP")
		a = plot!(a, tickfontsize=13, guidefontsize=15,background_color=:transparent)
        return [a, EPSPS]
end


function get_fano(gi_factor; model, compartments=0:2)
    delay = -40:0.2:40
    myplot, epsps = F_factor(gi_factor,delay, model=model, compartments=compartments)
	return myplot
end
## test inhibition
@eval(TN, Esyn_dend = eyal_exc_synapse(dt,"dend"))
milesmm = get_fano(10., model=TN.H_medial)

@eval(TN, Esyn_dend = duarte_exc_synapse(dt,"dend"))
duartemm = get_fano(10., model=TN.H_medial)

plot(duartemm, title="", legend=false)
plot!(milesmm, xaxis=true, xlabel="")
onpath = plot(milesmm, duartemm, layout=(2,1), frame=:axes)

savefig(duartemm, joinpath(@__DIR__,"figures","Fig5a.pdf"))
