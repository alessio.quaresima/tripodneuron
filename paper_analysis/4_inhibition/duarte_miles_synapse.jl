"""
This file reproduce SIFig 1b
"""

include("../base.jl")
using LaTeXStrings


function protocol(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)

                  TN.inject_current(500, tripod.s)
    if step ==100
        TN.inh_spike!(tripod.d[1], eff=10.)
        TN.inh_spike!(tripod.s, eff=10.)
    end
    # TN.set_rate(140., -0.5, tripod.d[2],dt)
end
## miles
@eval(TN, Esyn_dend = eyal_exc_synapse(dt,"dend"))
voltage, currents,spikes, synapses_inh, _ = TN.run_simulation(protocol; tripod=TN.TripodModel(TN.H_distal_distal), record_synapses=true, sim_time=500)
s = plot(synapses_inh[3,:,2], color=TN.RED, linewidth=3., label="GABAa dendrite")
s = plot!(synapses_inh[4,:,2], linewidth=3,color=:black, label="GABAb")

@eval(TN, Esyn_dend = duarte_exc_synapse(dt,"soma"))

voltage, currents,spikes, synapses_inh, _ = TN.run_simulation(protocol; tripod=TN.TripodModel(TN.H_distal_distal), record_synapses=true, sim_time=500)
s = plot!(synapses_inh[3,:,2], color=TN.BLU , linestyle=:wsolid, linewidth=3, label="GABAa soma")

xticks!(0:500:2000,string.(0:50:200))
plot!(xlims=(0,2000), linewidth=3, ylabel= L"g(nS)",xlabel="time (ms)")

s = plot!(tickfontsize=13, guidefontsize=15,background_color=:transparent)

savefig(s, joinpath(@__DIR__,"figures","SIFig1b.pdf"))
