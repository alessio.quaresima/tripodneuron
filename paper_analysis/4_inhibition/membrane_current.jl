include("../base.jl")
using Plots
using Printf
using StatsPlots


"""
This file reproduces Fig 5b
"""

four_compartments =  "H. 1->3,400,4; 2->3, 400,4; 3->s, 150,4"
models = [TN.H_distal, TN.H_medial, TN.H_proximal]
EXCSPIKETIME = 500
INPUT_SPIKE = 1.
SIMTIME = 10000

function base_rate(;tripod::TN.Tripod,step::Int,dt::Float64)
        TN.set_rate!(tripod.s,0.1)
        TN.set_rate!(tripod.d[1],1.)
end

function inhibition(;tripod, step, dt, compartment, ν)
    base_rate(tripod=tripod,step=step,dt=dt)
    if compartment ==0
		TN.set_rate!(tripod.s,-ν)
	elseif compartment ==1
		TN.set_rate!(tripod.d[1],-ν)
	elseif compartment ==2
		TN.set_rate!(tripod.d[2],-ν)
	end
end

function target_inhibition(compartment, νs, model)
        m_=plot()
        n_=plot()
		tripod = TN.Tripod(model)
		comps = length(tripod.d)+1
		voltage = zeros(size(νs)[1], comps,2)
		current = zeros(size(νs)[1], comps,2)
		spikes = zeros(size(νs)[1], 2)
		for (n,ν) in enumerate(νs)
			v, c, _ = TN.simulate_tripod(tripod, SIMTIME, inhibition,  compartment=compartment, ν = ν )
			voltage[n, :,1] .= mean(v, dims=2)[:,1]
			current[n, :,1] .= mean(c[1:end-1,:], dims=2)[:,1]
			voltage[n, :,2] .= std(v, dims=2)[:,1]
			current[n, :,2] .= std(c[1:end-1,:], dims=2)[:,1]
			current[n, 1,1] = -current[n,1,1]
			# spikes[n,1] = s
			# spikes[n,2] = cv
		end
		return voltage, current
end

function inhibition_frequence(model)
	labels= ["soma" "dendrite" "dendrite"]
	lines = [:solid :solid :solid :solid :solid]
	lines_spike_plot = [:solid :solid :dash :solid :solid]
	colors = [TN.BLU TN.RED TN.RED TN.GREEN TN.BROWN]
	plots_volt = []
	plots_current = []
	plot_spikes = plot()
	for i in [2,0,1]
		lines = [:solid :solid :dash]
		νs = 0.1:0.2:8
		voltage , current = target_inhibition(i,νs, model)
		pv = plot(νs, voltage[:,:,1],ribbons=voltage[:,:,2],       linewidth=3, yticks=false,labels=labels, linestyle=lines,color=colors, legend=false)
		pv = plot!(νs, voltage[:,1,1],ribbons=voltage[:,1,2],       linewidth=3, yticks=false, linestyle=lines,color=colors, label="")
		pc = plot(νs, zeros(size(νs)), linewidth=2, color=:black, label="")
		pc = plot!(pc, νs, -current[:,:,1],ribbons=current[:,:,1], linewidth=3, yticks=false,labels=labels, linestyle=lines,color=colors, legend=false)
		pc = plot!(pc, νs, -current[:,1,1],ribbons=current[:,1,1], linewidth=3, yticks=false,labels=labels, linestyle=lines,color=colors, label="")
		if i ==0
			annotate!(pc, [(1,250,Plots.text("Depolarizing",13, :left))]);
			annotate!(pc, [(1,-250,Plots.text("Hyperpolarizing",13,:left))]);
		end
		push!(plots_volt,pv)
		push!(plots_current,pc)
	end
	plot(plots_volt[1], ylabel="Membrane potential (mV)", yticks=true, legend=true, xlabel="Inhibition rate (KHz)")
	membrane =plot(plots_volt..., layout =(1,3), frame=:axes, ylims=(-90,0))
	plot(plots_current[1], ylabel="Compartment current (pA)", legend=true,  yticks=(-2000:2000:2000, -2:2:2), ylims=(-2000,2000))
	currents = plot(plots_current..., layout=(1,3), frame=:axes)
	return  membrane, currents
end

##
plots = inhibition_frequence(TN.H_medial)

membrane = plot!(plots[1],tickfontsize=13, guidefontsize=18,background_color=:white, xlabel="", ylims=(-90,40))
currents = plot!(plots[2],tickfontsize=13, guidefontsize=18,background_color=:white, xaxis=true, title="", legend=false)

inh_= plot(membrane, currents, layout=(2,1))
savefig(inh_,joinpath(@__DIR__,"inhibition_rate.pdf"))
