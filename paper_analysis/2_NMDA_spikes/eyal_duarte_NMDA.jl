"""
This file reproduces Fig SI 1a
"""

include("../base.jl")
using Plots


NMDA_eyal = TN.eyal_exc_synapse(TN.dt, "dend")
NMDA_duarte = TN.duarte_exc_synapse(TN.dt, "dend")

function protocol(;tripod=nothing,
                  soma=nothing,
                  step::Int64,
                  dt::Float64, somaspike=false)

    if step == 1
    TN.exc_spike!(tripod.s,)
    TN.inh_spike!(tripod.s,)
    TN.exc_spike!(tripod.d[1])
    TN.inh_spike!(tripod.d[1])
    end
end

duration =150

TN.Esyn_dend.NMDA = NMDA_eyal.NMDA
voltage, currents,spikes, synapses_eyal, _ = TN.run_simulation(protocol; record_synapses=true, sim_time =duration)
TN.Esyn_dend.NMDA = NMDA_duarte.NMDA

voltage, currents,spikes, synapses_duarte, _ = TN.run_simulation(protocol; record_synapses=true, sim_time =duration)

ampa_max = maximum(synapses_duarte[1,:,1])

a =plot(TN.dt:TN.dt:duration,synapses_duarte[1,:,1] ./ampa_max,   linewidth=2, label = "AMPA channel");
a =plot!(a,TN.dt:TN.dt:duration,synapses_duarte[2,:,2] ./ampa_max,linewidth=2,color=:black, linestyle=:dash, label = "NMDA Mouse Barrel cortex");
a =plot!(a,TN.dt:TN.dt:duration,synapses_eyal[2,:,2] ./ampa_max,linewidth=2, color=:black, label = "NMDA Human L2/3")
plot!(a,  ylabel=L"Max. conductance \quad (g^{syn}_{AMPA})", xlabel="time (ms)",guidefontsize=18, tickfontsize=13, frame=:grid)
savefig(a,joinpath(@__DIR__,"figures","SI1a.pdf"))
