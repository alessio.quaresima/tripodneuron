"""
This file reproduces Fig 2a and Fig 2b
In this file we analyze the EPSP in four conditions:
	HUMAN physiology	HUMAN synapses
	MOUSE physiology    MOUSE synapses
	for all the dendritic lenghts

"""
##
include("../base.jl")
using Plots
using EllipsisNotation
pyplot()

## Define conditions
tripod_human(l) = "H. 1->s, $l, 4"
tripod_mouse(l) = "M. 1->s, $l, 4"

eyal = TN.eyal_exc_synapse(TN.dt,"dend")
duarte  = TN.duarte_exc_synapse(TN.dt,"dend")

EXCSPIKETIME= 300
function NMDA_Duarte(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
                 if step == EXCSPIKETIME
					 tripod.d[1].syn.NMDA.gsyn = duarte.NMDA.gsyn
                     TN.exc_spike!(tripod.d[1],eff=EFF)
                 end
end
function AMPA_only(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
                 if step == EXCSPIKETIME
					 tripod.d[1].syn.NMDA.gsyn = 0.
                     TN.exc_spike!(tripod.d[1],eff=EFF)
                 end
end
function NMDA_Eyal(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
                 if step == EXCSPIKETIME
					 tripod.d[1].syn.NMDA.gsyn = eyal.NMDA.gsyn
                     TN.exc_spike!(tripod.d[1],eff=EFF)
                 end
end


## simulate

"""
Get EPSP for all dendritic lengths
"""
function epsp_length(eff)
	ls = 100:500
	global EFF = eff
	EPSPs = zeros(3, length(ls))
	for l in eachindex(ls)
		model = tripod_human(ls[l])
		null_voltage, _,spikes, synapses, _ = TN.run_simulation(TN.null_input; tripod=model, record_synapses=true, sim_time=100)
		Duarte_voltage, _,_, _, _ = TN.run_simulation(NMDA_Duarte; tripod=model, record_synapses=true, sim_time=100)
		AMPA_voltage, _,_, _, _ = TN.run_simulation(AMPA_only; tripod=model, record_synapses=true, sim_time=100)
		Eyal_voltage, _,_, _, _ = TN.run_simulation(NMDA_Eyal; tripod=model, record_synapses=true, sim_time=100)
		AMPA_epsp = TN.get_EPSP(AMPA_voltage;spiketime=EXCSPIKETIME,rest=null_voltage[1,EXCSPIKETIME])
		Duarte_epsp = TN.get_EPSP(Duarte_voltage;spiketime=EXCSPIKETIME,rest=null_voltage[1,EXCSPIKETIME])
		Eyal_epsp = TN.get_EPSP(Eyal_voltage;spiketime=EXCSPIKETIME,rest=null_voltage[1,EXCSPIKETIME])
		EPSPs[:,l] = [AMPA_epsp, Duarte_epsp, Eyal_epsp]
	end
	return EPSPs
end


#Detect NMDA spikes
function get_maxima(data)
    arg_maxima = []
    for x in 2:length(data)-1
        (data[x] > data[x-1]) && (data[x]>data[x+1]) && (push!(arg_maxima,x))
    end
    return arg_maxima
end

"""
Get non-linear upswing position and size
"""
function detect_non_linearity(data)
	nmda_spikes = Array{Tuple,2}(undef,size(data)[1], size(data)[3])
	soma_spikes = Array{Tuple,2}(undef,size(data)[1], size(data)[3])
	max_value = 0.
	for model in 1:size(data)[1]
		for len in 1:size(data)[3]
			second_derivative = diff(diff(data[model,:,len]))
			second_derivative[second_derivative .< 0.1] .=0
			arg_max = get_maxima(second_derivative)
			if isempty(arg_max)
				nmda_spikes[model,len] = (0,0)
				soma_spikes[model,len] = (0,0)
			else
				if length(arg_max)<2
					arg = arg_max[1]
					epsp = data[model,arg+2,len] - data[model,arg,len]
					if epsp >10
						soma_spikes[model,len] = (arg, epsp)
					else
						soma_spikes[model,len] = (0,0)
					end
					nmda_spikes[model,len] = (arg, epsp)
				else
					arg = arg_max[1]
					epsp = data[model,arg+2,len] - data[model,arg,len]
					nmda_spikes[model,len] = (arg, epsp)
					arg = arg_max[2]
					epsp = data[model,arg+2,len] - data[model,arg,len]
					soma_spikes[model,len] = (arg, epsp)
				end
				@show model,arg_max[1], max
			end
		end
	end
	nmda_spikes, soma_spikes, max_value
end

## Plot for all length
EPSPs = epsp_length(50)
label=["Mouse (NAR = 0.25)" "AMPA-only (NAR = 0.)" "Human (NAR = 1.8)"]
p = plot(EPSPs[1:3,:]', frame=:axes, guidefontsize=18, label=label, tickfontsize=13,xlabel="Dendritic length "*L"(\mu m)", ylabel="EPSP amplitude (mV)", lw=3)
p = savefig(p,joinpath(@__DIR__,"figures","fig2a.pdf"))

## Compare EPSPs
"""
EPSP varying dendritic lenght and co-active synapses
for both mouse and human physiology
"""
ls = 100:10:500
effs = 1:1:200
d_samples = 1:5:41
stimuli = [NMDA_Duarte, AMPA_only, NMDA_Eyal]
EPSPs_h = zeros(3,length(effs),length(ls))
EPSPs_m = zeros(3,length(effs),length(ls))

# Get EPSP for Human
for e in eachindex(effs)
	for l in eachindex(ls)
		model = tripod_human(ls[l])
	 	global EFF = effs[e]
		null_voltage, currents,spikes, synapses, _ = TN.run_simulation(TN.null_input; tripod=model, record_synapses=true, sim_time=100)
		for s in eachindex(stimuli)
			stimulus = stimuli[s]
			voltage, currents,spikes, synapses, _ = TN.run_simulation(stimulus; tripod=model, record_synapses=true, sim_time=100)
			epsp = TN.get_EPSP(voltage;spiketime=EXCSPIKETIME,rest=null_voltage[1,EXCSPIKETIME], compartment=1)
			EPSPs_h[s,e,l] = epsp
		end
	end
end

# and Mouse physiology
for e in eachindex(effs)
	for l in eachindex(ls)
		model = tripod_mouse(ls[l])
	 	global EFF = effs[e]
		null_voltage, currents,spikes, synapses, _ = TN.run_simulation(TN.null_input; tripod=model, record_synapses=true, sim_time=100)
		for s in eachindex(stimuli)
			stimulus = stimuli[s]
			voltage, currents,spikes, synapses, _ = TN.run_simulation(stimulus; tripod=model, record_synapses=true, sim_time=100)
			epsp = TN.get_EPSP(voltage;spiketime=EXCSPIKETIME,rest=null_voltage[1,EXCSPIKETIME], compartment=1)
			EPSPs_m[s,e,l] = epsp
		end
	end
end
##
nmda_spikes, soma_spikes, max_value = detect_non_linearity(EPSPs_h)
p= plot()
plot!(p, 100:500, 50ones(length(100:500)), ls=:dash, lw=2, c=:black, label="")
for model in 1:size(EPSPs_h)[1]
	for len in 1:size(EPSPs_h)[3]
		if nmda_spikes[model,len][2] > 0.
			a,b, s = len,nmda_spikes[model,len][1], nmda_spikes[model,len][2]
			mc = TN.color_list[model]
			s = s > 20 ?  10. : 5s
			@show model,a,b,s
			scatter!(p,[ls[a]],[effs[b]], c=TN.color_list[model], markersize=s, markerstrokecolor=mc, label="")
		end
		if soma_spikes[model,len][2] > 0.
			a,b, s = len,soma_spikes[model,len][1], soma_spikes[model,len][2]
			mc = :black
			s = 10
			# 5*c/max_valu
			@show model,a,b,s
			scatter!(p,[ls[a]],[effs[b]], c=TN.color_list[model], markersize=s, markerstrokecolor=mc, label="")
		end
	end
end
plot!(p,xlabel="Dendritic length "*L"(\mu m)",
	ylabel="Co-active synapses", frame=:axes, xlims=(90,600)
)
scatter!([],[], c=TN.GREEN, markersize=15, label="Soma spike")
scatter!([],[], c=TN.GREEN, markersize=8, markerstrokecolor=TN.GREEN, label="NMDA spike")
plot!(legend=:topright, legendfontsize=13)

plot!(100:500,transpose(EPSPs),    inset = (1, bbox(0.0, 0.30, 0.40, 0.45, :bottom, :right)), xlims=(100,500),   subplot = 2, label=["NAR=0." "NAR=0.25" "NAR=1.8"], c=[TN.RED TN.BLU TN.GREEN], lw = 3, legendfontsize=12, bglegend=:white, fglegend=:transparent,grid=false, guidefontsize = 18, tickfontsize=13, frame=:axes, legend=false, legendtitle="GluRs", ylabel="EPSP (mV)", background=:transparent)
nonlinearpp = plot!(tickfontsize=13, guidefontsize=18, legendtitlefontsize=12 )

savefig(nonlinearpp, joinpath(@__DIR__,"figures","fig2b.pdf"))




#
# ##
# ls[15]
# p = plot(
#     EPSPs_h[:,:,18]',
# 	lw = 3,
#     color=TN.color_list,
#     bg_inside = :transparent,
#     frame=:axes,
# 	xlabel="Co-active synapses",
# 	ylabel="EPSP (mV)",
# 	label=["Mouse (NAR = 0.25)" "AMPA-only (NAR = 0.)" "Human (NAR = 1.8)"],
# 	legendtitle="GluRs",legendfontsize=12, legendtitlefontsize=12
#
# )
# p = plot!(tickfontsize=13, guidefontsize=18 )
# savefig(p, joinpath(@__DIR__,"epsp.pdf"))
#
# ##Plot three dimensional data
# clist = TN.colormap(length(d_samples), TN.RED, TN.GREEN)
#
# p = surface(ls,effs,EPSPs_h[3,:,:], label="",colorbar=false,title="Human, NAR=0.2", alpha=0.5,c=:grey)
# for n in eachindex(d_samples)
# 	x = d_samples[n]
# 	plot!(repeat([ls[x]],length(effs)),effs,[EPSPs_h[3,:,x]],lw=4, c=clist[n],	label="")
# end
# plot!(grid=false, background=:white,set_axis_off=true, lw=3)
# plot!(zlims=(0,40), xlabel="Dendritic \nlength"*L" (\mu m) ", ylabel="co-active synapses", zlabel="EPSP (mV)")
#
# plot!(repeat([ls[22]],length(effs)),effs,[EPSPs_h[3,:,22]],lw=4, c=:black,	label="distal threshold")
# plot!(camera= (40,10))
# savefig(p, joinpath(@__DIR__,"epsp_efficacy_h.pdf"))
#
# p = surface(ls,effs,EPSPs_m[2,:,:], label="",colorbar=false,title="Human, NAR=0.2", alpha=0.5,c=:grey)
# for n in eachindex(d_samples)
# 	x = d_samples[n]
# 	plot!(repeat([ls[x]],length(effs)),effs,[EPSPs_m[2,:,x]],lw=4, c=clist[n],	label="")
# end
# plot!(grid=false, background=:white,set_axis_off=true, lw=3)
# plot!(zlims=(0,40), xlabel="dendritic \nlength"*L" (\mu m) ", ylabel="co-active synapses", zlabel="EPSP (mV)")
# plot!(repeat([ls[22]],length(effs)),effs,[EPSPs_m[2,:,22]],lw=4, c=:black,	label="distal threshold")
# plot!(camera= (40,10))
#
# savefig(p, joinpath(@__DIR__,"epsp_efficacy_m.pdf"))
#
# ## Plot data for appendix
# pyplot()
# p1 = surface(ls,effs,EPSPs_h[2,:,:], label="",colorbar=false,title="Human, NAR=0.2", camera=(45,10))
# p2 = surface(ls,effs,EPSPs_h[3,:,:], label="",colorbar=false,title="Human, NAR=1.8", camera=(45,10))
# p3 = surface(ls,effs,EPSPs_m[2,:,:], label="",colorbar=false,title="Mouse, NAR=0.2", camera=(45,10))
# p4 = surface(ls,effs,EPSPs_m[3,:,:], label="",colorbar=false,title="Mouse, NAR=1.8", camera=(45,10))
# p = plot(p1,p2,p3,p4, layout=(2,2))
# plot!(xlabel="dendritic length"*L" (\mu m) ", ylabel="co-active synapses", zlabel="EPSP (mV)")
# using PyPlot
# tight_layout()
# savefig(p,joinpath(@__DIR__,"epsp_four_cases.pdf"))
# ## Section of previous plot for proximal, medial and distal dendrites
#
# clist= TN.colormap(3, TN.RED, TN.GREEN)
# p = plot(effs,EPSPs_m[2,:,[2,21,31]], color=clist, linestyle=[:solid :solid :solid], xlabel="Co-active synapses", ylabel="EPSP (mV)", label=["" "" ""],guidefontsize=15, linewidth=2, tickfontsize=13, title="Mouse")
# ylims!(p, (0,35))
# xlims!(p, (0,250))
# yticks!(0:10:30,string.(0:10:30))
# # annotate!([(215,33,Plots.text("Soma\nspike",11))]);
# s = plot(effs,EPSPs_h[3,:,[6,21,31]], color=clist, linestyle=[:solid :solid :solid], xlabel="Co-active synapses", ylabel="EPSP (mV)",guidefontsize=15, linewidth=2, tickfontsize=13, title="Human", label=["Proximal(150)" "Medial(300)" "Distal(400)"], legend=:bottomright)
# ylims!(s, (0,35))
# xlims!(s,(0,120))
# # annotate!([(75,33,Plots.text("Soma\nspike",11))]);
# # annotate!([(60,10,Plots.text("NMDA\nspike",11))]);
# yticks!(0:10:30,string.(0:10:30))
# ps = plot(s,p, layout=(1,2), frame=:axes)
# # plot!([[],[]], label=["Human" "Mouse"], linestyle=[:solid :dash ], color=:black, linewidth=2, frame=:grid)
# savefig(ps,joinpath(@__DIR__,"EPSP_section.pdf"))
#
# ## dsf
# # p = plot!(p,[[],[]], label=["mouse" "human"], color=:black, linestyle=[:dash :solid], linewidth=2, legend=:topleft)
