include("../base.jl")
using Plots
pyplot()
##
"""This file reproduces Fig 3a and Fig 3b"""
function single_spike(;tripod::TN.Tripod, step::Int,dt::Float64, intensity::Int)
    if step==1
        TN.exc_spike!(tripod.d[1],eff=intensity)
    end
    # TN.exc_spike(tripod.d[1],0.05)
end

function NMDA_spikes(;color,axis=false, delay=0, model=MODEL, my_range = 0:15:200)
    s=plot()
    p=plot()
    above_threshold = Vector()
    end_threshold = Vector()
	CList = reshape( range(TN.BLU, stop=TN.RED,length=length(my_range)), 1,length(my_range) );

    for (n,intensity) in enumerate(my_range)
        neuron = TN.Tripod(model)
        init, current, synapses= TN.simulate_tripod(neuron, 100, single_spike, record_synapses=true, intensity= 0)
        final, current, synapses= TN.simulate_tripod(neuron, 200, single_spike, record_synapses=true, intensity=intensity)
        final =hcat(init,final)
	    s= plot!(s, [final[2,:] ],label="", color=CList[n], linewidth=2)
	    p= plot!(p, [final[1,:] ], ylims=(-77, -55), color=CList[n], label="", linewidth=2)
	    push!(above_threshold, TN.dt*length(findall(x->x>-65, final[2,round(Int,1+delay/TN.dt):end])))
	    push!(end_threshold, findlast(x->x>-61, final[1,1:end]))
	end

	plot!(s,ylabel="Membrane\nsoma (mV)")
	plot!(p,ylabel="Membrane\ndendrite (mV)")

    s =xlabel!(s, "")
    # s = xticks!(s, )
    xlims!(s,(880,2700))

    p= xlabel!(p, "Time after spike (ms)")
    p = xticks!(p,1001:500:2700, string.(-0:50:170))
    xlims!(p,(880,2700))

    s= plot!(s, [0, 6000], [-0, -0], linestyle=:dash, color=:black,label="")
    if axis
        s= annotate!(s, [(1600, -3.5*4, Plots.text("GluRs reverse \npotential", 13,  :left))])
    end

    end_threshold[1] =1000
    end_threshold[2] =1100
    return s,p, above_threshold
end

## Run

NMDA_eyal = TN.eyal_exc_synapse(TN.dt, "dend")
TN.Esyn_dend.NMDA = NMDA_eyal.NMDA
s1,p1, aboveNMDA= NMDA_spikes(color=TN.GREEN,axis=false, model=TN.H_distal_distal);

NMDA_duarte = TN.duarte_exc_synapse(TN.dt, "dend")
TN.Esyn_dend.NMDA.gsyn = NMDA_duarte.NMDA.gsyn
s,p, aboveAMPA = NMDA_spikes(color=:black, axis=true, model =TN.H_distal_distal);

plot!(p, xlabel="Time after spike (ms)")
p1 = plot(p1, frame=:axes, ylabel="");
s1 = plot(s1, frame=:axes, ylabel="");
plot!(p1, yticks=nothing)
plot!(s1, ticks=nothing, title="Human (NAR = 1.8)")
plot(s, xticks=nothing ,title="Mouse (NAR = 0.25)")
inset = (1, bbox(0.05, 0.05, 0.40, 0.35, :bottom, :right))
z = plot(s,s1,p,p1, layout=(2,2),frame=:axes)

plot!(z,guidefontsize=18,tickfontsize=13)
s
c =TN.colormap(length(ls),TN.BLU, TN.RED)
heatmap!(c, inset=inset, subplot=5, axis=false, title="Co-active syapses")
savefig(joinpath(@__DIR__, "figures","fig3a.pdf"))

## Measure the plateau duration

tripod_human(l) = "H. 1->s, $l, 4"
tripod_mouse(l) = "M. 1->s, $l, 4"


function above_threshold()
    plateau = plot()
    t2 = plot()
	ls = 100:30:500
    eff_range = 1:2:200
    delay=0
    plateau = zeros(2,length(ls),length(eff_range))
    for n in eachindex(ls)
		l = ls[n]
		model = tripod_human(l)
        NMDA_eyal = deepcopy(TN.eyal_exc_synapse(TN.dt, "dend"))
        TN.Esyn_dend.NMDA = NMDA_eyal.NMDA
        _,_, plateau_NMDA = NMDA_spikes(delay=delay, color=TN.RED, model=model, my_range=eff_range)
        TN.Esyn_dend.NMDA.gsyn = 0.
        _,_, plateau_AMPA= NMDA_spikes(delay=delay, color=TN.RED, model=model, my_range=eff_range);
        plateau[1, n, :] =  plateau_AMPA
        plateau[2, n, :] =  plateau_NMDA
    end

	## make plots
	for l in 1:length(ls)
		for x in 1:length(eff_range)-1
			if plateau[1,l,x+1] < plateau[1,l,x]
				plateau[1,l,x:end] .= plateau[1,l,x]
				break
			end
			if plateau[2,l,x+1] < plateau[2,l,x]
				plateau[2,l,x:end] .= plateau[2,l,x]
				break
			end
		end
	end
	return plateau
end

plateau = above_threshold()
c =TN.colormap(length(ls),TN.RED, TN.BLU)
plot(plateau[2,:,:]', ylims=(0,120), lw=2, c=c, label="")
plot!(plateau[2,8,:], c=:black, ylims=(0,120), lw=3, ls=:dash, label="Spike Threshold", legend=:topleft, legendfontsize=13)
plot!(33:100,plateau[2,1:7,33:end]', c=:white, ylims=(0,120), lw=3, ls=:dash, label="")
plot!(title="NMDA plateau potential", titlefontsize=18, ylabel="Plateau duration (ms)", xlabel="Co-active synapses")
annotate!([(100,12, Plots.text("Distal", 13,  :right))])
annotate!([(50,12, Plots.text("Proximal", 13,  :left))])
# annotate!([(65,83, Plots.text("Spike threshold", 15,  :left))])
annotate!([(55,72, Plots.text("Soma spikes", 13,  :left))])

myfont = Plots.text("").font
myfont.rotation=14
plot!(ylims=(0,125),guidefontsize=18,tickfontsize=13, frame=:axes)
inset = (1, bbox(0.05, 0.05, 0.40, 0.25, :bottom, :right))
heatmap!(c, inset=inset, subplot=2, axis=false)
savefig(joinpath(@__DIR__, "figures","fig3b.pdf"))
