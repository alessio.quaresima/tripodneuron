include("../base.jl")

using HDF5
using Plots

EXCSPIKETIME = 100
function stimulus(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  Δt::Int)
    """ Poisson input train"""
    if step == EXCSPIKETIME
        TN.exc_spike!(tripod.d[1], eff=50.)
    end
    if step == EXCSPIKETIME+Δt
        TN.exc_spike!(tripod.d[1], eff=150.)
    end
end

##


intervals= 0:20:500
ΔEPSP = zeros(length(intervals))
integral = zeros(length(intervals))
conductance = zeros(length(intervals))
for (n,Δt) in enumerate(intervals)
    tripod= TN.Tripod(TN.H_distal_distal)
    v, _, s=TN.simulate_tripod(tripod, 200, stimulus, Δt=Δt, record_synapses=true, record_currents=false)
    conductance[n] = mean(s[1,:,2])
    ΔEPSP[n] = TN.get_EPSP(v, spiketime=1)
    integral[n] = sum(v[1,:])/length(v[1,:])
end
using LaTeXStrings
plot(plot(intervals,ΔEPSP, title=L"\Delta"*"EPSP (mV)"),
        plot(intervals,conductance, title="Mean Synaptic conductance (nS)"),
        plot(intervals,integral, title= "Mean membrane potential (mV)" ),
        layout=(1,3)
        )

# plot(v[1,:])
# v=TN.simulate_fast(tripod, 500, stimulus, Δt=500)
