"""
This file reproduces Fig 8b and SIFig 6

Compute the spiking rate of the tripod in all dendritic configurations
Synaptic input only on dendrites, and innhibition with same input rate than the excitation
"""
##
include("../base.jl")
using HDF5
using Printf
using EllipsisNotation

function stimuliA(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  f::Real)
    """ Poisson input train"""
      TN.set_rate!(tripod.d[1],1/f, eff=f)
      TN.set_rate!(tripod.d[2],1/f, eff=f)
      TN.set_rate!(tripod.d[1],-1., eff=1.)
      TN.set_rate!(tripod.d[2],-1.,eff=1.)
end

function stimuliB(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  f::Real)
    """ Poisson input train"""
      TN.set_rate!(tripod.d[1],1/f, eff=f)
      TN.set_rate!(tripod.d[2],1/f, eff=f)
      TN.set_rate!(tripod.d[1],-1/f, eff=f)
      TN.set_rate!(tripod.d[2],-1/f, eff=f)
end

function stimuliC(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  f::Real)
    """ Poisson input train"""
      TN.set_rate!(tripod.d[1],1.)
      TN.set_rate!(tripod.d[2],1.)
      TN.set_rate!(tripod.d[1],-1/f, eff=f)
      TN.set_rate!(tripod.d[2],-1/f, eff=f)
end

function stimuliD(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  f::Real)
    """ Poisson input train"""
    f0 = round(Int, f)
    for _ in 1:f0
        TN.set_rate!(tripod.d[1],1/f0)
        TN.set_rate!(tripod.d[2],1/f0)
    end
    TN.set_rate!(tripod.d[1],-1.)
    TN.set_rate!(tripod.d[2],-1.)
end


# voltage = TN.simulate_model(model, 15000, stimuli, f=10)

function test_f(; f, model::String, stimuli::Function)
    _ = TN.simulate_model(model, 1000, stimuli, f=f)
    voltage = TN.simulate_model(model, 10000, stimuli, f=f)
    r = TN.get_spike_rate(voltage)
    c = TN.get_cv(voltage)
    vm = mean(voltage, dims=2)
    vs = std(voltage, dims=2)
    return [r c vm... vs...]
end


function map_fspace(;fs, models, stimuli)
    samples= 20
    data = zeros(8,  length(fs), length(models)) ## rate/cv/voltage/soma/d1/d2
    Threads.@threads for n in eachindex(fs)
        @show n
        for (h, model) in enumerate(models)
            out = [0. 0. 0. 0. 0. 0. 0. 0.]
            fails = 0
            for _ in 1:samples
                out .+= test_f(f=fs[n], model=model, stimuli=stimuli)
                # catch
                #     fails +=1
                # end
            end
            data[:,n, h] = out ./(samples - fails)
            # else
            #     @show fs[n], "fails"
            # end
        end
    end
    return data
end

function get_heat(data; numbers=false)
    data[isnan.(data)] .= 0
    dataΔ = zeros(size(data))
    for n in 1:4
        dataΔ[:,:,n] = data[:,:,n] .- data[:,1,n]
    end
    models_labels = [ "distal", "sh.distal", "proximal", "soma"]
    units = ["Hz", "", "mV", "mV", "mV", "mV", "mV", "mV"]
    plots= []
    for (n,u) in enumerate(units )
        if n ∈ [1,2]
            p = heatmap(fs,1:4,dataΔ[n,:,:]',yticks=(1:4, models_labels), xscale=:log,tickfontsize=13, guidefontsize=18, xlabel = "Simultaneous spikes", c=:amp, title="Spike rate", clims=(0,30), colorbar=false)
        elseif n ∈ [3,4,5]
            p = heatmap(fs,1:4,dataΔ[n,:,:]',yticks=(1:4, models_labels), clims=(-20,0), xscale=:log,tickfontsize=13, guidefontsize=18, xlabel = "Simultaneous spikes", c=:amp)
        else
            p = heatmap(fs,1:4,dataΔ[n,:,:]',yticks=(1:4, models_labels), clims=(0,30), xscale=:log,tickfontsize=13, guidefontsize=18, xlabel = "Simultaneous spikes", c=:amp, title="Membrane potential\n variation", colorbar=false)
        end
        plot!(xrotation = -45)
        # if !(n ∈ [1,2,5,8] )
        #     plot!(colorbar=false)
        # end
        if !(n ∈ [1, 3, 6])
            plot!(yaxis=false)
        end
        if n ∈ [1,2,6,7,8]
            c= :black
        else
            c=:white
        end
        if numbers
            for pos in 1:4
                annotate!([(0.12, pos, text(string(round(data[n,1,pos], digits=1)) * u,:left,:bottom,c,12))])
            end
            plot!([0.1, 0.1],[0.5,4.5], ls=:dash, c=:black, label="")
            for pos in 1:4
                annotate!([(95, pos, text(string(round(data[n,end,pos], digits=1)) * u,:right,:bottom,c,12))])
            end
            plot!([100, 100],[0.5,4.5], ls=:dash, c=:black, label="")
        end
        push!(plots, p)
        end
    return plots
end

function get_plots(data; numbers=false)
    data[isnan.(data)] .= 0
    dataΔ = zeros(size(data))
    for n in 1:4
        dataΔ[:,:,n] = data[:,:,n] .- data[:,1,n]
    end
    models_labels = [ "distal", "sh.distal", "proximal", "soma"]
    units = ["Hz", "", "mV", "mV", "mV", "mV", "mV", "mV"]
    plots= []
    return plot(data[4,:,:], ribbon= data[7,:,:])
    return plots
end


##
println("#####################")
models = [TN.H_distal, TN.H_medial, TN.H_proximal, TN.H_ss]
using Plots
fs = 10 .^range(-1,2,length=30)

dataA = map_fspace(fs=fs,models=models, stimuli=stimuliA)
dataB = map_fspace(fs=fs,models=models, stimuli=stimuliB)
dataC = map_fspace(fs=fs,models=models, stimuli=stimuliC)
dataD = map_fspace(fs=fs,models=models, stimuli=stimuliD)



## plots

subset = [1:3...,4]
p = get_plots(dataB[..,subset], numbers= true)
data = dataB
colors = [TN.BLU TN.RED TN.GREEN :black]
##
a = plot(fs, data[4,:,:], ribbon= data[7,:,:], xlabel="Simultaneous spikes", c= colors, labels= ["distal" "sh.distal" "proximal" "soma-only"])
s1 =plot(fs, data[1,:,:],c=colors, legend=false, xticks=nothing, ylabel="Spike\nrate (Hz)")
# s2 =plot(fs, data[2,:,:],c=colors, legend=false, xticks=nothing, ylabel="CV")
s3 = plot(fs, data[3,:,:], ribbon=data[6,:,:], c= colors, legend=false, xlabel="Simultaneous spikes", ylabel="Soma \npotential (mV)")
l = @layout [[a;  c] d]
plot(s1,s3, a,layout=l, link =:x)
plot!( lw=5, frame=:axes, xscale=:log, guidefontsize=18, tickfontsize=13)
##
plots_exc = get_plots(dataA[..,subset])
plots_inh = get_plots(dataC[..,subset])
pE = plot(plots_exc[1], plots_exc[7], xlabel="", xticks=([],[]))
pI = plot(plots_inh[1], plots_inh[7], title="")
plot_exc_inh=plot(pE, pI, layout=(2,1))

savefig(plot_exc_inh, joinpath(@__DIR__, "figures","exc_inh_simult.pdf"))
savefig(plot_comparison, joinpath(@__DIR__,"figures", "comparison_simult.pdf"))
