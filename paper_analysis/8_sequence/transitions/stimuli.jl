using RollingFunctions

function oscillate_signal(simtime, interval, dt=TN.dt)
    sig = zeros(Int,round(Int,simtime/dt))
    for step in 1:length(sig)
        fase =(step ÷ (interval/dt))
        if fase%2 == 0
                sig[step] = 0
        elseif fase%2 ==1
                sig[step] = 1
        end
    end
    return sig
end


function stimulus_interval(;tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  interval::Real, kwargs...
                  )
    ## baseline input
    TN.set_rate!(tripod.s,1.)
    ## 4 KHz of weak inputs on dendritic
    fase =(step ÷ (interval/dt))
    # @show fase
    if fase%2 == 0
        TN.set_rate!(tripod.d[1],0.5,eff=2.)
    elseif fase%2 ==1
        TN.set_rate!(tripod.d[2],0.5,eff=2.)
    end
end

function generate_signal(simtime; ξ=0.9)
    signal = zeros(simtime*10)
    mem    = 0
    for x in 0:simtime-1
        if rand() > ξ
            mem = (mem+1)%2
        end
        signal[1+(x*10):(x+1)*10] .= mem
    end
    signal = sign.(signal)
    signal[signal .==0] .= 2
    return signal
end

function integrator(signal; τ=10)
    state = zeros(length(signal)+1)
    state[1] = signal[1]
    for x in eachindex(signal)
        state[x+1] = state[x]*(1-1/τ) +signal[x]
    end
    return state
end

function stimulus_signal(;tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  signal::Array
                  )
    ## baseline input
    TN.set_rate!(tripod.s,1.)
    ## 4 KHz of weak inputs on dendritic
    fase =signal[step]
    if fase%2 == 0
        TN.set_rate!(tripod.d[1],0.5,eff=2.)
    elseif fase%2 ==1
        TN.set_rate!(tripod.d[2],0.5,eff=2.)
    end
end


function stimulus_interval(;tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  interval::Real, kwargs...
                  )
    ## baseline input
    TN.set_rate!(tripod.s,1.)
    ## 4 KHz of weak inputs on dendritic
    fase =(step ÷ (interval/dt))
    # @show fase
    if fase%2 == 0
        TN.set_rate!(tripod.d[1],0.5,eff=2.)
    elseif fase%2 ==1
        TN.set_rate!(tripod.d[2],0.5,eff=2.)
    end
end

function test_frequency(ν)
    interval = round(Int,1000/ν)
    SIM_TIME = 1000
    v = TN.simulate_model(TN.H_ss, SIM_TIME, stimulus_interval,interval=interval)
    data = zeros(size(TN.get_spikes(v[1,:]),1),6)
     for n in 1:6
        _spikes = falses(500,size(TN.get_spikes(v[1,:]),1))
        m = TN.models[n]
        @show m, ν
        for x in 1:500
            v = TN.simulate_model(m, SIM_TIME, stimulus_interval,interval=interval)
            _spikes[x,:] .= TN.get_spikes(v[1,:])
        end
        data[:,n] = runmean(sum(_spikes, dims=1)[1,:],100)
    end
    return data, interval
end
