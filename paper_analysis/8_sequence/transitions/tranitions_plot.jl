"""
This file reproduces Fig 10 a and 10b, run transitions_signal.jl before
"""
include("../../base.jl")
using StatsBase
using HDF5
using ScikitLearn
using ScikitLearn: fit!, predict
using Printf
using Plots
pyplot()
include("stimuli.jl")

##
file = joinpath(@__DIR__,"transition_detection.h5")
fid = read(h5open(file, "r"))
scores_oscillation = fid["oscillation"]
scores_signal = fid["signal"]
νs = fid["νs"]

##
data, interval = test_frequency(10)
input = oscillate_signal(1000,interval)
##
styles=[:dash :solid :solid :solid :solid :solid :solid :dash]
colors=[:black TN.BLU TN.BLU TN.BLU TN.RED TN.RED :black :black]
labels = ["input signal", "distal - distal", "sh.distal - sh.distal", "proximal - proximal", "distal - proximal", "sh.distal - proximal", "soma only"]
res = zeros(length(input),7)
res[:,1] .= input
res[:,2:7]= data

p = plot(title="Spike density")
for n in 1:7
    plot!((-n .+ 0.85*res[:,n] ./maximum(res[:,n])), ls=styles[n], c=colors[n], xlims=(0,10000), label="")
    plot!((-n .+ 0.85*res[:,n] ./maximum(res[:,n])), ls=styles[n], c=colors[n], xlims=(0,10000), label="")
    # annotate!([(10300,-n+0.5, Plots.text(labels[n], 13, :black, :left))])
end
p = plot!(legend=false, axis=false)
# savefig(p, joinpath(@__DIR__,"oscillations.pdf"))
# heatmap(data[:,:],c=:amp)
# yticks!()


my_scores=zeros(15,7)
my_scores[:,2:7] = scores_oscillation
q = heatmap(νs,1:7, my_scores', yflip=true, xscale=:log, c=:amp, clims=(0,1), colorbar=false, xlabel="Signal frequency (Hz)")
yticks!(1:7, labels)
final = plot(q,p)
plot!(tickfontsize=13, guidefontsize=18, xrotation=-45)
savefig(final, joinpath(@__DIR__,"oscillations.pdf"))


##

plot()
for n in 1:6
    v = TN.simulate_model(TN.models[n],1000, stimulus_interval,interval=100 )
    times = TN.get_spike_times(v[1,:])
    scatter!(times, n*ones(length(times)), labels=string(n))
end
p = plot!( frame=:none, legend=false)
p =plot(p,p)
savefig(p,"spikes.pdf")

##
colors = [TN.color_list[1:5]... :black]
labels = ["distal - distal" "sh.distal - sh.distal" "proximal - proximal" "distal - proximal" "sh.distal - proximal" "soma-only"]

signal = generate_signal(1000,ξ=0.99)
v = TN.simulate_model(TN.H_medial, 1000, stimulus_signal,signal=signal)
inputs = abs.(diff(signal[1:end]))

plot(10:10:600, mean(scores_signal, dims=1)[1,:,:], ribbon=std(scores_signal, dims=1)[1,:,:]./2, lw= 2, c=colors, fillalpha=0.4, xlabel="Signal to voltage delay (ms)", ylabel="Signal/Spike Correlation", labels=labels,  legend=:topright, ylims=(-0.2,0.6))
# inputs_shadow = integrator(abs.(diff(signal[1:end])), τ=1000)
# _v = integrator(TN.get_spikes(v[1,1:end]), τ=100)
annotate!([(200,0.52, Plots.text("Signal", 13, :black, :left))])
annotate!([(200,0.37, Plots.text("Soma\nmembrane", 13, :black, :left))])
inset = (1, bbox(0.69, 0.6, 0.30, 0.35, :bottom, :right))
plot!([inputs*5, v[1,:]./5],c=[:black TN.BLU], inset=inset, subplot=2, xaxis=false,alpha=[1 1 0.2 0.2], yaxis=false, label=["" ""], lw=2)
p = plot!(guidefontsize=18, tickfont=13)
savefig(p, joinpath(@__DIR__, "signal_spikes.pdf"))
