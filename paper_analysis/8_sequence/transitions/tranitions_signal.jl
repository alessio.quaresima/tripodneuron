include("../../base.jl")
using StatsBase
using HDF5
using Printf
using RollingFunctions

SIM_TIME = 10000
##
include("stimuli.jl")
function correlate_signal()
    ## compute correlation changing the delay between the input and output signal
    shifts = 10:10:600
    scores = zeros(100, length(shifts), 6)
    samples = 100
    Threads.@threads for m in eachindex(TN.models)
        model = TN.models[m]
        for s in 1:20
            @show s, model
            signal = generate_signal(2000,ξ=0.99)
            v = TN.simulate_model(model, 1000, stimulus_signal,signal=signal)
            spikes = falses(samples, 20000)
            for x in 1:samples
                v = TN.simulate_model(TN.models[m], 2000, stimulus_signal, signal=signal)
                spikes[x, :] = TN.get_spikes(v[1,:])
            end
            average = runmean(sum(spikes, dims=1)[1,:],100)
            for x in eachindex(shifts)
                shift = shifts[x]
                a = integrator(abs.(diff(signal[1:end-shift])), τ=100)
                b = average[shift+1:end]
                a = a ./ maximum(a)
                b = b ./ maximum(b)
                score = cor(a,b)
                scores[s,x, m] = score
            end
        end
    end
    return scores
end

function test_frequency(ν)
    interval = round(Int,1000/ν)
    SIM_TIME = 1000
    v = TN.simulate_model(TN.H_ss, SIM_TIME, stimulus_interval,interval=interval)
    data = zeros(size(TN.get_spikes(v[1,:]),1),6)
     for n in 1:6
        _spikes = falses(500,size(TN.get_spikes(v[1,:]),1))
        m = TN.models[n]
        for x in 1:500
            v = TN.simulate_model(m, SIM_TIME, stimulus_interval,interval=interval)
            _spikes[x,:] .= TN.get_spikes(v[1,:])
        end
        data[:,n] = runmean(sum(_spikes, dims=1)[1,:],100)
    end
    return data, interval
end

function oscillation_detection(νs)
    scores = zeros(size(νs)[1], 6)
    samples= 2
    indexes = 1:length(νs)
    Threads.@threads for n in indexes
        @show ν
        data, interval =  test_frequency(νs[n])
        sample_times = collect(1:10:interval*10*3)
        for m in 1:6
            scores[n,m] = mean(abs.(autocor(data[:,m], sample_times)[[interval, interval*2]]))
        end
    end
    return scores
end
##

νs = 10 .^range(0.5,2.5,length=15) #Hertz
scores_oscillation = oscillation_detection(νs)
scores_signal = correlate_signal()
file = joinpath(@__DIR__,"transition_detection.h5")
isfile(file) && rm(file)
h5open(file, "w") do fid
    fid["oscillation"] = scores_oscillation
    fid["signal"] = scores_signal
    fid["νs"] = νs
end
