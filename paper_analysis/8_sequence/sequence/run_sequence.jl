include("sequence.jl")
println("Run models")
println("ss")
scores_ss = get_scores(model=ss)
println("pp")
scores_pp = get_scores(model=pp)
println("mm")
scores_mm = get_scores(model=mm)
println("dd")
scores_dd = get_scores(model=dd)
println("dp")
scores_dp = get_scores(model=dp)
println("mp")
scores_mp = get_scores(model=mp)

file= joinpath(@__DIR__,"sequence_scores.h5")
isfile(file) && (rm(file))
h5open(file,"w") do fid
    fid["ss"] = scores_ss
    fid["pp"] = scores_pp
    fid["mm"] = scores_mm
    fid["dd"] = scores_dd
    fid["dp"] = scores_dp
    fid["mp"] = scores_mp
end
