include("../../base.jl")
using StatsBase
using HDF5
using StatsPlots
using ScikitLearn
using ScikitLearn: fit!, predict
using Printf
pyplot()

dd() = TN.Tripod(TN.H_distal_distal);
pp() = TN.Tripod(TN.H_proximal_proximal);
ss() = TN.Tripod("H. 1->1, 100,4; 2->2,100,4");
mm() = TN.Tripod("H. 1->s, 300,4; 2->s,300,4");
mp() = TN.Tripod("H. 1->s, 300,4; 2->s,150,4");
dp() = TN.Tripod(TN.H_distal_proximal);
models = [dd dp pp ss]

function stimulus(;tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  order::Int64, interval::Real, kwargs...
                  )
    ## baseline input
    TN.set_rate!(tripod.s,1.)
    ## 4 KHz of weak inputs on dendritic
    ## compartments
    if (step ÷ interval/dt)%3 == order
        TN.set_rate!(tripod.d[1],0.5,eff=5.)
        TN.set_rate!(tripod.d[2],0.1,eff=1.)
    elseif (step ÷ interval/dt)%3 == abs(order-1)
        TN.set_rate!(tripod.d[1],0.1,eff=1.)
        TN.set_rate!(tripod.d[2],0.5,eff=5.)
    end
end

function stimulus_labels(order::Int,
                       interval::Real,
                       simtime::Int64,
                       dt::Float64)
    ## 4 KHz of weak inputs on dendritic
    ## compartments
    input = zeros(round(Int,simtime/dt))
    for t in eachindex(input)
        if (t ÷ interval)%3 == order
        # d = rand([1,2])
            input[t]=2
        elseif (t ÷ interval)%3 == abs(order-1)
            input[t]=1
        else
            input[t]=0
        end
    end
    return input
end

##



function _simulate_ss(;tripod::TN.Tripod, simtime::Int, inh_ratio::Real, τinh::Real, interval::Real, kwargs...)
    total_steps = round(Int,simtime/TN.dt)
    n_dend = length(tripod.d)
    ## Recordings
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
    currents = Array{Float64,1}(undef,n_dend+1)
    inh_voltage = Array{Float64,2}(undef,2,total_steps)
    spiked = (tripod.s.v > TN.AdEx.u_th)
    ### Bono&Clopath inhibitory filter
    E_in = 0
    g_in = 0
    τrise = 2 #ms
    Ainhib_1 = 5 #pS
    Ainhib_2 = 5*inh_ratio #pS
    current  = 0.
    @fastmath @inbounds for n in 1:total_steps
    	for (n,d) in enumerate(tripod.d)
            d.v = tripod.s.v
            TN.update_synapses!(d, TN.Esyn_dend)
            current += (TN.syn_current(d, TN.Esyn_dend))
    	end
        tripod.s.v -= TN.AdEx.C⁻ * current
        current = 0.
     	spiked = TN.update_AdEx_soma!(tripod.s, spiked)
        ### Inhibitory coupling
        E_in += TN.dt* (-E_in/τinh) + spiked
        g_in += TN.dt* (-g_in + E_in)/τrise
        I1 =  -Ainhib_1 * g_in*(tripod.s.v - TN.Esyn_dend.GABAa.E_rev)
        I2 =  -Ainhib_2 * g_in*(tripod.s.v - TN.Esyn_dend.GABAa.E_rev)
    	tripod.s.v += TN.AdEx.C⁻*(I1+I2)
        stimulus(;tripod=tripod,step=n,dt=TN.dt, interval=interval, kwargs...)
        for (nd,d) in enumerate(tripod.d)
            voltage[nd+1,n] = d.v
        end
        voltage[1,n] = tripod.s.v
        inh_voltage[1,n] = I1
        inh_voltage[2,n] = I2
    end
    ## Current and Voltage plot
    return voltage, inh_voltage
end

function _simulate_tripod(;tripod::TN.Tripod, simtime::Int, inh_ratio::Real, τinh::Real, kwargs...)
    total_steps = round(Int,simtime/TN.dt)
    n_dend = length(tripod.d)
    ## store currents --> to be removed lateron
    ## Recordings
    voltage = Array{Float64,2}(undef,n_dend+1,total_steps)
    currents = Array{Float64,1}(undef,n_dend+1)
    inh_voltage = Array{Float64,2}(undef,2,total_steps)
    spiked = (tripod.s.v > TN.AdEx.u_th)
    ### Bono&Clopath inhibitory filter
    E_in = 0
    g_in = 0
    τrise = 2 #ms
    Ainhib_1 = 5 #pS
    Ainhib_2 = 5*inh_ratio #pS
    @fastmath @inbounds for n in 1:total_steps
        spiked = TN.update_tripod!(tripod,currents,spiked)
        ### Inhibitory coupling
        E_in += TN.dt* (-E_in/τinh) + spiked
        g_in += TN.dt* (-g_in + E_in)/τrise
        I1 =  -Ainhib_1 * g_in*(tripod.d[1].v - TN.Esyn_dend.GABAa.E_rev)
        I2 =  -Ainhib_2 * g_in*(tripod.d[2].v - TN.Esyn_dend.GABAa.E_rev)
    	tripod.d[1].v += TN.dt*tripod.d[1].pm.C⁻*I1
    	tripod.d[2].v += TN.dt*tripod.d[2].pm.C⁻*I2
        stimulus(;tripod=tripod,step=n,dt=TN.dt,kwargs...)
        ## store
        for (nd,d) in enumerate(tripod.d)
            voltage[nd+1,n] = d.v
        end
        voltage[1,n] = tripod.s.v
        inh_voltage[1,n] = I1
        inh_voltage[2,n] = I2
    end
    ## Current and Voltage plot
    return voltage, inh_voltage
end

# exc, inh = _simulate_tripod(;tripod=mm(), simtime=10400, inh_ratio=1., interval=100., τinh=50, order=1)
# exc2, inh2 = _simulate_tripod(;tripod=mm(), simtime=10400, inh_ratio=1., interval=100., τinh=50, order=2)
# @show mean(exc[3,:]), mean(exc[2,:]), mean(exc2[3,:]), mean(exc2[2,:])
# @show mean(inh[1,:]), mean(inh[2,:]), mean(inh2[1,:]), mean(inh2[2,:])
# plot(exc[:,1000:2000]', )
##
# v,i = _simulate_tripod(;tripod=dp(), simtime=1000, inh_ratio=50., τinh=50., interval=500., order=2)
# plot(v)

function simulate_sequence(;model, order, inh_ratio::Real=30, interval::Real=100, τinh::Real=50, repeat=2, simtime=-1)
    """ Simulate the tripod activity and output the sequence of symbols"""
    if simtime < 0
        simtime = round(Int,interval*repeat*3)
    end
    interval = round(Int,interval /TN.dt)
    tripod = model()
    if model == ss
        _,  _= _simulate_ss(tripod=tripod,simtime=1000, inh_ratio = inh_ratio, τinh = τinh, interval=interval, order=order)
        exc,  inh= _simulate_ss(tripod=tripod,simtime=simtime, inh_ratio = inh_ratio, τinh = τinh, interval=interval, order=order)
    else
        _,  _= _simulate_tripod(tripod=tripod,simtime=1000, inh_ratio = inh_ratio, τinh = τinh, interval=interval, order=order)
        exc,  inh= _simulate_tripod(tripod=tripod,simtime=simtime, inh_ratio = inh_ratio, τinh = τinh, interval=interval, order=order)
    end
    symbols = stimulus_labels(order, interval, simtime,TN.dt)
    return exc, inh, symbols
end

# exc, inh, sym = simulate_sequence(;model=dd, order=2, repeat = 15, interval=100)
# exc, inh, sym= simulate_sequence(model=dd, order=2, inh_ratio = 10, interval=100, τinh = 50, repeat=10)
# plot(exc)
# plot!(sym)




function comparative_plot(;model, interval, inh_ratio, τinh)
    exc, inh, symbols= simulate_sequence(model=model, order=1,inh_ratio=inh_ratio, interval=interval, τinh=τinh)
    # plot_tripod(exc, current)
    a = plot(10*(symbols), color=:black, lw=2)
    a = plot!(a,exc[2,:], color=TN.RED, linewidth=2)
    a = plot!(a, exc[3,:], color=TN.BLU, linewidth=2)
    a = plot!(a,exc[1,:], color=:black, linewidth=2)
    a = scatter!(a, [v>-30 ? 10 : -100 for v in exc[1,:]], marker=:black)
    a = ylims!(a, (-80,25))
    a = xaxis!(a, false )
    a = ylabel!(a, "Membrane\nvoltage (mV)")
    title!(a, "ABϵABϵ", titlefont=15, titleloc=:right)


    exc, inh, symbols = simulate_sequence(model=model, order=0,inh_ratio=inh_ratio, interval=interval, τinh=τinh)
    # plot_tripod(exc, current)
    b = plot(10*(symbols), color=:black, linewidth=2)
    b = plot!(b,exc[2,:], color=TN.RED, linewidth=2)
    b = plot!(b, exc[3,:], color=TN.BLU, linewidth=2)
    b = plot!(b, exc[1,:], color=:black, linewidth=2)
    b = scatter!(b, [v>-30 ? 10 : -100 for v in exc[1,:]], color=:black)
    b = ylims!(b,(-80,25))
    b = ylabel!(b, "Membrane\nvoltage (mV)")
    time_ = size(exc,2)
    b = xticks!(b, 0:time_/5:time_, string.(0:time_/50000:(time_)/10000))
    b = xlabel!(b, "Time (s)")
    title!(b, "BAϵBAϵ", titlefont=15, titleloc=:right)

    myplot= plot(a,b, layout=(2,1), legend=false)
    plot!(myplot, guidefontsize=15, tickfontsize=13, colorbar=false)
    plot(myplot, frame=:axes)
    return myplot
end
# myplot = comparative_plot(;model=mm, interval=100., inh_ratio=30., τinh=50.)
# savefig(myplot, joinpath(@__DIR__,"compare activity.pdf"))

## test rate/cv
function run_batch(; model, inh_ratio, interval, τinh, batch_size=100)
    repeat = 10
    rates = zeros(batch_size)
    cvs =zeros(batch_size)
    colors = []
    simtime = maximum([1000, interval*repeat*3])
    simtime = minimum([10000, simtime])
    membranes =zeros(batch_size, simtime, repeat)
    for x in 1:batch_size
        exc, inh, symbols = simulate_sequence(simtime=simtime, model=model, order=x%2,inh_ratio=inh_ratio, interval=interval, τinh=τinh, repeat=repeat)
        exc = exc[1,:]
        membranes[x,:,:] = reshape(exc, simtime, repeat )
        color = x%2 ==0 ? TN.BLU : TN.RED
        cvs[x] = TN.CV(exc)
        rates[x] = TN.get_spike_rate(exc)
        push!(colors,color)
        @printf "\r %d %%" x/batch_size*100
    end
    X = hcat(cvs, rates)
    colors = map(x->x%2 ==0 ? TN.RED : TN.BLU, 1:batch_size)
    Y = map(x->x%2 ==0 ? 1 : -1, 1:batch_size)
    return X, Y, colors
end


function classify(; inh_ratio, interval, τinh, samples=200, model)
    LinReg = ScikitLearn.Models.LinearRegression()
    X,Y,colors = run_batch(; model=model, inh_ratio=inh_ratio, interval=interval,
                            τinh=τinh, batch_size=samples)
    fit!(LinReg, X, Y)
    X,Y,colors = run_batch(; model=model, inh_ratio=inh_ratio, interval=interval,
                            τinh=τinh, batch_size=samples)
    return 1-mean(abs.((sign.(predict(LinReg,X))-Y)/2))
end


function get_scores(;model)
    scores = Vector{Float64}()
    ratios = 0:1:20
    intervals = 10 .^range(0.5,3,length=20)
    scores = Array{Float64,2}(undef,length(intervals), length(ratios))
    name = String(Symbol(model))
    Threads.@threads  for r in eachindex(ratios)
        tic = time()
        for i in eachindex(intervals)
            S = classify(model=model, inh_ratio = ratios[r], interval=round(Int,intervals[i]), τinh = 50)
            scores[i,r] = S
        end
        toc = time()
        Δt  = toc-tic
        @show name, Δt, r
    end
    return scores
end
