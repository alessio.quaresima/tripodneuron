"This file reproduces Fig 10c and 10d"
## get_scores
using HDF5
using Plots
using StatsBase
using RollingFunctions
using LaTeXStrings
pyplot()
include("sequence.jl")

##
fid = h5open(joinpath(@__DIR__,"sequence_scores.h5")) do file
    read(file)
end
scores = fid["scores"]
intervals = fid["intervals"]
ratios = fid["ratios"]
for key in keys(scores)
    scores[key][isnan.(scores[key])] .= 0.5
end

mean_scores = []
for key in keys(scores)
    push!(mean_scores,runmean(mean(scores[key], dims=1)[1,:],3))
end

##
ppp = heatmap( ratios, intervals, clims=(0.5,1), scores["pp"], c=:amp, colorbar=false, title= "proximal-proximal", yaxis=false, xaxis=false);
pmm = heatmap(ratios, intervals, clims=(0.5,1), scores["mm"], c=:amp, colorbar=false, title= "sh.distal-sh.distal", yaxis=false, xaxis=false);
pdd = heatmap(ratios, intervals, clims=(0.5,1), scores["dd"], c=:amp, colorbar=false, title= "distal - distal", yaxis=false, xaxis=false);
pdp = heatmap(ratios, intervals, clims=(0.5,1), scores["dp"], c=:amp, colorbar=false, title= "distal - proximal", xlabel="                                              Inhibition ratio ("*L"\beta)", ylabel="                 Sequence interval (ms)");
pmp = heatmap(ratios, intervals, clims=(0.5,1), scores["mp"], c=:amp, colorbar=false, title= "sh.distal-proximal", yaxis=false, xaxis=false);
pss = heatmap(ratios, intervals, clims=(0.5,1), scores["ss"], c=:amp, colorbar=false, title= "soma - soma", yaxis=false, xaxis=false);

s = plot(pdd, pmm, ppp, pdp, pmp, pss, yscale=:log)
plot!(guidefontsize=18, tickfontsize=13, colorbar=false, xrotation=-45)
savefig(s, joinpath(@__DIR__, "sequence_raster.pdf"))
##


## Plot
#
X,Y, colors = run_batch(model=ss, inh_ratio = 10, interval=100, τinh = 50)
q = scatter(X[:,1],X[:,2], color=:white, markersize=10, markerstrokecolor=colors, alpha=1., label=false)
scatter!([[]],[[]], label="soma only", color=:black, markersize=10)
#
X,Y, colors = run_batch(model=dd, inh_ratio = 2, interval=100, τinh = 50)
q = scatter(X[:,1],X[:,2], color=:white, markersize=10, markerstrokecolor=colors, alpha=1., label=false, markershape=:cross);
scatter!([[]],[[]], markershape=:cross, label="distal - distal", color=:black, markersize=10)


X,Y, colors = run_batch(model=mm, inh_ratio = 10, interval=100, τinh = 50)
q = scatter!(X[:,1],X[:,2], color=:white, markersize=10, markerstrokecolor=colors, alpha=1., label=false, markershape=:dtriangle);
scatter!([[]],[[]], markershape=:dtriangle, label="short_distal - short_distal", color=:black, markersize=10)

X,Y, colors = run_batch(model=pp, inh_ratio = 10, interval=100, τinh = 50)
q = scatter!(X[:,1],X[:,2], color=:white, markersize=10, markerstrokecolor=colors, alpha=1., label=false, markershape=:rect);
scatter!([[]],[[]], markershape=:rect, label="proximal - proximal", color=:black, markersize=10)

X,Y, colors = run_batch(model=dp, inh_ratio = 10, interval=100, τinh = 50)
q = scatter!(X[:,1],X[:,2], color=:white, markersize=10, markerstrokecolor=colors, alpha=1., label=false, markershape=:utriangle);
scatter!([[]],[[]], markershape=:utriangle, label="distal - proximal", color=:black, markersize=10)

scatter!([[],[]],markersize=10,markerstrokecolor=[ TN.BLU TN.RED], color= [TN.BLU  TN.RED], label= ["AB" "BA"], legendtitle="Sequence", legend=:bottomright, legendtitlefont=13)

plot!(q, ylabel="Spike rate (Hz)", xlabel=" Spike rate CV", markershape=:star)
plot!(xlims=(0.,1.2))
p=plot(q, markerstrokewidth=2.,color=:transparent, tickfontsize=13,legendfontsize=13, guidefontsize=18,background_color=:white, framestyle=:axes, xaxis=true, legend=:topleft)
# savefig(q,joinpath(@__DIR__,"Sequence_raster.pdf"))
##
