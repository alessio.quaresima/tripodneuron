include("../base.jl")
using Plots
using HDF5
include("bimodal_kernel.jl")
pyplot()

# model = "1->3, 250,2; 2->3, 250,2; 3->s, 100,4"
SIM_TIME = 10000
noise = 0
model = TN.H_distal_proximal

ds = 0.00:0.005:0.2
exs = 0.:2.:50
function protocol(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  exc::Real, prox=0., current=0, inhibition=0., d1=0.02, d2=0.02)
        global noise

        TN.inject_current(tripod.s, current)

        TN.set_rate!(tripod.s, 1., eff=1.)
        TN.set_rate!(tripod.s, -1., eff=1.)
        # distal input
        TN.set_rate!(tripod.d[1], -d1, eff=inhibition)
        TN.set_rate!(tripod.d[1], d1, eff=exc)
        #proximal input
        TN.set_rate!(tripod.d[2], d2, eff=prox)
        TN.set_rate!(tripod.d[2], -d2, eff=inhibition)
end

## simulation


dd = TN.H_distal_distal
pp = TN.H_proximal_proximal
mm = TN.H_medial
ss = TN.H_ss
dp = TN.H_distal_proximal
sp = "H. 1->s, 300,4; 2->s,150,4"
models = [dd, mm, pp, dp, sp, ss]
titles = ["distal - distal", "sh.distal - sh.distal", "proximal - proximal",
         "distal - proximal", "sh.distal - proximal", "soma-only"]

models_hist = []
for (model, title) in zip(models, titles)
    voltage = TN.simulate_model(model,100_000, protocol; exc=40., prox=40., inhibition=40)
    p= histogram(voltage[1,:], normalize=true, bins=range(-90, stop=-40, length=240), color=:black, label="",title=title, titlefontsize=15)
    s = maximum(map(x-> !isnan(x) ? x : 0, p.series_list[1][:y]))
    kde = globalKDE(3, voltage[1,:])
    plot!(-90:-40, kde, label="", lw=2)
    rate = round(TN.get_spike_rate(voltage), digits=1)
    (model==sp) && (plot!(xlabel="Soma membrane potential (mV)"))
    annotate!([(-40, s/1.5, text(string(rate)*"Hz", :bottom,:right,:black ))])
    push!(plots, p)
    push!(models_hist, p)
end
p = plot(models_hist..., yaxis=false, tickfontsize=13, rotation = -45, guidefontsize=18)

savefig(p,joinpath(@__DIR__,"all_models.pdf") )


## Increasing current on the soma produce a shift of the ## Proximal activity
plots = []
for prox in 0:20:80
    voltage, currents,spikes, synapses, _ = TN.run_simulation(protocol; tripod=model, record_synapses=true, sim_time=SIM_TIME, exc=80., inhibition=40+prox, prox=prox)
    p= histogram(voltage[1,:], normalize=true, bins=range(-90, stop=-40, length=240), color=:black)
    s = maximum(map(x-> !isnan(x) ? x : 0, p.series_list[1][:y]))
    annotate!(p, -81, s/2, Plots.text(string(prox)))
    plot!(p, yaxis= false, xaxis =false, xticks=false, legend=false, xlims=(-90,-40), frame=:grid)
    rate = round(TN.get_spike_rate(voltage), digits=1)
    kde = globalKDE(3, voltage[1,:])
    plot!(-90:-40, kde, label="", lw=2, c=TN.RED)
    annotate!([(-40, s/2, text(string(rate)*"Hz", :bottom,:right,:black ))])
    push!(plots, p)
end
plot!(plots[end], xaxis =true, xticks=true, xlabel="Soma membrane (mV)");
annotate!(plots[3], -85, 0, Plots.text( "Proximal synapses", myfont));

proximal_input = plot(plots..., layout=(5,1), tickfontsize=13, guidefontsize=15,background_color=:transparent)

plots = []
for dist in 0:20:80
    voltage, currents,spikes, synapses, _ = TN.run_simulation(protocol; tripod=model, record_synapses=true, sim_time=SIM_TIME, exc=40+dist, inhibition=dist+40, prox=40)
    p= histogram(voltage[1,:], normalize=true, bins=range(-90, stop=-40, length=240), color=:black)
    s = maximum(map(x-> !isnan(x) ? x : 0, p.series_list[1][:y]))
    annotate!(p, -81, s/2, Plots.text(string(dist+40)))
    plot!(p, yaxis= false, xaxis =false, xticks=false, legend=false, xlims=(-90,-40), frame=:grid)
    rate = round(TN.get_spike_rate(voltage), digits=1)
    kde = globalKDE(3, voltage[1,:])
    plot!(-90:-40, kde, label="", lw=2, c=TN.RED)
    annotate!([(-40, s/2, text(string(rate)*"Hz", :bottom,:right,:black ))])
    push!(plots, p)
end
plot!(plots[end], xaxis =true, xticks=true, xlabel="Soma membrane (mV)")
myfont = Plots.text("",15).font
myfont.rotation = 90
annotate!(plots[3], -85, 0, Plots.text( "Distal synapses", myfont))
distal_input = plot(plots..., layout=(5,1), tickfontsize=13, guidefontsize=15,background_color=:transparent)


ptight = plot(proximal_input, distal_input, xrotation= -45)
savefig(ptight,joinpath(@__DIR__,"histogram_inhibition.pdf"))


ptight

## Inhibition
plots = []
for inh in [0., 20., 40., 60., 80.]
    voltage, currents,spikes, synapses, _ = TN.run_simulation(protocol; tripod=model, record_synapses=true, sim_time=SIM_TIME, exc=80, inhibition=inh+40, prox=40.)
    p= histogram(voltage[1,:], normalize=true, bins=range(-90, stop=-40, length=240), color=:black)
    s = maximum(map(x-> !isnan(x) ? x : 0, p.series_list[1][:y]))
    annotate!(p, -81, s/2, Plots.text(string(inh+40)))
    plot!(p, yaxis= false, xaxis =false, xticks=false, legend=false, xlims=-85,55, frame=:grid, frame)
    rate = round(TN.get_spike_rate(voltage), digits=1)
    kde = globalKDE(3, voltage[1,:])
    plot!(-90:-40, kde, label="", lw=2, c=TN.RED)
    annotate!([(-40, s/2, text(string(rate)*"Hz", :bottom,:right,:black ))])
    push!(plots, p)
end
plot!(plots[end], xaxis =true, xticks=true, xlabel="Soma membrane (mV)")
myfont = Plots.text("",15).font
myfont.rotation = 90
annotate!(plots[3], -85, 0, Plots.text( "Nr. inhibitory synapses", myfont))
plot_inh = plot(plots..., layout=(5,1), tickfontsize=13, guidefontsize=15,background_color=:transparent)

plots = []
for current in [0, 100, 200, 400, 800]
    voltage, currents,spikes, synapses, _ = TN.run_simulation(protocol; tripod=model, record_synapses=true, sim_time=SIM_TIME, exc=80, current=current, inhibition=80, prox=20)
    p= histogram(voltage[1,:], normalize=true, bins=range(-90, stop=-40, length=240), color=:black)
    s = maximum(map(x-> !isnan(x) ? x : 0, p.series_list[1][:y]))
    annotate!(p, -81, s/2, Plots.text(string(current)))
    plot!(p, yaxis= false, xaxis =false, xticks=false, legend=false, xlims=-85,55, frame=:grid, frame)
    rate = round(TN.get_spike_rate(voltage), digits=1)
    kde = globalKDE(3, voltage[1,:])
    plot!(-90:-40, kde, label="", lw=2, c=TN.RED)
    annotate!([(-40, s/2, text(string(rate)*"Hz", :bottom,:right,:black ))])
    push!(plots, p)
end
plot!(plots[end], xaxis =true, xticks=true, xlabel="Soma membrane (mV)");
myfont = Plots.text("",15).font;
myfont.rotation = 90;

annotate!(plots[3], -85, 0, Plots.text( "Inj. current (nA)", myfont));

plot_currents = plot(plots..., layout=(5,1), tickfontsize=13, guidefontsize=15,background_color=:transparent)

a = plot(plot_currents, plot_inh)
savefig(a,joinpath(@__DIR__,"histogram_current.pdf"))

a


##
windowsd2 = zeros(Int, length(ds), length(exs))
Threads.@threads for n in 1:length(ds)
    for m in 1:length(exs)
    voltage = TN.simulate_fast(TN.Tripod(model),SIM_TIME, protocol; exc=exs[m], prox=1., d1 = ds[n], d2=0.)
    c = critical_window(voltage[1,:])
    @show n, m,  c
    windowsd2[n,m] = c
    end
end

file = joinpath(@__dir__,"updown.h5")
(isfile(file)) && rm(file)
h5open(file,"cw") do fid
    fid["ds"] = collect(ds)
    fid["exs"]= collect(exs)
    long = create_group(fid,"long")
    long["window"] = windowsd1
    long["d2"] = [0.,0.]
    # short = create_group(fid,"short")
    # short["window"] = windowsd2
    # short["d1"] = [0.02,40.]
end

## heatmap
file = joinpath(@__DIR__,"updown.h5")
file =h5open(file,"r")
fid = read(file)
ds = fid["ds"]
exs = fid["exs"]
windowsd1 = fid["long"]["window"]
p =  heatmap(exs,ds, windowsd1, c = :amp)
yticks!(0:0.05:0.2, string.(collect(0:50:200)));
plot!(xlabel="Co-active synapses", ylabel="Input rate (Hz)", guidefontsize=18, tickfontsize=13, colorbar=false);


## Get four histo grams for examples
p =plot()
voltage = TN.simulate_fast(TN.Tripod(model),SIM_TIME, protocol; exc=40., prox=1., d1 = 0.02, d2=0.)
histogram!(
    voltage[1,:],
    inset = (1, bbox(0.05, 0.6, 0.30, 0.35, :bottom, :right)),
    subplot = 2,
    normalize=true, bins=range(-90, stop=-40, length=240), color=:black,lc=:match,
    bg_inside = :transparent,
    axis=false,
    legend=false)
kernel = globalKDE(3,voltage[1,:])
plot!(subplot = 2, lw=3,-90:-40,kernel, bg_inside=:transparent)

voltage = TN.simulate_fast(TN.Tripod(model),SIM_TIME, protocol; exc=10., prox=1., d1 = 0.05, d2=0.)
histogram!(
    voltage[1,:],
    inset = (1, bbox(0.65, 0.6, 0.30, 0.35, :bottom, :right)),
    subplot = 3,
    normalize=true, bins=range(-90, stop=-40, length=240), color=:black,lc=:match,
    bg_inside = :transparent,
    axis=false,
    legend=false)
kernel = globalKDE(3,voltage[1,:])
plot!(subplot = 3, lw=3,-90:-40,kernel, bg_inside=:transparent)

voltage = TN.simulate_fast(TN.Tripod(model),SIM_TIME, protocol; exc=40., prox=1., d1 = 0.15, d2=0.)
histogram!(
    voltage[1,:],
    inset = (1, bbox(0.35, 0.6, 0.30, 0.35, :bottom, :right)),
    subplot = 4,
    normalize=true, bins=range(-90, stop=-40, length=240), color=:black,lc=:match,
    bg_inside = :transparent,
    legend=false, axis=false)
kernel = globalKDE(3,voltage[1,:])
plot!(subplot = 4, lw=3,-90:-40,kernel, bg_inside=:transparent)
# scatter!(collect(-90:-40)[maxima], kernel[maxima])

voltage = TN.simulate_fast(TN.Tripod(model),SIM_TIME, protocol; exc=10., prox=1., d1 = 0.15, d2=0.)
histogram!(
    voltage[1,:],
    inset = (1, bbox(0.65, 0.05, 0.30, 0.35, :bottom, :right)),
    subplot = 5,
    normalize=true, bins=range(-90, stop=-40, length=240), color=:black,lc=:match,
    bg_inside = :transparent,
    axis=false,
    legend=false)
kernel = globalKDE(3,voltage[1,:])
plot!(subplot = 5, lw=3,-90:-40,kernel, bg_inside=:transparent)

savefig(p, joinpath(@__DIR__,"histograms_updown.pdf"))
