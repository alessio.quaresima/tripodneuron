include("../base.jl")
using Plots

function up(;tripod,step,dt, soma_in=false, two_spikes=false)
	if two_spikes
		if step ==10
			TN.exc_spike(tripod.d[1],100.)
		end
		if step ==1000
			TN.exc_spike(tripod.d[1],200.)
		end
	else
		if step ==10
			TN.exc_spike(tripod.d[1],200.)
		end
		if soma_in
		    # TN.set_rate(1.,tripod.s,0.5, dt)
		end
	end
end

function down(;tripod,step,dt, soma_in=false, two_spikes=false)
    # TN.set_rate(5.,tripod.d[1],.05,dt)
    # TN.set_rate(5.,tripod.d[2],.05,dt)
	if soma_in
	    TN.set_rate(1.,tripod.s,0.5, dt)
	end
end

function up_down()
	# neuron.s.v=-38
	s= plot()
	for soma_in in [false, true]
		neuron = TN.Tripod(TN.H_ball_stick)
		final, currents, final_synapses= TN.simulate_tripod(neuron, 200, down, record_synapses=true, soma_in=soma_in)
		voltage, currents, synapses= TN.simulate_tripod(neuron, 200,up, record_synapses=true, soma_in=soma_in)
		final =hcat(final,voltage)
		final_synapses =hcat(final_synapses,synapses)
		voltage, currents, synapses= TN.simulate_tripod(neuron, 200,down, record_synapses=true, soma_in=soma_in, two_spikes=true)
		final =hcat(final,voltage)
		final_synapses =hcat(final_synapses,synapses)
		voltage, currents, synapses= TN.simulate_tripod(neuron, 200,up, record_synapses=true, soma_in=soma_in)
		final =hcat(final,voltage)
		final_synapses =hcat(final_synapses,synapses)
		if !soma_in
			s= plot!(s,ylabel="voltage (mv)",final[1,1:1:end] ,label=["no soma input" "distal1" "diatal2"], color=TN.RED, linestyle=:dash)
		end
		if soma_in
			s= plot!(ylabel="voltage (mv)",final[2,1:1:end] ,label="distal", linestyle=:solid, linewidth=3, color=TN.GREEN)
			s= plot!(s,ylabel="voltage (mv)",final[1,1:1:end] ,label=["soma" "distal1" "diatal2"], linewidth=3, color=:black, guidefontsize=18)
		end
	end
	xticks!(s, 1:2000:8001,string.(collect(0:200:800)))
	s= plot!(xlabel="time (ms)", legend=true)
	savefig(s, joinpath(@__DIR__,"updown_state.pdf"))
	return s
end

S = up_down()
## run simulation

plot!(legend=:topright)
savefig(S, joinpath(@__DIR__,"updown.pdf"))
# up_single_dend()


function plot_v_nullcline(r, my_range, i_ext,label, color)
	V(v)= TN.AdEx.gl *( -v +TN.AdEx.Er +  TN.AdEx.ΔT * exp(TN.AdEx.ΔT⁻*(v-TN.AdEx.θ))) + i_ext
	plot!(clim=(-70,-45),r,my_range,V.(my_range), legend=false, color = color)
	# plot!(r,my_range,W.(my_range), label="")

	return r
end
# final, final_current, final_synapses= simulate_tripod(neuron, 1000, up, record_synapses=true)

function get_zero(i_ext)
	V(v)= TN.AdEx.gl *( -v +TN.AdEx.Er + TN.AdEx.ΔT * exp(TN.AdEx.ΔT⁻*(v-TN.AdEx.θ))) + i_ext
	values = [-80:0.001:-45...]
	x = findfirst(x-> x<0, V.(values))
	return values[x]
end

function middle(;tripod,step,dt, freq, efficacy=1.)
    TN.set_rate(efficacy,tripod.d[1],freq,dt)
    TN.set_rate(efficacy,tripod.d[2],freq,dt)
    # set_rate(12.,t.s,0.2, dt)
    # exc_spike(t.d[1],0.11)
end
get_zero(720)

function middle_freq_plot()
	CList = reshape( range(TN.BLU, stop=TN.RED,length=freqs.len), 1, freqs.len );
	r = plot()
	dvmean= Vector()
	svmean= Vector()
	for (n, freq) in enumerate(freqs)
	    v,c,sp,s,_ = TN.run_simulation(middle, synapses=true,freq=freq)
		# ext_current = (syn.AMPA_rev  - mean(v[1,:])) * mean(s[1,:,1]) +
		# 		   (syn.GABAa_rev - mean(v[1,:])) * mean(s[3,:,1]) +
		# 		   (syn.GABAb_rev - mean(v[1,:])) * mean(s[4,:,1]) +
		ext_current = mean(c[1,2000:end])
		println(ext_current)
		# - mean(c[end,:])
		plot_v_nullcline(r,-70:0.2:-45,ext_current, string(freq), CList[n])
		r= scatter([get_zero(ext_current)],[0], color=:black)
		# println(sp)
		push!(dvmean,mean(v[2,2000:3000]))
		push!(svmean,mean(v[1,2000:3000]))
	end
	q = plot(freqs, svmean , label="soma")
	q = plot!(q,freqs, dvmean , label="dendrite")
	return (r,q)
end
const freqs = 0:0.1:3
gr()
(p,q) = middle_freq_plot()
plot!(p,ylim=[-700,400], [TN.AdEx.θ, TN.AdEx.θ],[-700,1000], color=:green, linestyle=:dash)
plot!(p,ylim=[-700,400], [TN.AdEx.Er, TN.AdEx.Er],[-700,1000], color=:black, linestyle=:dash)
# plot!(p,-70:0.1:-45,W.(-70:0.1:-45), color=:black, linestyle=:solid)
myfont = Plots.Font("sans-serif", 10, :hcenter, :vcenter, 0.0, colorant"black")
myfont.rotation=90
annotate!(p, [(-51, 150, text("Exp. upswing", myfont,rotation=90))])
annotate!(p, [(-69.5, -500, text("Resting state", myfont))])
annotate!(p, [(-67, 100, Plots.text("Fixed points \n(no spikes regime)", 10, :black,    :left))])
plot!(xlabel="soma membrane voltage (mV)", ylabel="adaptation current w (pA)")
savefig(p,"fixed_point.pdf") # save the most recent fig as fn

function middle_freq_onset()
	q = plot()
	for (α,ls) in zip([10, 1., 0.5],[:solid, :dash, :dot])
		my_freqs = Array(0:1/α:20/α)
		dvmean= Vector()
		svmean= Vector()
		for (n, freq) in enumerate(my_freqs)
		    v,c,sp,s,_ = TN.run_simulation(middle, synapses=true, freq=freq, efficacy=α)
			push!(dvmean,mean(v[2,2000:3000]))
			push!(svmean,mean(v[1,2000:3000]))
		end
		q = plot!(q, my_freqs*α, svmean , label="",color=TN.BLU,   linewidth=4, linestyle=ls, alpha=1)
		q = plot!(q, my_freqs*α, dvmean , label="",color=TN.RED,linewidth=4,  linestyle=ls,alpha=1)
	end
	plot!(q, [[],[]],linewidth=4, color=[TN.BLU TN.RED], label=["soma" "dendrite"], legend=:topleft)
	return q
end
q = middle_freq_onset()
plot!(q,ylabel="Membrane potential (mV)", xlabel= "dendritic input")
plot!(q,[0,50],[TN.AdEx.Er,TN.AdEx.Er], color=:black, linestyle=:dash, label="")
plot!(q,[0,50],[TN.AdEx.θ, TN.AdEx.θ], color=:black, linestyle=:dash, label="")
annotate!(q, [(10, -47, text("Exp. upswing", myfont,rotation=0)),(10, -67, text("Resting state", myfont, rotation=0))])
plot!(q, legend=:topleft, ylims=(-75,0), xlims=(0,20))
savefig(q,"Upstate_onset.pdf")


# v, c, s= simulate_tripod(neuron, 5000,up, record_synapses=true)
# ext_current = (syn.AMPA_rev  - mean(v[1,:])) * mean(s[1,:,1]) +
# 		   (syn.GABAa_rev - mean(v[1,:])) * mean(s[3,:,1]) +
# 		   (syn.GABAb_rev - mean(v[1,:])) * mean(s[4,:,1]) +
# 		   mean(c[1,:]) - mean(c[end,:])
# plot_v_nullcline(r,-70:0.2:-45,ext_current, "up state")

v,c,sp,s,_ = run_simulation(middle_freq, synapses=true)
plot_tripod(Tripod(1),v,c,nothing)
nothing

function make_animation(file_destination,gif_title, v,c,s)
	# file_destination = "OR.gif"
	function plot_v_nullcline(r, my_range,i_ext, mean=false)
		w(v)= AdEx.gl *( -v +AdEx.Er +  AdEx.ΔT * exp(AdEx.ΔT⁻*(v-AdEx.θ))) + i_ext
		if mean
		plot!(r,my_range,w.(my_range), label="nullcline", legendtitle="AdEx", linestyle=:dash)
		else
		plot!(r,my_range,w.(my_range), label="", legendtitle="AdEx", color=:black, alpha=0.2)
		end
		return r
	end
	xlim=(minimum(v[1,:]),-20)
	ylim=(minimum(c[5,:]), maximum(c[5,:])+20)

	mean_current = (syn.AMPA_rev  - mean(v[1,:])) * mean(s[1,:,1]) +
		   (syn.GABAa_rev - mean(v[1,:])) * mean(s[3,:,1]) +
		   (syn.GABAb_rev - mean(v[1,:])) * mean(s[4,:,1]) +
		   mean(c[1,:])- mean(c[end,:])
	anim = @animate for i in 1000:15:3000
	     r = scatter(v[1,i:i+1],c[5,i:i+1], xlim=xlim, ylim=ylim, c=:red, markerstrokecolor=:red, label="Soma")
	     for shade in 1:20
	         z = i-shade
	         scatter!(r,v[1,z:z+1],c[5,z:z+1], xlim=xlim, ylim=ylim, alpha=1/shade, c=:blue, markerstrokecolor=:blue, label="")
	     end
		 ext_current = (syn.AMPA_rev  - v[1,i]) * mean(s[1,i-3:i+3,1]) +
					   (syn.GABAa_rev - v[1,i]) * mean(s[3,i-3:i+3,1]) +
					   (syn.GABAb_rev - v[1,i]) * mean(s[4,i-3:i+3,1]) +
					   c[1,i]
		 r = plot_v_nullcline(r,xlim[1]:0.5:xlim[2],ext_current)
		 r = plot_v_nullcline(r,xlim[1]:0.5:xlim[2],mean_current, true)
	     scatter!(r,v[1,i:i+1],c[5,i:i+1], xlim=xlim, ylim=ylim, c=:blue, markerstrokecolor=:red, label="")
		 plot!(r, title=gif_title)
		 plot!(r,xaxis=("Soma (mv)"))
		 plot!(r,yaxis=("Adaptation current (nA)"))
		 tt = round(i*dt, digits=1)
	        r = annotate!(r, -30, 0,  text("t = $tt ms"))
		 end
	return gif(anim, file_destination, fps = 10)
end

function make_plots(path, state)
	neuron = Tripod(1,conductance="low")
	if !ispath(path)
		mkdir(path)
	end
	if state=="UP"
		v, c, s= simulate_tripod(neuron, 5000,up, record_synapses=true)
	else
		v, c, s= simulate_tripod(neuron, 5000,down, record_synapses=true)
	end
	dynamics = plot(plot_tripod(Tripod(1),v,c,s)..., layout=(2,1))
	savefig(dynamics,state*"plot.pdf") # save the most recent fig as fn
	make_animation(path*"/state"*state*".gif", state*" state", v,c,s)
end
#
make_plots("test","UP")
make_plots("test","DOWN")


function up_single_dend()
	neuron = TN.Tripod(TN.H_ball_stick)
	final, currents, final_synapses= TN.simulate_tripod(neuron,200 , up_single, record_synapses=true)
	voltage, currents, synapses= TN.simulate_tripod(neuron, 200,down, record_synapses=true)
	final =hcat(final,voltage)
	final_synapses =hcat(final_synapses,synapses)
	voltage, currents, synapses= TN.simulate_tripod(neuron, 200,up, record_synapses=true)
	final =hcat(final,voltage)
	final_synapses =hcat(final_synapses,synapses)
	voltage, currents, synapses= TN.simulate_tripod(neuron, 200,down, record_synapses=true)
	final =hcat(final,voltage)
	final_synapses =hcat(final_synapses,synapses)
	# final =final[:,500:2000]
	q= plot(ylabel="voltage (mv)",final[1,1:1:end] ,label=["soma" "distal1" "diatal2"],  legendtitle="Compartments", legendtitlefontsize=8)
	# q= plot!(ylabel="voltage (mv)",final[2,1:1:end] ,label="distal1", linestyle=:solid, legendtitle="Compartments", legendtitlefontsize=8)
	q= plot!(ylabel="voltage (mv)",final[3,1:1:end] ,label="distal2", linestyle=:dash, legendtitle="Compartments", legendtitlefontsize=8)
	# q= plot!(ylabel="voltage (mv)",final[4,1:2:end] ,label="proximal", color=:black, linestyle=:dash, legendtitle="Compartments", legendtitlefontsize=8)
	xticks!(q, 1:2000:8001,string.(collect(0:200:800)))
	q= plot!(xlabel="time (s)")
	savefig(q, "updown_single.pdf")
	return q
end
