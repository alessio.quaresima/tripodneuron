"""This file reproduces Fig9b"""

include("../base.jl")
using Plots
using HDF5
using ColorSchemes
pyplot()

# model = "1->3, 250,2; 2->3, 250,2; 3->s, 100,4"
SIM_TIME = 10000
model = TN.H_distal_proximal

function protocol(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  distal::Real,  probe::Real=1., inh_comp=nothing)
        if step <500 && step >100
            TN.set_rate!(tripod.d[1], distal, eff=5.)
        end
        if step >500 && step <600
            if inh_comp=="soma"
                TN.set_rate!(tripod.s, -1., eff=5.)
            elseif inh_comp=="dend"
            @show "inhibition"
                TN.set_rate!(tripod.d[1], -1., eff=5.)
            end
        end
        if step > (500+(probe/dt))
            TN.set_rate!(tripod.d[2], 1., eff=1.)
        end
end

## simulation
reverse_amp=[]
for a in 1:256
    x = reverse(ColorSchemes.amp)[a]
    push!(reverse_amp,RGBA(x,1))
end
reverse_amp = cgrad(reverse_amp)

function test_memory(inh_comp=nothing)
    SIM_TIME = 250
    distal_rate = 0:0.2:5
    probes = 0:10:150
    spikes = zeros(500,length(distal_rate), length(probes))
    Threads.@threads for p in eachindex(probes)
        for n in eachindex(distal_rate)
            for x in 1:500
                probe=probes[p]
                tripod = TN.Tripod(TN.H_distal_proximal)
                v = TN.simulate_fast(tripod, 200, protocol, distal=distal_rate[n], probe=probe, inh_comp=inh_comp)
                first = findfirst(x->x>-20, v[1,500:end])
                spikes[x,n,p] = isnothing(first) ? SIM_TIME : first*0.1
            end
        end
    end
    return spikes
end
function make_plot(data, dostd=false)
    distal_rate = 0.2:0.2:5
    probes = 0:10:150
    p = heatmap(probes, distal_rate, mean(data,dims=1)[1,:,:],c=reverse_amp, xlabel="Proximal input delay (ms)", ylabel="Distal input rate (Hz)", guidefontsize=18, tickfontsize=13, titlefontsize=18, frame=:axes, colorbar=false)
    if dostd
        annotate!([(0.80*155,5*0.4, Plots.text("Relative\nstd. deviation", 14, :black, :center, :bottom))])
        annotate!([(0.80*70,5*0.1, Plots.text("> 200 ms", 14, :black, :center, :bottom))])
        annotate!([(0.80*15,5*0.30, Plots.text("< 50 ms", 14, :white, :center, :bottom))])
        plot!(p, title="Mean first spike time (ms)")
        inset = (1, bbox(0.01, 0.01, 0.35, 0.35, :bottom, :right))
        σ = std(data,dims=1)[1,:,:]./ mean(data,dims=1)[1,:,:]
        p = heatmap!(probes, distal_rate, inset=inset, subplot=2, -σ,c=reverse_amp, frame=:box, xticks=([],[]), yticks=([],[]), clims=(-1,0), colorbar=false)
    end
    return p
end

##

full_spikes = test_memory()
full_spikes_inh_soma = test_memory("soma")
full_spikes_inh_dend = test_memory("dend")

file= joinpath(@__DIR__, "spike_time_data.h5")
isfile(file) && (rm(file))
h5open(file, "w") do fid
    fid["no_inhib"]= full_spikes
    fid["dend_inhib"]= full_spikes_inh_dend
    fid["soma_inhib"]= full_spikes_inh_soma
end
##
file= joinpath(@__DIR__, "spike_time_data.h5")
fid = h5open(file, "r")
full_spikes= read(fid["no_inhib"])
full_spikes_inh_soma = read(fid["soma_inhib"])
full_spikes_inh_dend = read(fid["dend_inhib"])

p1 = make_plot(full_spikes_inh_dend-full_spikes_inh_soma)
p2 = make_plot(full_spikes_inh_dend-full_spikes)
p3 = make_plot(full_spikes, true)
savefig(p1,joinpath(@__DIR__, "memory_time_soma.pdf"))
savefig(p2,joinpath(@__DIR__, "memory_time_dend.pdf"))
savefig(p3,joinpath(@__DIR__, "memory_time.pdf"))
