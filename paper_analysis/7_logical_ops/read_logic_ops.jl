"""
This file reproduces Fig 9a
"""


##
include("../base.jl")
using HDF5
using StatsBase
using Plots
using ColorSchemes
using StatsPlots
using RecipesPipeline
using CategoricalArrays
using LaTeXStrings

function Base.unique(ctg::CategoricalArray)
    l = levels(ctg)
    newctg = CategoricalArray(l)
    levels!(newctg, l)
end
pyplot()

200_000
conditions = [:a, :b, :ab, :none]
operators=(
    A =   [:a, :ab],
    B =   [:b, :ab],
    AND = [:ab],
    OR =  [:a, :b, :ab],
    XOR = [:a, :b],
    IM =  [:none, :a, :ab ],
    MI =  [:none, :b, :ab ],
)

operators_labels = collect(map(x->String(x), keys(operators)))
operators_labels

SYMBOL_TIME = 50
SIMTIME = 200_000
RATE = 3.

##
file = joinpath(@__DIR__,"data/data_logical_ops.h5")
fid  = h5open(file, "r")
scores = read(fid["logic"])
soma = scores[:,4]
scores[:,4:5]=scores[:,5:6]
scores[:,6] = soma
confusion = read(fid["confusion"])
models_labels = read(fid["models"])
models_labels[4] = models_labels[6]
models_labels[6] =  "soma only"

ctg = CategoricalArray(repeat(models_labels, inner = 7))
nam = CategoricalArray(repeat(operators_labels, outer = 6))
levels!(ctg, models_labels)
levels!(nam, operators_labels)

colors=[TN.BLU TN.BLU TN.BLU TN.RED TN.RED :black]
pyplot()
p1= groupedbar(nam, scores, group = ctg, xlabel = "Operators",
        title = "", ylabel="Score", bar_width = 0.67, lw =0.5,background_legend=:white,legend=:bottomright, frame = :axes, ylims=(0.5,1.), legendtitle="Tripod configuration", legendfont=12,
        lc=:black, c=colors)
operators_labels[end-1] = "-A V B"
operators_labels[end] = "-B V A"
xticks!(0.5:6.5,operators_labels)
p1=plot!(tickfontsize=13, guidefontsize=18,background_color=:white)

# heatmap(log.(scores), xticks=(1:5,models_labels), yticks=(1:6, operators_labels))
savefig(p1,joinpath(@__DIR__,"figures","logic_ops.pdf"))

labels_A = models_labels

labels_A[4] = "sh.distal - proximal"
labels_A[end] = "soma only"

labels_A
plots=[]
for x in [1,2,3,6,5,4]
    p=heatmap( 1 .- confusion[x,:,:], colorbar=false, clims=(0,1),
    c=:amp, title=labels_A[x])
    yticks!((1:7, operators_labels))
    xticks!((1:4, string.(conditions)))
    push!(plots, p)
    # annotate!()
end

p = plot(plots...)
savefig(p, joinpath(@__DIR__, "figures","confusion_matrix.pdf"))
