"""
In this file we will explore the computational property of the Tripod in distinguishing between different input conditions.
Namely:

        A   B   AND OR  XOR IM
_ _     o   o   o   o   o   x
A _     x   o   o   o   x   x
_ B     o   x   o   x   x   o
A B     x   x   x   x   o   x

The neuron activity is divided in blocks of 50ms and each block is scored against the
operators. For each operator there is a trained classifier that evaluate the neuron is
the accepted condition.
"""
##
include("../base.jl")
using HDF5
using Random
using MLJ
using MLDataUtils
using MLJLinearModels
using StatsBase
using Plots
using ColorSchemes
pyplot()
RATE = 3.

##

conditions = [:a, :b, :ab, :none]
operators=(
    A =   [:a, :ab],
    B =   [:b, :ab],
    AND = [:ab],
    OR =  [:a, :b, :ab],
    XOR = [:a, :b],
    IM =  [:none, :a, :ab ],
    MI =  [:none, :b, :ab ],
)

dd() = TN.Tripod(TN.H_distal_distal);
pp() = TN.Tripod(TN.H_proximal_proximal);
ss() = TN.Tripod("H. 1->1, 100,4; 2->2,100,4");
mm() = TN.Tripod("H. 1->s, 250,4; 2->s,250,4");
dp() = TN.Tripod(TN.H_distal_proximal);
mp() = TN.Tripod(TN.H_medial_proximal);
models = [dd mm pp ss dp mp]

SYMBOL_TIME = 50
SIMTIME = 20_000*5
function stimulus(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64,
                  in_rate::Real,
                  condition::Symbol)
    if(condition ==:a)
      TN.set_rate!(tripod.d[1], in_rate )
      TN.set_rate!(tripod.d[1], -in_rate)
    elseif(condition ==:b)
      TN.set_rate!(tripod.d[2], in_rate )
      TN.set_rate!(tripod.d[2], -in_rate)
    elseif(condition ==:ab)
      TN.set_rate!(tripod.d[1], in_rate )
      TN.set_rate!(tripod.d[2], in_rate )
      TN.set_rate!(tripod.d[1], -in_rate)
      TN.set_rate!(tripod.d[2], -in_rate)
  end end

function generate_sequence(model::Function)
    """
    Generate a sequence of states from a simulation with random configurations
    :a :b :none :ab
    Each state is composed of SYMBOL_TIME*dt features, that is the membrane value
    of the soma during the exposition to the stimulus.
    The soma without dendrites is modeled with two synapses
    return the sequence of membrane values and the labels
    """
    n_symbols = round(Int,SIMTIME/SYMBOL_TIME)
    membrane = zeros(SYMBOL_TIME*10,3, round(Int, n_symbols))
    adaptation = zeros(SYMBOL_TIME*10, round(Int, n_symbols))
    inputs = Array{Symbol, 1}(undef,n_symbols)
    if model == ss
        println("Model: ", model)
        _tripod = model()
        for x in 1:n_symbols
            condition=rand(conditions)
            TN.simulate_tripod_soma(_tripod, 300, stimulus, in_rate=RATE, condition=:none, record_currents=true )
            volt, adapt = TN.simulate_tripod_soma(_tripod, SYMBOL_TIME, stimulus, in_rate=RATE, condition=condition, record_currents=true )
            membrane[:,:,x] = volt'
            adaptation[:,x] = adapt'
            inputs[x] = condition
        end
    else
        println("Model: ", model)
        _tripod = model()
        for x in 1:n_symbols
            condition=rand(conditions)
            TN.simulate_tripod(_tripod, 300, stimulus, in_rate=RATE, condition=:none )
            volt, current, _ = TN.simulate_tripod(_tripod, SYMBOL_TIME, stimulus, in_rate=RATE, condition=condition )
            membrane[:,:,x] = volt'
            adaptation[:,x] = current[end,:]'
            inputs[x] = condition
        end
    end
    return vcat(membrane[:,1,:], adaptation), inputs
end

function feat_from_data(data::Matrix{Float64})
    nr_feat = 5
    h = 500 ÷ nr_feat
    features = zeros(Float64, 2 + nr_feat*2, size(data)[2])
    for (n,sample) in enumerate(eachcol(data))
        features[3:nr_feat+2,n]   = [mean(sample[1+h*x:(x+1)*h]) for x in 0:nr_feat-1]
        features[nr_feat+3:end,n] = [mean(sample[500+1+h*x:500+(x+1)*h]) for x in 0:nr_feat-1]
        features[1,n] = TN.get_spike_rate(sample[1:500])
        cv = TN.get_cv(sample[1:500])
        features[2,n] = isnan(cv) ? 0. : cv
    end
    return features
end
#
#
# scatter(mean(v[1:500,:], dims=1)[1,:],mean(v[500:end,:], dims=1)[1,:], c=
#
function _trainclassifier(;OP::Symbol, X::Matrix{Float64},inputs::Vector{Symbol}, λ=0.1::Float64)
    y = ones(size(inputs))*-1
    for i in eachindex(inputs)
        (inputs[i] ∈ getfield(operators,OP)) && (y[i] = 1)
    end
    train_std = StatsBase.fit(ZScoreTransform, X, dims=2)
    StatsBase.transform!(train_std,X)
    intercept = false
    # deploy MultinomialRegression from MLJLinearModels, λ being the strenght of the reguliser
    lr = LogisticRegression(λ; fit_intercept=intercept)
    # Fit the model
    θ  = MLJLinearModels.fit(lr, X', y)
    return (θ, train_std)
end

function _testclassifier(;OP::Symbol, X::Matrix{Float64},inputs::Vector{Symbol}, lr)
    y = ones(size(inputs))*-1
    for i in eachindex(inputs)
        (inputs[i] ∈ getfield(operators,OP)) && (y[i] = 1)
    end
    θ, train_std = lr
    StatsBase.transform!(train_std,X)
    preds = MLJLinearModels.apply_X(X',θ)
    # #and evaluate the model over the labels
    scores = mean(sign.(preds) .== y)
    return scores, [inputs, sign.(preds), y]
end

function train_classifiers(X::Matrix{Float64}, inputs::Vector{Symbol})
    classifiers = []
    _ops = []
    for op in keys(operators)
        lr = _trainclassifier(OP=op, X=X,inputs=inputs; λ=0.5::Float64)
        push!(classifiers, lr)
        push!(_ops, op)
    end
    # for op in keys(operators)
    #     lr = _trainclassifier(OP=op, X=X,inputs=shuffle(inputs); λ=0.5::Float64)
    #     push!(rnd_classifiers, lr)
    # end
    classifiers = (;zip(_ops,classifiers)...)
    # rnd_classifiers = (;zip(_ops,rnd_classifiers)...)
    return classifiers#, rnd_classifiers
end

function test_classifiers(X::Matrix{Float64}, inputs::Vector{Symbol}, classifiers::NamedTuple)
    scores = []
    confusion = []
    for op in keys(operators)
        score, data = _testclassifier(OP=op, X=X,inputs=inputs,lr=getfield(classifiers,op))
        push!(scores, score)
        push!(confusion, data)
    end
    return scores, confusion
end

function test_model(model)
    data, inputs = generate_sequence(model)
    features = feat_from_data(data)
    classifiers = train_classifiers(features, inputs)
    data, inputs = generate_sequence(model)
    features = feat_from_data(data)
    scores, confusion = test_classifiers(features, inputs, classifiers)
    # rnd_scores = test_classifiers(features, inputs, rnd_classifiers)
    return scores, confusion
end

function test_rate(models, rate)
    global RATE = rate
    rates = zeros(size(conditions)[1],size(models)[2])
    for n in eachindex(models)
        for m in eachindex(conditions)
            _tripod = models[n]()
            condition = conditions[m]
            if models[n]==ss
                _ = TN.simulate_tripod_soma(_tripod, 1000, stimulus, in_rate=RATE, condition=condition )
                volt = TN.simulate_tripod_soma(_tripod, 10000, stimulus, in_rate=RATE, condition=condition )
            else
                _ = TN.simulate_fast(_tripod, 1000, stimulus, in_rate=RATE, condition=condition )
                volt = TN.simulate_fast(_tripod, 10000, stimulus, in_rate=RATE, condition=condition )
            end
            rates[m,n] = TN.get_spike_rate(volt)
        end
    end
    return rates
end

function get_confusion_matrix(confusion_data)
    inputs_position = [] # (none, a, b, ab)
    final_scores = zeros(6,7,4)
    inputs = confusion_data[1][1][1]
    for sym in conditions
        push!(inputs_position,findall(x->x==sym, inputs))
    end
    for m in eachindex(confusion_data)
        sub_score = zeros(7,4)
        for op in eachindex(confusion_data[m])
            for pos in eachindex(inputs_position)
                sub_score[op,pos] = mean(confusion_data[m][op][2][inputs_position[pos]] .==confusion_data[m][op][3][inputs_position[pos]])
            end
        end
        final_scores[m,:,:] = sub_score
    end
    return final_scores
end
#

## Run simulation
scores_list = []
confusion_data = []
for model in models
    score, confusion = test_model(model)
    push!(scores_list, score )
    push!(confusion_data, confusion )
end

models_labels = ["distal - distal","short distal - short distal","proximal - proximal", "soma-only", "distal - proximal", "short distal - proximal"]
scores = hcat(scores_list...)
# rnd_scores = hcat(rnd_scores...)
operators_labels = collect(map(x->String(x), keys(operators)))
scores[:,1] = scores_dd
confusion_data[1] = confusion
heatmaps = []
confusion = get_confusion_matrix(confusion_data)
for m in 1:6
    h = heatmap(1 .- confusion[m,:,:], c=:amp, clims=(0,1), colorbar=false)
    push!(heatmaps,h)
end
plot(heatmaps...)


scores
file = joinpath(@__DIR__,"data","data_logical_ops.h5")
rm(file)
h5open(file, "w") do fid
    fid["confusion"] = confusion
    fid["logic"]  = scores
    fid["models"] = models_labels
end

## DD has troubles
# v, i = generate_sequence(dd)
# feat = feat_from_data(v)
# states = map(y->findfirst(x-> x==y, conditions),i)
#
#
# colors = map(x->TN.color_list[x],states)
# plotlyjs()
# scatter(feat[22,:], feat[11,:], feat[1,:], c=colors)
#
# score = test_model(dd)
#
model = dd
data, inputs = generate_sequence(model)
classifiers= train_classifiers(data, inputs)
data, inputs = generate_sequence(model)
scores_dd, confusion = test_classifiers(data, inputs, classifiers)
