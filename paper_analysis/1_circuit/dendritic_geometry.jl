"""
This file reproduces fig.1.
We use the physiological parameters reported in
    (human) Eyal, G.; Verhoog, M. B.; Testa-Silva, G.; Deitcher, Y.; Lodder, J. C.; Benavides-Piccione, R.; Morales, J.; DeFelipe, J.; de Kock, C. P.; Mansvelder, H. D.; Segev, I. Unique Membrane Properties and Enhanced Signal Processing in Human Neocortical Neurons. eLife 2016, 5, e16553. https://doi.org/10.7554/eLife.16553.
    (mouse) (1) Dasika, V. K.; White, J. A.; Colburn, H. S. Simple Models Show the General Advantages of Dendrites in Coincidence Detection. Journal of Neurophysiology 2007, 97 (5), 3449–3459. https://doi.org/10/dc8tr4.
and plot the conductance as a function of the dendritic geometry
"""

## include Tripod
include("../base.jl")
using Plots
pyplot()
using LaTeXStrings
## Set the param and compute conductance
Ri    = TN.MOUSE.Ri
Rd    = TN.MOUSE.Rd

# TN.G_axial(Ri=Ri,d=10TN.μm,l=100TN.μm)
βC = 0.5
ds = 0.1:0.01:6
ls = 50:1.:550
gs = Vector{Float64}()
βm = Vector{Bool}()
βdistal_sup = Vector{Bool}()
βdistal_inf = Vector{Bool}()
for d_ in ds
    for l_ in ls
        global gs
        gm =  TN.G_mem(Rd=Rd,d=d_*TN.μm,l=l_*TN.μm)
        g = TN.G_axial(Ri=Ri,d=d_*TN.μm,l=l_*TN.μm)
        push!(gs,g.val)
        push!(βdistal_sup,g.val>βC*TN.AdEx.gl)
        push!(βdistal_inf,g.val>0.5βC*TN.AdEx.gl)
        push!(βm,gm.val<g.val)
    end
end
βm = reshape(βm, length(ls), ds.len);
pos_mouse= map(x-> x == nothing ? 1 : x, map(y->findlast(x->x,y), eachcol(βm)));

Ri    = TN.HUMAN.Ri
Rd    = TN.HUMAN.Rd
βm = Vector{Bool}()
for d_ in ds
    for l_ in ls
        global gs
        g = TN.G_axial(Ri=Ri,d=d_*TN.μm,l=l_*TN.μm)
        gm =  TN.G_mem(Rd=Rd,d=d_*TN.μm,l=l_*TN.μm)
        push!(βm,gm.val<g.val)
    end
end
βm = reshape(βm, length(ls), ds.len);
pos_human= map(x-> x == nothing ? 1 : x, map(y->findlast(x->x,y), eachcol(βm)));


##
# gr()
# p1 = heatmap(ds,ls,reshape(gs, length(ls), ds.len), xlabel="diameter (μm)", colorbar=false, ylabel="length (μm)");
p1 = plot(xlabel="diameter (μm)", colorbar=false, ylabel="length (μm)");
βs_sup = reshape(βdistal_sup, length(ls), ds.len);
βs_inf = reshape(βdistal_inf, length(ls), ds.len);
pos_inf= map(x-> x == nothing ? 1 : x, map(y->findlast(x->x,y), eachcol(βs_inf)));
pos_sup= map(x-> x == nothing ? 1 : x, map(y->findlast(x->x,y), eachcol(βs_sup)));
plot!(p1,ds,ls[pos_sup], linewidth=2, color=:black, linestyle=:solid, label="");
plot!(p1,ds,ls[pos_inf], linewidth=2, color=:black, linestyle=:solid, label="");
ls[pos_sup[391]]
pos_inf
my_font = Plots.text("").font
my_font.rotation = 45
annotate!([(4.3,500,Plots.text(L"\frac{\beta}{2}<\frac{g_{ax}}{g^s_m}<\beta", :black,font))]);
annotate!([(5.7,500,Plots.text(L"\frac{g_{ax}}{g^s_m}>\beta",:black,font))]);
annotate!([(1.4,500,Plots.text(L"\frac{g_{ax}}{g^s_m}<\frac{\beta}{2}",:black,font))]);
annotate!([(1.4,450,Plots.text("No spiking \nregime", :black,:top,font))]);
annotate!([(5.5,450,Plots.text("Spiking\nregime", :black,:top,font))]);

myfont = Plots.text("").font
myfont.rotation =0


plot!(p1,ds,ls[pos_mouse], linewidth=2, color=:grey, linestyle=:dot, label="");
plot!(p1,ds,ls[pos_human], linewidth=2, color=:grey, linestyle=:dot, label="");
annotate!(5,250,Plots.text("Mouse \n"*L"g^d_{m}<g_{ax}", myfont, :grey));
annotate!(0.8,250,Plots.text("Human \n"*L"g^d_{m}<g_{ax}", myfont, :grey));
myfont = Plots.text("").font
myfont.rotation =19
annotate!(3.2,275,Plots.text("Max Length", myfont, :grey));
y = [150, 150, 400]
x = [4, 2.5, 4]

# plot!(rectangle(1.8,30,1.5,170), color=:white, alpha=0.8, marginrcolor=:white, label="")
scatter!(x, y, color=[TN.RED, TN.BLU, TN.GREEN], labels="", markershape=:circle, markersize=5);
annotate!([(4,180,Plots.text("Proximal", 14, TN.RED))]);
annotate!([(2,180, Plots.text("Proximal thin", 14, TN.BLU))]);
annotate!([(4,430,Plots.text("Distal", 14, TN.GREEN ))]);
plot!(p1, ylims=(60,540), title="");
plot!(p1, foregroundcolor=:white, ticks=true, frame=:full, guidefontsize=18, tickfontsize=13);
savefig(p1,joinpath(@__DIR__,"figures","fig1a.pdf"));
p1
