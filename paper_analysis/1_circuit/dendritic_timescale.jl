
"""
This file reproduces Fig. 1B.

In the file we compute the dendritic integration timescale of the dendritic compartments, for all the physiological condition (HUMAN, MOUSE) and all the dendritic lengths.
"""
## include Tripod
include("../base.jl")
using Plots
pyplot()
using LaTeXStrings

dr = 100:1:500
βC = 0.5
timescale = zeros(Float64,4,length(dr) )
colors = Array{typeof(TN.GREEN),2}(undef, 4, length(dr))


rectangle(w, h, x, y) = Shape(x .+ [0,w,w,0], y .+ [0,0,h,h])
function get_timescale(params)
    g1, g2, C = params
    return C/(g2+g1)
end

function get_color(params)
    g1, g2, C = params
    G = βC*TN.AdEx.gl
    if 0.5G > g2
        return TN.GREY
    elseif G > g2 && 0.5G < g2
        return TN.GREEN
    else
        return TN.RED
    end
end


0.5*βC*TN.AdEx.gl
model ="M. 1->s, 100,4"
params = TN.dend_parameters(TN.Tripod(model).d[1])
get_color(params)
for n in eachindex(dr)
    for (a,S) in zip([1,2],["H","M"])
        d  = dr[n]
        model ="$S. 1->s, $d,4"
        params = TN.dend_parameters(TN.Tripod(model).d[1])
        τ = get_timescale(params)
        c = get_color(params)
        timescale[a,n] = τ
        colors[a,n] = c
    end
    for (a,S) in zip([3,4],["H","M"])
        d  = dr[n]
        model ="$S. 1->s, $d,2.5"
        params = TN.dend_parameters(TN.Tripod(model).d[1])
        τ = get_timescale(params)
        c = get_color(params)
        timescale[a,n] = τ
        colors[a,n] = c
    end
end
##
p = plot()
plot!([150,150],[0,4], c=:black, ls=:solid, lw=5, alpha=0.3 ,label="");
plot!([400,400],[0,4], c=:black, ls=:solid, lw=5, alpha=0.3 ,label="");
plot!(100:500,timescale[1,:],  lw=6, alpha=0.5,label="Human");
plot!(100:500,timescale[2,:],  lw=6, alpha=0.5,label="");
plot!(100:500,timescale[3,:],  lw=6, alpha=0.5,label="");
plot!(100:500,timescale[4,:],  lw=6, alpha=0.5,label="");
plot!(100:500,timescale[2,:],color=:black, lw=3,ls=:dot,label="");
plot!(100:500,timescale[4,:],color=:black, lw=3,ls=:dot,label="Mouse");
annotate!([(155,3.8, Plots.text("Proximal", 14, :black, :left))])
annotate!([(405,3.8,Plots.text("Distal", 14, :black, :left))])
ylabel!("Integration \ntimescale "*L"\tau_d"*" (ms)")
plot!(legendfontsize=12, bglegend=:transparent, fglegend=:transparent,grid=false, guidefontsize = 18, tickfontsize=13, frame=:frame)

xlabel!("dendritic length "*L"(\mu m)")
savefig(p,joinpath(@__DIR__,"figures","fig1b.pdf"));
##
