"""
This file measures the effect of interaction between two excitatory conductances and reproduces Fig 4a and Fig 4b
"""
##
include("../base.jl")
using LsqFit
using IterTools
using Plots
using LaTeXStrings
nmda_curr(v) =-(v)*TN.NMDA_nonlinear(TN.syn.NMDA, v::Float64)

dd() = TN.Tripod(TN.H_distal_distal);
pp() = TN.Tripod(TN.H_proximal_proximal);
mm() = TN.Tripod(TN.H_medial);
ss() = TN.Tripod(TN.H_ss);

function double_input(g1, g2, d1, d2)
    TN.dospike(d1,g1)
    TN.dospike(d2,g2)
end

function baserate(;tripod::TN.Tripod, step::Int, dt::Float64)
        TN.exc_spike!(tripod.d[2],eff=1.05)
        TN.exc_spike!(tripod.d[1],eff=0.05)
end

models = [dd, mm, pp, ss]

## functions

function gg_simulate(g1::Float64,g2::Float64; model::Function, same=false, baseinput=false, NMDA=false)
    currents = Array{Float64,1}(undef,4)
    spiked = false
	neuron = model()
	if !NMDA
		neuron.d[1].syn.NMDA.gsyn=0
		neuron.d[2].syn.NMDA.gsyn=0
	else
		eyal = deepcopy(TN.get_synapses_exc(TN.dt,"dend"))
		neuron.d[1].syn.NMDA.gsyn=eyal.NMDA.gsyn
		neuron.d[2].syn.NMDA.gsyn=eyal.NMDA.gsyn
	end
	base = TN.null_input
	if baseinput == true
		base = baserate
	end
	if model == TN.H_ss
	    voltage1 , _, _= TN.simulate_tripod_soma(neuron, 600, base)
	else
	    voltage1 , _, _= TN.simulate_tripod(neuron, 600, base)
	end
	rest = mean(voltage1[1,end-50:end])
    if same
        double_input(g1,g2,neuron.d[1],neuron.d[1])
    else
        double_input(g1,g2,neuron.d[1],neuron.d[2])
    end
	if model == TN.H_ss
	    voltage1 , _, _= TN.simulate_tripod_soma(neuron, 200, base)
	else
	    voltage, _, _= TN.simulate_tripod(neuron, 200, base)
	end
	inh=false
	if g1==0 && g2<0
		inh =true
	end
	epsp = TN.get_EPSP(voltage, spiketime=1, rest=rest, inh=inh)
    return epsp
	return vcat(voltage1[1,:],voltage[1,:]), rest, epsp

end

function gg_interaction(a,b; kwargs...)
	A = gg_simulate(a,0.;kwargs...)
	B = gg_simulate(0.,b;kwargs...)
	C = gg_simulate(a, b;kwargs...)
	return (C-(abs(A)+abs(B)))
	# return C,A,B
end


function gg_synaptic_efficacy(;gs=nothing, kwargs...)
	Δ = Vector()
	g = Vector()
	if isnothing(gs)
		gs = reshape(repeat(collect(1:1.:30),outer=2),30,2)
	end
	for (a,b) in eachrow(gs)
		push!(Δ, gg_interaction(a,b;kwargs...))
		push!(g,a*b)
	end
	return Δ, g
end

conditions = product([true, false] ,[true, false])
for (x,y) in conditions
	@show x, y
end
function gg_models( models )
	gs = rand(1.:30.,100,2)
	results = zeros(length(models), 2, 2, size(gs)[1]) # model, NMDA/AMPA, AA/AB, gs
	conditions = [true, false]
	for (n,model) in enumerate(models)
		for (nmda,NMDA) in enumerate(conditions)
			for (same,SAME) in enumerate(conditions)
				Δ, _ = gg_synaptic_efficacy(gs=gs, same=SAME, NMDA=NMDA, model=model )
				results[n, nmda, same, :] .= Δ
			end
		end
	end
	return results, gs
end

@. linear_model(x,p) = p[1]*x + p[2]

## Scattered inputs
results, gs = gg_models(models)
gg = gs[:,1] .* gs[:,1]
comp = 4
plots = []
for comp in 1:4
	p = Plots.scatter(gg, results[comp,1,1,:] ,markersize=6, c=TN.RED);
	Plots.scatter!(gg,results[comp,2,1,:],markersize=6, c=TN.BLU);
	Plots.scatter!(gg,results[comp,1,2,:],markersize=6, c=TN.RED, markershape=:cross);
	Plots.scatter!(gg,results[comp,2,2,:],markersize=6, c=TN.BLU, markershape=:cross, legend=false);
	Plots.plot!([minimum(gg), maximum(gg)], [0., 0.], ls=:dash, lw=3, c=:black)
	plot!(ylabel=L"\Delta"*"EPSP", xlabel=L"|g_x g_x| (nS)^2")
	push!(plots,p)
end
plot(plots...)



##
function interaction(model::Function)
	function add_plot(gg::Function, same::Bool, my_plot=nothing; label="", NMDA=false, model=model, max_y=1., kwargs...)
		Δ, g = gg_synaptic_efficacy(same=same, baseinput=false, NMDA=NMDA, model=model )
		fit = curve_fit(linear_model, g,Δ, [0.,0.])
		# fit = curve_fit(linear_model, g,Δ, [abs(Δ[3]-Δ[2]/(g[3] - g[2])), Δ[1]])
		# abs(fit.param[1])
		Δnorm = (Δ .- fit.param[2])#./g[end]
		if my_plot == nothing
		else
			plot!(my_plot, g, Δ; kwargs...)
		end
		n = length(my_plot.series_list)

		return my_plot, g, fit #, abs(fit.param[1])
	end
	p = plot()
	p, g, fit2 = add_plot(gg_interaction, true, p; model=model, linestyle=:dash, markersize=8,markerstrokecolor=TN.RED, linewidth=2, color= TN.RED, label=L"g_eg_e")
	p, g,  fit1 = add_plot(gg_interaction, true, p; model=model, linestyle=:solid, markersize=8,markerstrokecolor=TN.RED, linewidth=2, color= TN.RED, label=L"g_eg_e (NMDA)", NMDA=true)
	p, g, fit4 = add_plot(gg_interaction, false,p; model=model, linestyle=:dash, markersize=8,markerstrokecolor=TN.BLU, linewidth=2, color= TN.BLU, label=L"g_eg_e")
	p, g, fit3 = add_plot(gg_interaction, false, p;model=model, linestyle=:solid, markersize=8,markerstrokecolor=TN.BLU, linewidth=2, color= TN.BLU, label=L"g_eg_e (NMDA)", NMDA=true)
	return p,g, [fit1,fit2, fit3, fit4]
end

pdd, g, fitdd = interaction(dd)
yticks!(-6:3:6, string.(-6:3:6))
ppp, g, fitpp = interaction(pp)
yticks!(-6:3:6, string.(-6:3:6))
pmm, g, fitmm = interaction(mm)
yticks!(-6:3:6, string.(-6:3:6))

plot!(pdd, xlabel=L"|g_x g_x| (nS)^2", ylabel="", yaxis=true)
yticks!(-6:3:6, string.(-6:3:6))
plot!(pmm,ylabel=L"\Delta"*"EPSP (a.u.)")
plot!(pdd,tickfontsize=13, guidefontsize=18,background_color=:white)
# plot!(ppp, [[],[],[],[]], linestyle=[:dot :dash], color=[TN.RED TN.BLU TN.RED TN.BLU], )
p=plot(ppp, pmm, pdd, layout=(1,3), legend=false, ylims=(-6,6), markersize=5,tickfontsize=13, guidefontsize=18,background_color=:white, framestyle=:axes, xaxis=true)
plot!(ppp, [[],[],[],[]], linestyle=[:dot :dash], color=[TN.RED TN.BLU TN.RED TN.BLU])
savefig(p,joinpath(@__DIR__,"figures","fig4a.pdf"))

##

function estimate_fit(g, fit)
	@assert(length(fit.resid) == length(g))
	(sqrt(sum((fit.resid ) .^ 2 ))/(length(fit.resid)-1))
end

function all_fit(g,myfit)
	σ =[]
	μ =[]
	for fit in myfit
		push!(σ, estimate_fit(g,fit))
		push!(μ, fit.param[1])
	end
	return σ, μ
end

σdd, μdd = all_fit(g,fitdd)
σmm, μmm = all_fit(g,fitmm)
σpp, μpp = all_fit(g,fitpp)

σs = reshape(hcat(σpp..., σmm..., σdd..., ),(4,3))
μs = reshape(hcat(μpp..., μmm..., μdd..., ),(4,3))

## make plots
using StatsPlots
ctg = repeat(["AA \nNMDA", "AA \nAMPA", "AB \nNMDA", "AB \nAMPA"], outer = 3)
nam = repeat(["proximal" , "medial", "distal"] , inner = 4)


# Compute the reference residuals for a gaussian noise with Norm(x,x)
σref = mean(map(x->estimate_fit(g,curve_fit(linear_model, g,rand.(Normal.(g,1)), [0.,0.])),1:500))
c = [TN.GREEN TN.BLU TN.RED]

p1= groupedbar(ctg, 400μs, group = nam, xlabel = "Tripod configuration",
        title = "", ylabel=L"\Delta"*"EPSP (mV)", bar_width = 0.67,c = c, lw = 0, framestyle = :box, legend=false, ylims=(-4.,4.))

p2= groupedbar(ctg, σs./σref, group = nam, xlabel = "Tripod configuration", ylabel = "residuals of linear fit (σ)",
        title = "", bar_width = 0.67, c= c,
        lw = 0, framestyle = :box, ylims=(-0.9,3.90))
q = plot(p1,p2)
plot!(q,tickfontsize=13, guidefontsize=18,background_color=:white)

savefig(q,joinpath(@__DIR__,"figures","fig4b.pdf"))
#
