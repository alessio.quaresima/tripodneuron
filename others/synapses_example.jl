include("../TripodNeuron.jl")
using Plots
function protocol(;tripod=nothing,
                  soma=nothing,
                  step::Int64,
                  dt::Float64, somaspike=false)

    if step < 30
        if tripod != nothing
            if somaspike
                TN.exc_spike(tripod.s, 1.)
                TN.inh_spike(tripod.s, 1.)
            else
                TN.exc_spike(tripod.d[1], 1.)
                TN.inh_spike(tripod.d[1], 1.)
            end
        end
        if soma != nothing
            TN.exc_spike(soma,1.)
            TN.inh_spike(soma,1.)
        end
    end
end

duration =500
voltage, currents,spikes, synapses_dend, _ = TN.run_simulation(protocol; record_synapses=true, sim_time =duration)
voltage, currents,spikes, synapses_soma, _ = TN.run_simulation(protocol; record_synapses=true, sim_time =duration, somaspike=true)
sst_voltage, _, _, synapses_sst = TN.simulate_neuron(TN.SST(),protocol; record_synapses=true,  sim_time =duration)
sst_voltage, _, _,synapses_pv = TN.simulate_neuron(TN.PV(),protocol; record_synapses=true,     sim_time =duration)
a =plot(ylims = (0,1),TN.dt:TN.dt:duration,transpose(synapses_soma[1:4,:,1]));
b =plot(ylims = (0,1),TN.dt:TN.dt:duration,transpose(synapses_dend[1:4,:,2]));
c =plot(ylims = (0,1.5),TN.dt:TN.dt:duration,transpose(synapses_sst[1:4,:]));
d =plot(ylims = (0,1.5),TN.dt:TN.dt:duration,transpose(synapses_pv[1:4,:]));
plot(a, b, c, d, legend=false)
