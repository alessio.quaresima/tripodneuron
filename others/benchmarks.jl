using BenchmarkTools
include("../TripodNeuron.jl")
using Printf


const random_spikes = rand([true,false],round(Int,1000/TN.dt))
function protocol_rate(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
            TN.set_rate!(tripod.s,1.)
end

function protocol_spike_array(;
                  tripod::TN.Tripod,
                  step::Int64,
                  dt::Float64)
            (random_spikes[step]) && (TN.exc_spike!(tripod.d[2]))
end

function protocol_spike_array_lif(;
                  soma::TN.Soma,
                  step::Int64,
                  dt::Float64)
    TN.exc_spike(soma,1.)
end

function arrays()
      voltages= zeros((10000,3))
      currents= zeros((10000,3))
      for x in 1:10000
            voltages[x,:] .= rand(3)
            currents[x,:] .= rand(3)
      end
end
## Simple tests
sim_time= 1000
@printf "Tripod: %d ms non-random spikes protocol - fast" sim_time
tripod= TN.Tripod()
@btime TN.simulate_fast($tripod, $sim_time*1, $protocol_spike_array)
@btime TN.simulate_fast($tripod, $sim_time*1, $protocol_rate)
@printf "Random arrays"
@btime arrays()

Net.net_test();
#
## Other tests

@printf "Tripod: %d ms rate protocol" sim_time
@btime TN.run_simulation($protocol_rate; synapses=true, sim_time=$sim_time)

@printf "Tripod: %d ms spikes protocol" sim_time
@btime TN.run_simulation($protocol_spike; synapses=true, sim_time=$sim_time)

@printf "Tripod: %d ms spikes protocol - no synapses" sim_time
@btime TN.run_simulation($protocol_spike; synapses=false, sim_time=$sim_time)

@printf "Tripod: %d ms non-random spikes protocol" sim_time
@btime TN.run_simulation($protocol_spike_array; synapses=true, sim_time=$sim_time)

@printf "Tripod: %d ms non-random spikes protocol - no synapses" sim_time
@btime TN.run_simulation($protocol_spike_array; synapses=false, sim_time=$sim_time)

@printf "Tripod: %d ms non-random spikes protocol - no synapses" sim_time*10
@btime TN.run_simulation($protocol_spike_array; synapses=false, sim_time=$sim_time*10)

@printf "Tripod: %d ms non-random spikes protocol - no synapses" sim_time*100
@btime TN.run_simulation($protocol_spike_array; synapses=false, sim_time=$sim_time*100)

@printf "SST: %d ms non-random spikes protocol - no synapses" sim_time
@btime TN.simulate_neuron($TN.SST(),$protocol_spike_array_lif; synapses=false, sim_time=$sim_time)

@printf "PV: %d ms non-random spikes protocol - no synapses" sim_time
@btime TN.simulate_neuron($TN.PV(),$protocol_spike_array_lif; synapses=false, sim_time=$sim_time)
##
#==
Commit 59f3707d3da98d964979392b0d7670966dc1c66c
Tripod: 1000 ms rate protocol 49.342 ms (360046 allocations: 18.47 MiB)
Tripod: 1000 ms spikes protocol 47.821 ms (330046 allocations: 17.10 MiB)
Tripod: 1000 ms spikes protocol - no synapses 5.277 ms (110044 allocations: 6.72 MiB    )
Tripod: 1000 ms non-random spikes protocol 45.401 ms (325018 allocations: 17.02 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses 5.086 ms (105016 allocations: 6.64 MiB)

Commit ccd5ef56ca95ed54578927a80a9b6b87c3062e95
Tripod: 1000 ms rate protocol  82.215 ms (1378607 allocations: 91.12 MiB)
Tripod: 1000 ms spikes protocol  78.171 ms (1348607 allocations: 89.75 MiB)
Tripod: 1000 ms spikes protocol - no synapses  7.318 ms (110138 allocations: 10.73 MiB)
Tripod: 1000 ms non-random spikes protocol  79.292 ms (1343545 allocations: 89.67 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses  6.847 ms (105076 allocations: 10.65 MiB)

Commit
Tripod: 1000 ms rate protocol  76.530 ms (1378607 allocations: 91.12 MiB)
Tripod: 1000 ms spikes protocol  73.054 ms (1348607 allocations: 89.75 MiB)
Tripod: 1000 ms spikes protocol - no synapses  7.354 ms (110138 allocations: 10.73 MiB)
Tripod: 1000 ms non-random spikes protocol  74.919 ms (1348607 allocations: 89.75 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses  6.967 ms (110138 allocations: 10.73 MiB)
SST: 1000 ms non-random spikes protocol - no synapses  2.178 ms (30046 allocations: 878.52 KiB)
PV: 1000 ms non-random spikes protocol - no synapses  2.009 ms (30046 allocations: 878.52 KiB)

Commit 4b39b1af3dac6dfd77d23d6f448a54a307bcc4b7
Tripod: 1000 ms spikes protocol  79.862 ms (1348602 allocations: 89.75 MiB)
Tripod: 1000 ms spikes protocol - no synapses  7.049 ms (110133 allocations: 10.73 MiB)
Tripod: 1000 ms non-random spikes protocol  80.085 ms (1348602 allocations: 89.75 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses  7.080 ms (110133 allocations: 10.73 MiB)
Tripod: 10000 ms non-random spikes protocol - no synapses  83.323 ms (1100137 allocations: 107.16 MiB)
Tripod: 100000 ms non-random spikes protocol - no synapses  918.964 ms (11000138 allocations: 1.05 GiB)
SST: 1000 ms non-random spikes protocol - no synapses  2.269 ms (30046 allocations: 878.52 KiB)
PV: 1000 ms non-random spikes protocol - no synapses  2.062 ms (30046 allocations: 878.52 KiB)

Commit 0eb5496d72f9fe73a202fab4c78bfef4438dd3e2
Tripod: 1000 ms rate protocol  73.857 ms (1378602 allocations: 91.12 MiB)
Tripod: 1000 ms spikes protocol  73.736 ms (1348602 allocations: 89.75 MiB)
Tripod: 1000 ms spikes protocol - no synapses  6.742 ms (110133 allocations: 10.73 MiB)
Tripod: 1000 ms non-random spikes protocol  71.691 ms (1348602 allocations: 89.75 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses  6.799 ms (110133 allocations: 10.73 MiB)
Tripod: 10000 ms non-random spikes protocol - no synapses  76.594 ms (1100137 allocations: 107.16 MiB)
Tripod: 100000 ms non-random spikes protocol - no synapses  886.340 ms (11000138 allocations: 1.05 GiB)
SST: 1000 ms non-random spikes protocol - no synapses  1.893 ms (30046 allocations: 878.52 KiB)
PV: 1000 ms non-random spikes protocol - no synapses  2.033 ms (30046 allocations: 878.52 KiB)


Tripod: 1000 ms rate protocol
89.591 ms (1387070 allocations: 87.34 MiB)
Tripod: 1000 ms spikes protocol
83.459 ms (1357070 allocations: 85.96 MiB)
Tripod: 1000 ms spikes protocol - no synapses
4.713 ms (90134 allocations: 6.05 MiB)
Tripod: 1000 ms non-random spikes protocol
84.324 ms (1357070 allocations: 85.96 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses
4.766 ms (90134 allocations: 6.05 MiB)
Tripod: 10000 ms non-random spikes protocol - no synapses
52.431 ms (900135 allocations: 60.39 MiB)
Tripod: 100000 ms non-random spikes protocol - no synapses
609.717 ms (9000136 allocations: 603.81 MiB)
SST: 1000 ms non-random spikes protocol - no synapses
1.574 ms (20053 allocations: 644.22 KiB)
PV: 1000 ms non-random spikes protocol - no synapses
1.761 ms (20053 allocations: 644.22 KiB)
Tripod: 1000 ms non-random spikes protocol - fast
3.115 ms (30004 allocations: 1.15 MiB)


Final version
Tripod: 1000 ms spikes protocol  67.707 ms (827168 allocations: 22.94 MiB)
Tripod: 1000 ms spikes protocol - no synapses  4.555 ms (40232 allocations: 3.46 MiB)
Tripod: 1000 ms non-random spikes protocol  67.494 ms (827168 allocations: 22.94 MiB)
Tripod: 1000 ms non-random spikes protocol - no synapses  4.561 ms (40232 allocations: 3.46 MiB)
Tripod: 10000 ms non-random spikes protocol - no synapses  47.046 ms (400233 allocations: 34.45 MiB)
Tripod: 100000 ms non-random spikes protocol - no synapses  539.779 ms (4000234 allocations: 344.41 MiB)
SST: 1000 ms non-random spikes protocol - no synapses  1.674 ms (10048 allocations: 485.39 KiB)
PV: 1000 ms non-random spikes protocol - no synapses  1.663 ms (10048 allocations: 485.39 KiB)

Tripod: allocat Tripod
215.434 μs (209 allocations: 8.69 KiB)
Tripod: 1000 ms non-random spikes protocol - fast
  3.674 ms (20004 allocations: 1015.83 KiB)
Random arrays  1.225 ms (40004 allocations: 3.51 MiB)
=#
